library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;
use IEEE.numeric_std.all;
--use WORK.rvapo_wrappers.all;
entity WrapperSPIMemd is
generic (
	axi      	:integer:=1;
	simple		:integer:=0;
	debug      	:integer:=0; 
	unalign		:integer:=0
	);
port (
	clk_12MHz:in std_logic;

	spi0_mosi:out std_logic;--master port to psram 		
	spi0_miso:in std_logic;
	spi0_sclk:out std_logic;
	spi0_cs0:out std_logic;

	SPI_CSL:in std_logic;--slave port to riscv
	SPI_MOSI:in std_logic;
	SPI_MISO:out std_logic;
	SPI_SCLK:in std_logic;

	RGB0: out std_logic;
	RGB1: out std_logic;
	RGB2: out std_logic
);
end;

architecture rtl of WrapperSPIMemd is
	component SB_HFOSC is 
	generic (
		CLKHF_DIV: in string
		); 
	port(
		CLKHFEN	:   in  std_logic;
		CLKHFPU	:   in  std_logic;
		CLKHF	:   out  std_logic
		);
	end component;

	component SB_LFOSC is 
	port(
		CLKLFEN	:   in  std_logic;
		CLKLFPU	:   in  std_logic;
		CLKLF	:   out  std_logic
		);
	end component;

	component SB_RGBA_DRV is
		generic(
		CURRENT_MODE	:   in  string;
		RGB0_CURRENT	:	in  string;
		RGB1_CURRENT	:	in  string;
		RGB2_CURRENT	:	in  string
	); port(
		CURREN			:   in  std_logic;
		RGBLEDEN		:   in  std_logic;
		RGB0PWM			:   in  std_logic;
		RGB1PWM			:   in  std_logic;
		RGB2PWM			:   in  std_logic;
		RGB0			:   out  std_logic;
		RGB1			:   out  std_logic;
		RGB2			:   out  std_logic
	);
	end component;

    signal addr_dm:std_logic_vector(31 downto 0):=x"00000000";
	signal din_dm:std_logic_vector(31 downto 0);
	signal dout_dm:std_logic_vector(31 downto 0);
	signal en_dm:std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
	signal rst_dm:std_logic;--! resets output. Fixed '0' should suffice.	
	signal we_dm:std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
	
	signal addr_im:std_logic_vector(31 downto 0);
	signal din_im:std_logic_vector(31 downto 0);
	signal dout_im:std_logic_vector(31 downto 0);
	signal en_im:std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
	signal rst_im:std_logic;--! resets output. Fixed '0' should suffice.	
	signal we_im:std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)

    signal clock: clock_t;
	signal clk:std_logic:='0';
	signal trapped:std_logic_vector(5 downto 0);
	signal state:std_logic_vector(2 downto 0):="001";--this is starting red
begin
	--OSCInst2: SB_HFOSC generic map(CLKHF_DIV=>"0b11")port map(CLKHFEN=>'1',CLKHFPU=>'1',CLKHF=>clk);	
	--OSCInst2: SB_LFOSC port map(CLKLFEN=>'1',CLKLFPU=>'1',CLKLF=>clk);	
	clk<=clk_12MHz;
	--divider:process(clk_12MHz)begin
	--	if rising_edge(clk_12MHz)then clk<=not clk; end if;
	--end process;

    wrapper_axi: WrapperSPI 
	generic map(
    	axi      	=>axi,
		simple		=>simple,
		debug       =>debug,
		unalign		=>unalign
        )
	port map (
		clk=>clk, 
        rst=>clock.reset, 
		trapped=>trapped,

		addr_hm=>open,
		din_hm=>open,
		en_hm=>open,
		rst_hm=>open,
		we_hm=>open,
		dsel=>(others=>'0'),

		addr_dm=>addr_dm,
		din_dm=>din_dm,
		dout_dm=>dout_dm,
		en_dm=>en_dm,
		rst_dm=>rst_dm,
		we_dm=>we_dm,
		delayed_dm=>'1',
		
		addr_im=>addr_im,
		din_im=>din_im,
		dout_im=>dout_im,
		en_im=>en_im,
		rst_im=>rst_im,
		we_im=>we_im,
		delayed_im=>'1',

		spi0_mosi=>spi0_mosi,--master port to psram 		
		spi0_miso=>spi0_miso,
		spi0_sclk=>spi0_sclk,
		spi0_cs0=>spi0_cs0,

		SPI_CSL=>SPI_CSL,--slave port to riscv
		SPI_MOSI=>SPI_MOSI,
		SPI_MISO=>SPI_MISO,
		SPI_SCLK=>SPI_SCLK
		);

    memory: CombinedMemory_Delayed port map(
        clock=>clock, 

		addr_1=>addr_dm,
		read_1=>dout_dm,
		write_1=>din_dm,
		web=>we_dm,

		addr_2=>addr_im,
		read_2=>dout_im
        );

    clock.reset <= '1' when state/="100" else '0';
	clock.enable <= '1';
	clock.pulse <= clk;--blue green red

	process (clk)begin		--with both went red to blue
		if rising_edge(clk) then
			if state/="100" then state<=state(1 downto 0)&"0"; end if;
--			if trapped = "111111" then
--				if unsigned(addr_dm)>4096 then
--					state<="010";--this should give green on success
--				else
--					state<="100";--with only this also red to blue 
--				end if;
--			elsif trapped = "000001" then
--				state<="110";	
--			elsif trapped = "000010" then
--				state<="011";		
--			end if;
		end if;
	end process;--therefore cpu runs, but program is not executed
				--oscilloscope?

	RGBA_DRIVER: SB_RGBA_DRV generic map(
		CURRENT_MODE=>"0b1",
		RGB0_CURRENT=>"0b000001",
		RGB1_CURRENT=>"0b000001",
		RGB2_CURRENT=>"0b000001"
	) port map (
		CURREN=>'1',
		RGBLEDEN=>'1',
		RGB0PWM=>state(0),
		RGB1PWM=>state(1),
		RGB2PWM=>state(2),
		RGB0=>RGB0,
		RGB1=>RGB1,
		RGB2=>RGB2
	);
end;


