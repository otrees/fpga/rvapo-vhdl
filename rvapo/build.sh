#!/bin/bash

set -e

sources=(
	'../../integration/vivado/ip/lx-fncapprox/lx_fncapprox_pkg.vhd'
	'../../integration/vivado/ip/lx-fncapprox/lx_fncapprox_dsp48.vhd'
	'../../integration/vivado/ip/lx-fncapprox/rom_table.vhd'
	'../../integration/vivado/ip/lx-fncapprox/lx_fncapprox.vhd'

	'rvapo_pkg.vhd'
	'stage/rvapo_pc.vhd' 'stage/rvapo_fetch.vhd' 'stage/rvapo_dec.vhd' 'stage/rvapo_exec.vhd' 'stage/rvapo_mem.vhd' 'stage/rvapo_mem_unalign.vhd' 'stage/rvapo_wb.vhd'
	'logic/rvapo_hu.vhd' 'logic/rvapo_alu.vhd' 'logic/rvapo_bu.vhd' 'logic/rvapo_mul.vhd' 'logic/rvapo_b.vhd'
	'memory/rvapo_gpr.vhd' 'memory/rvapo_cm.vhd'
	'rvapo_intcon_pipeline.vhd' 'rvapo_intcon_simple.vhd' #'rvapo_intcon_ice40.vhd'  
	'../xilinx_axi/axim_test_v1_0.vhd' '../xilinx_axi/axim_test_v1_0_M00_AXI.vhd'
	'../../integration/vivado/ip/mmap/mmap.vhd' '../../integration/vivado/ip/irq_hw/irq_hw.vhd'
#	'../tb/rvapo_ctrl/rvapo_ctrl_v1_0.vhd'
#	'../tb/rvapo_ctrl/rvapo_ctrl_v1_0_S00_AXI.vhd'
)
#sources_target_fileload=('rvapo_minit.vhd' 'rvapo_gpr_target.vhd' ) #'rvapo_cm_target_fileloading_tmp.vhd')
sources_target=( 'rvapo_cm_target.vhd' '../core/rvapo_gpr_target.vhd')
sources_tb=(
	'rvapo_tb_pc.vhd'
#	'rvapo_tb_fetch.vhd'
#	'rvapo_tb_dec.vhd'
#	'rvapo_tb_exec.vhd'
#	'rvapo_tb_mem.vhd'
#	'rvapo_tb_wb.vhd'
#	'rvapo_tb_bu.vhd'
#	'rvapo_tb_alu.vhd'
#	'rvapo_tb_hu.vhd'
#	'rvapo_tb_full_simple.vhd'
#	'rvapo_tb_full_pipeline.vhd'
#	'rvapo_tb_memdelay.vhd'
#	'rvapo_tb_axi.vhd'
#	'rvapo_tb_unalign.vhd'
#	'rvapo_tb_uaxi.vhd'
#	'rvapo_tb_devel.vhd'
#	'rvapo_tb_ice40.vhd'
#	'rvapo_tb_full.vhd'
	'rvapo_tb_mmap.vhd'
#	'rvapo_tb_wishbone.vhd'
)
BSRC=$(dirname $BASH_SOURCE)
clean() {
	rm -rf "$BSRC/workdir"
}

analyze() {
	ghdl -a --std=93c --ieee=synopsys -fexplicit -g --workdir="$BSRC/workdir" "$1"
}

elaborate() {
	ghdl -e --std=93c --ieee=synopsys -fexplicit -g --workdir="$BSRC/workdir" "$1"
}

run() {
	elaborate "$1"
	local assert_level=${2:-failure}
	ghdl -r --std=93c --ieee=synopsys -fexplicit -g --workdir="$BSRC/workdir" "$1" --wave="$BSRC/workdir/$1.ghw" --assert-level="$assert_level"
}

help(){
	echo "Specify target"
	echo "  c - for clean"
	echo "  a - for analysis"
	echo "  t bin_name - to build and run with given memory init in software/out/vhdl"	
	echo "  f - build binaries loading software/out/hexstr/rvapo_mem_content.txt file at runtime"
    echo "  f hexstring_name - build with custom hexstr file (produced by software/buildc.sh) initialising memory"
	echo "	r - runs the last built executable (only with gcc backend)"
	exit 1
	}

analyzeall(){
	for file in "${sources[@]}"; do
		analyze "$BSRC/core/$file"
	done

	analyze "$BSRC/workdir/rvapo_cm_target_init.vhd"
	for file in "${sources_target[@]}"; do
		analyze "$BSRC/target_tb/$file"
	done

	for file in "${sources_tb[@]}"; do
		analyze "$BSRC/tb/$file"
	done
	}

if [ $# -eq 0 ] ; then
	help;
fi

while read -n1 character; do
   # echo "$character"

case "$character" in
	c|clean)
		clean
	;;

	a|analyze)
		clean
		mkdir -p "$BSRC/workdir"

		analyzeall
	;;

	t|test)
		clean
		mkdir -p "$BSRC/workdir"

		if [ "$2" != "" ]; then
			ln -sf "../../software/out/vhdl/$2.vhd" workdir/rvapo_cm_target_init.vhd
		else
			# ln -sf ./rvapo_cm_target_init_default.vhd workdir/rvapo_cm_target_init.vhd
			exit 1
		fi
		
		analyzeall

		for file in "${sources_tb[@]}"; do
			unit=${file%.*}
			echo "$unit"
			run "$unit"
		done
	;;

	f|file)
		clean
		mkdir -p "$BSRC/workdir"

        cp "$BSRC/target_tb/rvapo_cm_target_fileloader.vhd" "$BSRC/target_tb/rvapo_cm_target_fileloader_tmp.vhd"
		sed -i "s|init_mem_from_file(\"|init_mem_from_file(\"$BSRC/|" "$BSRC/target_tb/rvapo_cm_target_fileloader_tmp.vhd"
        if  [[ $# -eq 2 ]]; then
            replacepath=$2;
            sed -i "s|rvapo_mem_content.txt|$replacepath|" "$BSRC/target_tb/rvapo_cm_target_fileloader_tmp.vhd"
        fi

		ln -sf "../target_tb/rvapo_cm_target_fileloader_tmp.vhd" "$BSRC/workdir/rvapo_cm_target_init.vhd"

		analyzeall

    #    for file in "${sources_tb[@]}"; do #iterate testbeds
	#		unit=${file%.*}
    #        echo "$unit"
	#		run "$unit" #launch ghdl simulation
	#	done
	#	run rvapo_tb_full
		 for file in "${sources_tb[@]}"; do #iterate testbeds
			unit=${file%.*}
			elaborate "$unit" 
		done

		rm ./*.o
    #    rm "$BSRC/target_tb/rvapo_cm_target_fileloader_tmp.vhd";
    ;;

	r|run)
		ln -s -f "$BSRC/../integration/vivado/ip/lx-fncapprox/reci_tab_a.dat"  reci_tab_a.dat
		ln -s -f "$BSRC/../integration/vivado/ip/lx-fncapprox/reci_tab_bc.dat" reci_tab_bc.dat
		ln -s -f "$BSRC/../integration/vivado/ip/lx-fncapprox/sin_tab_a.dat"   sin_tab_a.dat
		ln -s -f "$BSRC/../integration/vivado/ip/lx-fncapprox/sin_tab_bc.dat"  sin_tab_bc.dat
		run rvapo_tb_mmap
		rm reci_tab_a.dat
		rm reci_tab_bc.dat
		rm sin_tab_a.dat
		rm sin_tab_bc.dat
	;;
	*) 
		echo "Unknown command $1";
    ;; 
esac
done < <(echo -n "$1")
