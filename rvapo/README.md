# RVAPO hardware definition files

This folder contains:

* `./core` contains the processing core, which comes in three flavors: single cycle, pipelined, pipelined with correct implementation of unaligned memory access. They share most of their files; top level is `rvapo_intcon_single.vhd` or `rvapo_intcon_pipeline.vhd`, respectively. It contains no memory. Only pipelined version is supported on FPGA.
* `./target_tb` contains platform-specific files, mainly memories. 
* `./tb` contains GHDL simulation testbeds. `rvapo_tb_mmap.vhd` is the main one which runs the whole system, others are for testing CPU components, deprecated in favour of integration testing (see `/scripts`).
* `./build.sh` Script for GHDL simulation.

## Simulation
In order to run a processor, you will first need to compile some code. 

Run this in `/software/c` for a basic example, or see the readme therein to see how to compile your own code. 
`make CFILE=tests/alu_smoke.c`

Then run `./build.sh <commands> <bin>`.
Most use cases will be covered by
`./build.sh fr` 
to build and run with the last compiled software. Paths and filenames will be correct by default.

Otherwise any number of commands from the following list can be given, without separators. They are executed in order.
- `t`: In this mode `<bin>` is extensionless name of vhdl file that is in `software/out/vhdl` directory and contains memory image of program to be ran, which is built into the processor. Simulation is launched immediately.
- Mode `f`: Memory is loaded from hexstring file provided in `<bin>` parameter (just name, assumed to be in `/software/out/hexstring`). If no file name is provided, a default is used (recommended method). Simulation not launched.
- Mode `r`: Run simulation of processor previously built by `t` or `f`.

This script can be run from any folder.
After running `<testbed name>.ghw` waveform files are available in `./workdir` and memory dump file `rvapo_mem_dump.txt` is produced in the folder `build.sh` was run from. 

## Memories
* `rvapo_cm_target.vhd` is the basic simulated memory. The `delayed` generics decides whether it is synchronous (like FPGA RAM macros) or async. Core supports both.
* `rvapo_cm_target_ice40.vhd` assembles Lattice ICE-40 RAM macros into a rvapo-compatible RAM.
* `rvapo_ice40_mem_impl.vhd` simulates Lattice ICE-40 RAM macros.
* `rvapo_cm_target_fileloader.vhd` loads memory content from file and provides it to `rvapo_cm_target.vhd` through MEMORY_INIT shared variable for easier simulation.
*  Xilinx BRAM blocks are used on Zynq FPGA through the Vivado block design. 
*  Lattice ICE-40 RAM are directly used on the ICE-V integration.

In simulation, memory always assumes the existence rvapo_cm_target_init component, which contains their initial content in MEMORY_INIT variable. 

This is supplied either as a constant in a VHDL file produced by `/scripts/buildc.sh` and symlinked by `/rvapo/build.sh t`, or `./target_tb/rvapo_cm_target_fileloader.vhd`, which loads a hex file also produced by `buildc.sh`. In order to even theoretically work on FPGA, this dependency would have to be removed, so RAM macros are used there instead.

Three generics are used for configuration of the pipelined version, each is an integer interpreted as boolean:
* enable_M  - to enable the rv32m ISA extension.
* debug     - to generate debug memory control.
* unalign   - to use correct handling of unaligned memory access.

Singlecycle also has some generics, but only for port compatibility, they have no effect.

## Documentation
Full documentation not yet published.

Some information is contained in the `/docs/` folder. The main design is based on the `mapo` project together with the architecture drawn in the `cpu_new.png` diagram (this is an updated version of the `cpu.png` diagram).

There are certain additions to the implementation that are not in the original design:

1. Branching happens through a `Branch unit`. This computes the `BranchOutcomeM` signal and communicates with `PC` stage. There are no signals directly between the `memory` stage and the `PC` stage. This is mostly a structural change.
2. `BranchControl` signal from `Control unit` contains all possible branch variants (see control trasfer instructions below). They are coalesced into one control signal.
3. There is a `trap` signal coming out of the `Control unit` and going into `Hazard unit` through the `memory` stage. Later the traps are propagated into the `Interconnect`.
4. In the `memory` stage the `PcPlusImm` signal doesn't only go to the `Branch unit`, it it also muxed into the mux under `Data cache` with `ALUOutM` and `PCPlus4M` to create a third option for `AUIPC` instruction.
5. In the `decode` stage `rs1`, `rs2` and `rd` signals aren't always takes straight from the `instruction` signal. Instead they are decoded based on instruction type (similar to `immControl`) - this is so that instructions which don't touch registers don't accidentally create stalls against pipelined instructions which write to a register.

### Quirks

Other quirks of this particular implementation:

1. An all-zero instruction (`0x00000000`) is a noop.
2. Taken from ARM Cortex chips, after reset the stack pointer register (x2) is loaded with the value in memory at `0x00000000` and the pc is set to the value at `0x00000004`.
3. The size of the ram is actually defined in the vhdl file template in `software/xtask/src/main.rs` file. This needs to be in sync with the `software/link.x` linker script to make sure the stack is placed correctly (i.e. that it doesn't start somewhere beyond the ram).
4. When memory is delayed, all memory accesses are like `lw`, unless they are also unaligned and `unalign` generic is set. That is because the logic handling `lh, lb, lbu, lhu` is skipped in that case. 

## Instructions

Instruction set: RV32IM

### Integer computation

* [x] ADD (R, op: 0110011, f3: 000, f7: 0000000)
* [x] SUB (R, op: 0110011, f3: 000, f7: 0100000)
* [x] SLL (R, op: 0110011, f3: 001, f7: 0000000)
* [x] SLT (R, op: 0110011, f3: 010, f7: 0000000)
* [x] SLTU (R, op: 0110011, f3: 011, f7: 0000000)
* [x] XOR (R, op: 0110011, f3: 100, f7: 0000000)
* [x] SRL (R, op: 0110011, f3: 101, f7: 0000000)
* [x] SRA (R, op: 0110011, f3: 101, f7: 0100000)
* [x] OR (R, op: 0110011, f3: 110, f7: 0000000)
* [x] AND (R, op: 0110011, f3: 111, f7: 0000000)
* [x] ADDI (I, op: 0010011, f3: 000)
* [x] SLTI (I, op: 0010011, f3: 010)
* [x] SLTIU (I, op: 0010011, f3: 011)
* [x] XORI (I, op: 0010011, f3: 100)
* [x] ORI (I, op: 0010011, f3: 110)
* [x] ANDI (I, op: 0010011, f3: 111)
* [x] SLLI (I[shift], op: 0010011, f3: 001, f7: 0000000)
* [x] SRLI (I[shift], op: 0010011, f3: 101, f7: 0000000)
* [x] SRAI (I[shift], op: 0010011, f3: 101, f7: 0100000)

### Control transfer

* [x] LUI (U, op: 0110111)
* [x] AUIPC (U, op: 0010111)
* [x] JAL (J, op: 1101111)
* [x] JALR (I, op: 1100111, f3: 000)
* [x] BEQ (B, op: 1100011, f3: 000)
* [x] BNE (B, op: 1100011, f3: 001)
* [x] BLT (B, op: 1100011, f3: 100)
* [x] BGE (B, op: 1100011, f3: 101)
* [x] BLTU (B, op: 1100011, f3: 110)
* [x] BGEU (B, op: 1100011, f3: 111)

### Load and Store

* [x] LB (I, op: 0000011, f3: 000)
* [x] LH (I, op: 0000011, f3: 001)
* [x] LW (I, op: 0000011, f3: 010)
* [x] LBU (I, op: 0000011, f3: 100)
* [x] LHU (I, op: 0000011, f3: 101)
* [x] SB (S, op: 0100011, f3: 000)
* [x] SH (S, op: 0100011, f3: 001)
* [x] SW (S, op: 0100011, f3: 010)

### Multiplication and division

* [x] MUL   (R, op: 0110011, f3: 000)
* [x] MULH  (R, op: 0110011, f3: 001)
* [x] MULHSU(R, op: 0110011, f3: 010)
* [x] MULHU (R, op: 0110011, f3: 011)
* [x] DIV   (R, op: 0110011, f3: 100)
* [x] DIVU  (R, op: 0110011, f3: 101)
* [x] REM   (R, op: 0110011, f3: 110)
* [x] REMU  (R, op: 0110011, f3: 111)

### Miscellaneous

* [x] FENCE (I, op: 0001111, f3: 000)
* [x] ECALL (I, op: 1110011, f3: 000, imm: 000000000000)
* [x] EBREAK (I, op: 1110011, rd: 00000, f3: 000, rs1: 00000, imm: 000000000001) 

