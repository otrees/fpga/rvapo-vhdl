library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity CombinedMemory is
	port (
		clock: in Clock_t;
		
		from_f: in Intcon_F2IM;
		from_m: in Intcon_M2DM;
		from_dm: in Intcon_DM2Vo;
		from_im: in Intcon_DM2Vo;			
		to_f: out Intcon_IM2F;
		to_m: out Intcon_DM2M;
		to_dm: out Intcon_DM2Vi;
		to_im: out Intcon_DM2Vi	
	);
end;

architecture rtl of CombinedMemory is
	signal web: std_logic_vector(3 downto 0);
begin
	to_dm.addr<=from_m.alu_out;
	to_dm.din<=from_m.write_data;
	to_m.read_data<=from_dm.dout;
	to_dm.en<='1';
	to_dm.rst<='0';
	to_dm.we<=web;
	to_m.delayed_by_clk<=from_dm.delayed_by_clk;
	to_m.mem_wait<=from_dm.mem_wait;

	to_im.addr<=from_f.pc;	
	to_im.din<=(others=>'0');
	to_f.instruction<=from_im.dout;
	to_im.en<='1';
	to_im.rst<='0';
	to_im.we<=(others=>'0');
	to_f.delayed_by_clk<=from_im.delayed_by_clk;
	to_f.mem_wait<=from_im.mem_wait;

	write: process (from_m)
	begin
		if from_m.mem_write='1' then	
			web<=from_m.mem_bytes;			
		else
			web<="0000";--this is here because vivado mem reqs we to be 0000 for reading.
		end if;	
	end process;
end;
