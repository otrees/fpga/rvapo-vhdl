library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity GeneralPurposeRegister is
	generic (
    	register_bypass      : std_logic := '1'--1 is enable, 0 disable (for singlecycle operation)      
    	);
	port (
		clock: in Clock_t;

		from_d: in Intcon_D2GPR;
		from_w: in Intcon_W2GPR;
		to_d: out Intcon_GPR2D
	);
end;

architecture rtl of GeneralPurposeRegister is
begin
	inst_gprf_target: GeneralPurposeRegister_Target 
	generic map (
        register_bypass    =>register_bypass 
    	)
	port map (
		clock,

		addr_1 => from_d.rs1,
		read_1 => to_d.val1,
		addr_2 => from_d.rs2,
		read_2 => to_d.val2,

		addr_3 => from_w.rd,
		write_enable_3 => from_w.reg_write,
		write_3 => from_w.result
	);
end;
