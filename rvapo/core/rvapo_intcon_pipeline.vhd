library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;
use ieee.numeric_std.all;

entity InterconnectPipeline is
	generic (
		enable_M	:integer;
    	debug      	:integer; --1 is enable, 0 disable 
		unalign		:integer
    	);
	port (
		clk: in std_logic;
		rst: in std_logic;
		trapped: out std_logic_vector(5 downto 0);

		addr_hm: out  std_logic_vector(31 downto 0);
		din_hm: out  std_logic_vector(31 downto 0);
		en_hm: out std_logic;rst_hm: out std_logic;
		we_hm: out std_logic_vector(3 downto 0);
		dsel: in std_logic_vector(7 downto 0);

		addr_dm: out std_logic_vector(31 downto 0);
		din_dm: out  std_logic_vector(31 downto 0);
		dout_dm: in  std_logic_vector(31 downto 0);
		en_dm: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
		rst_dm: out std_logic;--! resets output. Fixed '0' should suffice.	
		we_dm: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
		delayed_by_clk_dm: in std_logic;
		wait_dm: in std_logic;
		req_dm: out std_logic;
		
		addr_im: out  std_logic_vector(31 downto 0);
		din_im: out  std_logic_vector(31 downto 0);
		dout_im: in  std_logic_vector(31 downto 0);
		en_im: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
		rst_im: out std_logic;--! resets output. Fixed '0' should suffice.	
		we_im: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
		delayed_by_clk_im: in std_logic;
		wait_im: in std_logic;
		req_im: out std_logic;

		pause: in std_logic;
		issue: in std_logic;
		step: in std_logic;
		debug_instr: in std_logic_vector(31 downto 0);
		result: out std_logic_vector(31 downto 0)
	);
end entity;

architecture rtl of InterconnectPipeline is
	---------------------------------------------------------------------
	-- pipelined signals --
	signal PC2F_in: Intcon_PC2F;
	signal PC2F_out: Intcon_PC2F;

	signal F2D_in: Intcon_F2D;
	signal F2D_reg: Intcon_F2D;
	signal F2D_out: Intcon_F2D;

	signal D2E_in: Intcon_D2E;
	signal D2E_out: Intcon_D2E;

	signal E2M_in: Intcon_E2M;
	signal E2M_out: Intcon_E2M;

	signal M2W_in: Intcon_M2W;
	signal M2W_reg: Intcon_M2W;
	signal M2W_out: Intcon_M2W;

	signal M2M_in: Intcon_M2M;
	signal M2M_out: Intcon_M2M;--used only if unalign		
	---------------------------------------------------------------------
	-- backward signals --
	signal F2PC: Intcon_F2PC;
	signal BU2PC: Intcon_BU2PC;
	signal M2E: Intcon_M2E;
	signal W2E: Intcon_W2E;
	---------------------------------------------------------------------
	-- hazard signals --
	signal F2HU: Intcon_F2HU;
	signal D2HU: Intcon_D2HU;
	signal E2HU: Intcon_E2HU;
	signal M2HU: Intcon_M2HU;
	signal W2HU: Intcon_W2HU;
	signal BU2HU: Intcon_BU2HU;

	signal HU2E: Intcon_HU2E;

	signal M2BU: Intcon_M2BU;
	---------------------------------------------------------------------
	-- data signals (gpr, im, dm, alu) --
	signal E2ALU: Intcon_E2ALU;
	signal ALU2E: Intcon_ALU2E;

	signal F2IM: Intcon_F2IM;
	signal IM2F: Intcon_IM2F;

	signal D2GPR_in: Intcon_D2GPR;
	signal D2GPR_out: Intcon_D2GPR;
	signal GPR2D: Intcon_GPR2D;

	signal M2DM_in: Intcon_M2DM;
	signal M2DM_out: Intcon_M2DM;
	signal DM2M_in: Intcon_DM2M;
	signal DM2M_out: Intcon_DM2M;

	signal W2GPR_in: Intcon_W2GPR;
	signal W2GPR_out: Intcon_W2GPR;
	---------------------------------------------------------------------
	-- interconnect signals --
	type RunState_t is (
		RunState_Begin,
		RunState_SetupStack,
		RunState_SetupPc,
		RunState_SetupPc2,
		RunState_Run,
		RunState_Trapped
	);
	signal debug_control:Intcon_I2F:=I2F_ZERO;

	signal D2GPR_setup: Intcon_D2GPR;
	signal W2GPR_setup: Intcon_W2GPR;

	signal M2DM_setup: Intcon_M2DM;

	signal run_state: RunState_t;
	signal hazards: Hazards_t;

	signal to_dm: Intcon_DM2Vi;
	signal to_im: Intcon_DM2Vi;
	signal from_dm: Intcon_DM2Vo;
	signal from_im: Intcon_DM2Vo;

	signal clock: clock_t;
	--debug signals
	signal CycleCounter:BusWidth_t:=x"00000000";
	signal D_issue_delayer:std_logic:='0';
	signal D_step_delayer:std_logic:='0';
	signal result_boilerplate:BusWidth_t:=(others=>'0');
	--M skip fix
	signal we_delayed:std_logic_vector(3 downto 0):=(others=>'0');
	signal E2M_memctrl_delayed:MemoryControl_t:=MemoryControl_Word;

	signal PC2F_fresh:std_logic;
	signal D2E_fresh:std_logic;
	signal E2M_fresh:std_logic;
	signal M2W_fresh:std_logic;
begin
	clock.reset <= rst;
	clock.enable <= '1';
	clock.pulse <= clk;
	---------------------------------------------------------------------
	-- STAGES
	inst_pc: StageProgramCounter port map (
		from_f => F2PC,
		from_bu => BU2PC,
		to_f => PC2F_in
	);

	inst_f: StageFetch port map (
		from_pc => PC2F_out,
		from_im => IM2F,
		from_i=>debug_control,
		to_hu=>F2HU,
		to_im => F2IM,
		to_pc => F2PC,
		to_d => F2D_in
	);

	inst_d: StageDecode 
	generic map (
        enable_M    => enable_M
    	)
	port map (
		from_f => F2D_out,
		from_gpr => GPR2D,
		to_hu => D2HU,
		to_gpr => D2GPR_in,
		to_e => D2E_in
	);

	inst_e: StageExecute port map (
		from_d => D2E_out,
		from_hu => HU2E,
		from_alu => ALU2E,
		from_m => M2E,
		from_w => W2E,
		to_hu => E2HU,
		to_alu => E2ALU,
		to_m => E2M_in,
		fresh => E2M_fresh
	);

	mem_unalign:if unalign=1 generate
		inst_m: StageMemoryUnalign port map (
			from_e => E2M_out,
			from_dm => DM2M_out,
			to_hu => M2HU,
			to_bu => M2BU,
			to_dm => M2DM_in,
			to_e => M2E,
			to_w => M2W_in,
			to_m=>M2M_out,
			from_m=>M2M_in,
			fresh => E2M_fresh,
			mem_req => req_dm
			);
	end generate mem_unalign;
	mem_simple:if unalign=0 generate--else generate permitted only in vhdl08, which does not permit unprotected shared variables that are used in fileloader
		req_dm<=E2M_fresh;
		inst_m: StageMemory port map (
			from_e => E2M_out,
			from_dm => DM2M_out,
			to_hu => M2HU,
			to_bu => M2BU,
			to_dm => M2DM_in,
			to_e => M2E,
			to_w => M2W_in
		);
	end generate mem_simple;
	inst_w: StageWriteback port map (
		from_m => M2W_out,
		to_hu => W2HU,
		to_gpr => W2GPR_in,
		to_e => W2E,
		fresh => M2W_fresh,
		clock => clock
	);
	---------------------------------------------------------------------
	-- Logic units
	inst_hu: HazardUnit port map (
		from_bu => BU2HU,
		from_f => F2HU,
		from_d => D2HU,
		from_e => E2HU,
		from_m => M2HU,
		from_w => W2HU,
		to_e => HU2E,

		hazards => hazards
	);

	inst_alu: ArithmeticLogicUnit
	generic map (
        enable_M    => enable_M
    	)
	port map (
		clock=>clock,
		from_e => E2ALU,
		to_e => ALU2E
	);

	inst_bu: BranchUnit port map (
		from_m => M2BU,
		to_hu => BU2HU,
		to_pc => BU2PC
	);
	---------------------------------------------------------------------
	-- Memories
	D2GPR_out <= D2GPR_in when run_state = RunState_Run else D2GPR_setup;
	W2GPR_out <= W2GPR_in when run_state = RunState_Run else W2GPR_setup;

	inst_gpr: GeneralPurposeRegister port map (
		clock => clock,

		from_d => D2GPR_out,
		from_w => W2GPR_out,
		to_d => GPR2D
	);

	M2DM_out <= M2DM_in when run_state = RunState_Run else M2DM_setup;
	DM2M_out <= DM2M_in when run_state = RunState_Run else DM2M_ZERO;

	inst_cm: CombinedMemory port map (
		clock => clock,

		from_f => F2IM,
		from_m => M2DM_out,
		from_dm => from_dm, 
		from_im => from_im,
		to_f => IM2F,
		to_m => DM2M_in,
		to_dm => to_dm, 
		to_im => to_im 
	);

	--DATA MEM
	addr_dm<=to_dm.addr;
	din_dm<=to_dm.din;
	from_dm.dout<=dout_dm;
	en_dm<=to_dm.en;
	rst_dm<=to_dm.rst;
	we_dm<=to_dm.we;
	from_dm.delayed_by_clk<=delayed_by_clk_dm;
	from_dm.mem_wait<=wait_dm;
	--INSTR MEM
	addr_im<=to_im.addr;
	din_im<=to_im.din;
	from_im.dout<=dout_im;
	en_im<=to_im.en;
	rst_im<=to_im.rst;
	we_im<=to_im.we;
	from_im.delayed_by_clk<=delayed_by_clk_im;
	from_im.mem_wait<=wait_im;

	req_im<=PC2F_fresh;

	gen_debug_ctrl:if debug=1 generate
		addr_hm<=CycleCounter;
		en_hm<='1';
		rst_hm<='0';
		we_hm<="1111" when unsigned(CycleCounter)<4096 and debug_control.issue='1' else "0000";

		process(dsel,clk,F2D_out,F2D_in,W2GPR_in,hazards,CycleCounter,debug_control,clock)begin
			if dsel=x"00" then din_hm<=F2D_out.pc;
			elsif  dsel=x"01" then din_hm<=F2D_in.pc;	--equals F2IM.pc
			elsif  dsel=x"02" then din_hm<=W2GPR_in.result;
			elsif  dsel=x"03" then din_hm<=	hazards.stall_fetch&
							hazards.stall_decode&
							hazards.flush_decode&	
							hazards.flush_execute&
							hazards.flush_memory&"0"&
							hazards.stall_all&
							hazards.stall_mem_feedback&
							x"000000";
			elsif  dsel=x"04" then din_hm<=F2D_out.instruction;
			elsif  dsel=x"05" then din_hm<=debug_control.instruction;
			elsif  dsel=x"06" then din_hm<=x"FFFF0"&'0'&debug_control.pause&debug_control.step&debug_control.issue&x"F"&"000"&clock.reset;
			elsif  dsel=x"07" then din_hm<=F2D_in.instruction;
			elsif  dsel=x"08" then din_hm<=CycleCounter;
			end if;
		end process;
	end generate gen_debug_ctrl;	

	gen_nodebug:if debug=0 generate
		addr_hm<=debug_control.instruction;
		din_hm<=x"FFFF0"&'0'&debug_control.pause&debug_control.step&debug_control.issue&x"F"&"000"&clock.reset;
		en_hm<='0';
		we_hm<="0000";
	end generate gen_nodebug;	

	result<=result_boilerplate;

	skip_fix_delayer:process(clock)begin
		if rising_edge(clock.pulse) then
			if E2M_out.alu_out(1 downto 0)="00" or unalign=0 then
				we_delayed<=M2DM_out.mem_bytes;
				E2M_memctrl_delayed<=E2M_out.control.mem_control;
			else
				we_delayed<=(others=>'1');
				E2M_memctrl_delayed<=MemoryControl_Word;
			end if;	
		end if;
	end process;

	process (clock,F2D_in,F2D_reg,to_im,F2D_out,M2W_reg,M2W_in,M2W_out,to_dm,
		from_dm,from_im,we_delayed,E2M_memctrl_delayed)
--	variable dout_masked:BusWidth_t;
	variable dout_mask:BusWidth_t;
	begin
		dout_mask(7 downto 0):=(others=>we_delayed(0));
		dout_mask(15 downto 8):=(others=>we_delayed(1));
		dout_mask(23 downto 16):=(others=>we_delayed(2));
		dout_mask(31 downto 24):=(others=>we_delayed(3));

		if clock.reset='0'then
			F2D_out.instruction<=F2D_reg.instruction;
			F2D_out.pc <= F2D_reg.pc;	--fpga
			F2D_out.pc_plus_4 <= F2D_reg.pc_plus_4;
			F2D_out.delayed_by_clk <= F2D_reg.delayed_by_clk;
			if F2D_reg.delayed_by_clk='1' then
				F2D_out.instruction<=from_im.dout;
			end if;
			
			M2W_out <= M2W_reg;
			if M2W_reg.delayed_by_clk='1' then
				--dout_masked:=from_dm.dout&dout_mask;
				if E2M_memctrl_delayed=MemoryControl_Byte then
					M2W_out.read_data(7 downto 0)<=from_dm.dout(7 downto 0);
					M2W_out.read_data(31 downto 8)<=(others=>from_dm.dout(7));
				elsif E2M_memctrl_delayed=MemoryControl_Half then
					M2W_out.read_data(15 downto 0)<=from_dm.dout(15 downto 0);
					M2W_out.read_data(31 downto 16)<=(others=>from_dm.dout(15));
				else
					M2W_out.read_data<=from_dm.dout and dout_mask;
				end if;
		--		M2W_out.read_data<=from_dm.dout;--M2W_in.read_data;
			end if;
		else 
			F2D_out <= F2D_ZERO;
			M2W_out <= M2W_ZERO;
		end if;
	end process;
	---------------------------------------------------------------------
	process (clock)
	begin
		if rising_edge(clock.pulse) then
			PC2F_fresh<=not hazards.stall_fetch;
			D2E_fresh<=hazards.stall_all nor hazards.flush_execute;
			E2M_fresh<=(hazards.stall_all and hazards.stall_mem_feedback) nor hazards.flush_memory;
			M2W_fresh<=not hazards.stall_all;	

		if clock.reset = '1' then
			CycleCounter<=x"00000000";

			PC2F_out <= PC2F_ZERO;
	--		F2D_out <= F2D_ZERO;
			F2D_reg <= F2D_ZERO;	
			D2E_out <= D2E_ZERO;
			E2M_out <= E2M_ZERO;
	--		M2W_out <= M2W_ZERO;
			M2W_reg <= M2W_ZERO;
			M2M_in <= M2M_ZERO;

			D2GPR_setup <= D2GPR_ZERO;
			W2GPR_setup <= W2GPR_ZERO;
			M2DM_setup <= M2DM_ZERO;

			run_state <= RunState_Begin;
			trapped <= (others=>'0');--Trap_None;
			debug_control <=I2F_ZERO;
			result_boilerplate<=(others=>'0');
		elsif clock.enable = '1' then--and rising_edge(clock.pulse) then
			if debug_control.issue='1' then
				result_boilerplate<=(others=>'0');
			elsif W2GPR_in.result/=BUSWIDTH_ZERO then 
				result_boilerplate<=W2GPR_in.result;
			end if;

			if run_state /= RunState_Trapped and debug_control.issue='1' and unsigned(CycleCounter)<4096 then
				CycleCounter<=std_logic_vector(unsigned(CycleCounter) + 4);
			end if;
			
			case run_state is
				-- read initial stack pointer from memory
				when RunState_Begin =>
					run_state <= RunState_SetupStack;
					M2DM_setup <= (
						alu_out => RESET_VECTOR_STACK,
						write_data => BUSWIDTH_ZERO,
						mem_write => '0',
						mem_bytes => (others=>'1')
					);
				-- store initial stack pointer in register
				-- read reset pointer from memory
				when RunState_SetupStack =>
					run_state <= RunState_SetupPc;
					M2DM_setup <= (
						alu_out => RESET_VECTOR_RESET,
						write_data => BUSWIDTH_ZERO,
						mem_write => '0',
						mem_bytes => (others=>'1')
					);
					W2GPR_setup <= (
						result => DM2M_in.read_data,
						rd => REGSEL_STACK,
						reg_write => '1'
					);
				when RunState_SetupPc =>
					M2DM_setup <= (
						alu_out => RESET_VECTOR_RESET,
						write_data => BUSWIDTH_ZERO,
						mem_write => '0',
						mem_bytes => (others=>'1')
						);	
					run_state <= RunState_SetupPc2;
					PC2F_out <= (pc => DM2M_in.read_data);--,pc_plus_4 => DM2M_in.read_data);--x"00000008");--
				--	M2DM_setup <= M2DM_ZERO;
					W2GPR_setup <= (
						result => DM2M_in.read_data,
						rd => REGSEL_STACK,
						reg_write => DM2M_in.delayed_by_clk
					);
				-- set pc to reset pointer
				when RunState_SetupPc2 =>
					run_state <= RunState_Run;
					PC2F_out <= (pc => DM2M_in.read_data);
					M2DM_setup <= M2DM_ZERO;
					W2GPR_setup <= W2GPR_ZERO;
				-- let the cpu run
				when RunState_Run =>
					if hazards.trap /= Trap_None and issue='0' then
						run_state <= RunState_Trapped;
						trapped <= trap_enum2vec(hazards.trap);
					else
						D_issue_delayer<=issue;--debug_control.issue;
						D_step_delayer<=step;--debug_control.step;
				--		debug_control.issue_old<=D_issue_delayer;
						debug_control.issue<=issue and not D_issue_delayer;		
						debug_control.pause<=pause;
						debug_control.instruction<=debug_instr;
						debug_control.step<=step and not D_step_delayer;
					--	if W2GPR_in.result/=BUSWIDTH_ZERO then result_boilerplate<=W2GPR_in.result; end if;										

						if hazards.stall_fetch /= '1' then
							PC2F_out <= PC2F_in;
						end if;
						
						if hazards.flush_decode = '1' then
							F2D_reg <= F2D_ZERO;
						elsif hazards.stall_decode /= '1' then
							F2D_reg <= F2D_in;
						else 
							if F2D_in.delayed_by_clk='1' then
								F2D_reg<=F2D_out;--because then this, not F2D_in, is the val that needs to be remembered
							end if;
							F2D_reg.delayed_by_clk<='0';
						end if;

						if hazards.flush_execute = '1' then
							D2E_out <= D2E_ZERO;
						elsif hazards.stall_all/='1' then
							D2E_out <= D2E_in;
						end if;

						if hazards.flush_memory = '1' then
							E2M_out <= E2M_ZERO;
						elsif hazards.stall_all/='1' then
							E2M_out <= E2M_in;
						end if;
						
						if hazards.stall_all/='1' then--M2W_in is copy of DM out, M2W_reg is that but delayed, M2W_out gets injected with current mem out, thus solving delay
							M2W_reg <= M2W_in;																--except when stall
						else 
							if M2W_in.delayed_by_clk='1' then
								M2W_reg<=M2W_out;--unlike F2D's, this reg did not update so it could not hold M2W_out in second half of stall period; fix in mem_unalign
							end if;
							M2W_reg.delayed_by_clk<='0';
						end if;

						if hazards.stall_mem_feedback/='1' then
							M2M_in<=M2M_out;--used only if unalign
						end if;
					end if;
				when RunState_Trapped =>
					if issue='1' and pause='1' then
						run_state<=RunState_Run;
					end if;
					null;
			end case;
		end if;
		end if;
	end process;
end architecture;
