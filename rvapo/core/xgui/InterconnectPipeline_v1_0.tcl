# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "debug" -parent ${Page_0}
  ipgui::add_param $IPINST -name "enable_M" -parent ${Page_0}
  ipgui::add_param $IPINST -name "unalign" -parent ${Page_0}


}

proc update_PARAM_VALUE.debug { PARAM_VALUE.debug } {
	# Procedure called to update debug when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.debug { PARAM_VALUE.debug } {
	# Procedure called to validate debug
	return true
}

proc update_PARAM_VALUE.enable_M { PARAM_VALUE.enable_M } {
	# Procedure called to update enable_M when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.enable_M { PARAM_VALUE.enable_M } {
	# Procedure called to validate enable_M
	return true
}

proc update_PARAM_VALUE.unalign { PARAM_VALUE.unalign } {
	# Procedure called to update unalign when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.unalign { PARAM_VALUE.unalign } {
	# Procedure called to validate unalign
	return true
}


proc update_MODELPARAM_VALUE.enable_M { MODELPARAM_VALUE.enable_M PARAM_VALUE.enable_M } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.enable_M}] ${MODELPARAM_VALUE.enable_M}
}

proc update_MODELPARAM_VALUE.debug { MODELPARAM_VALUE.debug PARAM_VALUE.debug } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.debug}] ${MODELPARAM_VALUE.debug}
}

proc update_MODELPARAM_VALUE.unalign { MODELPARAM_VALUE.unalign PARAM_VALUE.unalign } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.unalign}] ${MODELPARAM_VALUE.unalign}
}

