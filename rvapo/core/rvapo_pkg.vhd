library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

package rvapo_pkg is
	subtype BusWidth_t is std_logic_vector (31 downto 0);
	subtype RegSel_t is std_logic_vector (4 downto 0);

	type Clock_t is record
		pulse: std_logic;
		enable: std_logic;
		reset: std_logic;
	end record;

	type ShiftDirection_t is (
		ShiftDirection_Left,
		ShiftDirection_Right
	);

	type Trap_t is (
		Trap_None,
		Trap_Fault,
		Trap_Break,
		Trap_Call
	);

	type Hazards_t is record
		--! Stall pc-to-fetch latch.
		stall_fetch: std_logic;
		--! Stall fetch-to-decode latch.
		stall_decode: std_logic;
		--! Flush fetch-to-decode latch.
		flush_decode: std_logic;
		--! Flush decode-to-execute latch.
		flush_execute: std_logic;
		--! Flush execute-to-memory latch.
		flush_memory: std_logic;
		--! Break execution and halt the pc.
		trap: Trap_t;
		--! Stall whole cpu. Crude but saves a few signals.
		stall_all: std_logic;
		--! Stall memory feedback loop.
		stall_mem_feedback: std_logic;
	end record;

	---------------------------------------------------------------------
	-- Types wrapping pipeline stage interconnects
	-- Naming scheme: Intcon_<FROM>2<TO>
	-- Stages: Program counter, Fetch, Decode, Execute, Memory, Writeback, 
	-- 	Hazard unit, Branch unit, Arithmetica Logic Unit,
	--	General purpose register file, Instruction memory, Data memory

	---------------------------------------------------------------------
	-- General purpose register file (GPR)

	type Intcon_D2GPR is record
		--! First source register selector.
		rs1: RegSel_t;
		--! Second source register selector.
		rs2: RegSel_t;
	end record;

	type Intcon_GPR2D is record
		--! Value of rs1 register.
		val1: BusWidth_t;
		--! Value of rs2 register.
		val2: BusWidth_t;
	end record;

	type Intcon_W2GPR is record
		--! Resulting value.
		result: BusWidth_t;
		--! Destination register selector.
		rd: RegSel_t;
		--! Result written to register.
		reg_write: std_logic;
	end record;

	---------------------------------------------------------------------
	-- General memory
	
	type Intcon_DM2Vi is record
		addr: BusWidth_t;
		--! data in
		din: BusWidth_t;
		--! data out
	--	dout: BusWidth_t;
		--! enable r/w/reset
		en: std_logic;
		--! resets output
		rst: std_logic;
		--! choose bytes to write (out of 32b word). Must be 0 for reading in fpga.
		we: std_logic_vector(3 downto 0);
	end record;

	type Intcon_DM2Vo is record
		--! data out
		dout: BusWidth_t;
		--! '0'=>the memory reads immediately, '1'=>instead data is returned at next clock
		delayed_by_clk: std_logic;
		--! Indicates multicycle memory operation.
		mem_wait: std_logic;
	end record;

	---------------------------------------------------------------------
	-- Instruction memory

	type Intcon_F2IM is record
		--! Program Counter value.
		pc: BusWidth_t;
	end record;

	type Intcon_IM2F is record
		--! Instruction machine code read at PC address.
		instruction: BusWidth_t;
		--! '0'=>the memory reads immediately, '1'=>instead data is returned at next clock
		delayed_by_clk: std_logic;
		--! Indicates multicycle memory read.
		mem_wait: std_logic;
	end record;

	---------------------------------------------------------------------
	-- Data memory

	type MemoryControl_t is (
		MemoryControl_Word,
		MemoryControl_Half,
		MemoryControl_HalfUnsigned,
		MemoryControl_Byte,
		MemoryControl_ByteUnsigned
	);

	type Intcon_M2DM is record
		--! Result from ALU.
		alu_out: BusWidth_t;
		--! Value to be written to memory.
		write_data: BusWidth_t;
		--! Instruction writes to memory.
		mem_write: std_logic;
		--! Which bytes of the word shall be written.
		mem_bytes:std_logic_vector(3 downto 0);
	end record;

	type Intcon_DM2M is record
		--! Value read from memory.
		read_data: BusWidth_t;
		--! '0'=>the memory reads immediately, '1'=>instead data is returned at next clock
		delayed_by_clk: std_logic;
		--! Indicates multicycle memory operation.
		mem_wait: std_logic;
	end record;

	---------------------------------------------------------------------
	---------------------------------------------------------------------
	-- Hazard unit (HU)

	type ForwardValue_t is (
		ForwardValue_None,
		ForwardValue_Memory,
		ForwardValue_Writeback
	);

	type Intcon_F2HU is record 
		--! Instruction not read yet.
		mem_wait: std_logic;
	end record;


	type Intcon_D2HU is record 
		--! First source register selector.
		rs1: RegSel_t;
		--! Second source register selector.
		rs2: RegSel_t;
	end record;

	type Intcon_E2HU is record
		--! First source register selector.
		rs1: RegSel_t;
		--! Second source register selector.
		rs2: RegSel_t;
		--! Destination register selector.
		rd: RegSel_t;
		--! Loaded value comes from memory (otherwise comes from ALU).
		mem_to_reg: std_logic;
		--! Multicycle ALU operation
		ewait: std_logic;
	end record;

	type Intcon_W2HU is record
		--! Destination register selector.
		rd: RegSel_t;
		--! Result written to register.
		reg_write: std_logic;
	end record;

	type Intcon_M2HU is record
		--! Destination register selector.
		rd: RegSel_t;
		--! Result written to register.
		reg_write: std_logic;
		--! Trap instruction.
		trap: Trap_t;
		--! Indicates multicycle memory operation.
		mem_wait: std_logic;
		--! Indicates unaligned memory access.
		unalign: std_logic;
	end record;

	type Intcon_HU2E is record
		--! Replace RD1 by a forwarded value.
		forwardA: ForwardValue_t;
		--! Replace RD2 by a forwarded value.
		forwardB: ForwardValue_t;
	end record;

	---------------------------------------------------------------------
	-- Arithmetic Logic Unit

	type MulDivOp_t is(
		MulDivOp_Mul,
		MulDivOp_MulH,
		MulDivOp_MulHSU,
		MulDivOp_MulHU,
		MulDivOp_Div,
		MulDivOp_DivU,
		MulDivOp_Rem,
		MulDivOp_RemU
	);

	type AluControl_t is (
		AluControl_ADD,
		AluControl_SUB,
		AluControl_SLL,
		AluControl_SLT,
		AluControl_SLTU,
		AluControl_XOR,
		AluControl_SRL,
		AluControl_SRA,
		AluControl_OR,
		AluControl_AND,
		AluControl_MUL,
		AluControl_CLZ
	);

	type AluFlag_t is record
		--! Indicates the result is equal to zero.
		zero: std_logic;
	end record;

	type Intcon_E2ALU is record
		--! ALU function selection.
		alu_control: AluControl_t;
		--! ALU source A.
		srcA: BusWidth_t;
		--! ALU source B.
		srcB: BusWidth_t;
		--! Multiplication signs
		multype: MulDivOp_t;
		--! Pipeline has just moved.
		fresh: std_logic;
	end record;

	type Intcon_ALU2E is record
		--! Result from ALU.
		alu_out: BusWidth_t;
		--! Result flags from ALU.
		alu_flag: AluFlag_t;
		--! Long ALU operation
		alu_wait: std_logic;
	end record;

	type Intcon_ALU2MUL is record
		--! request to start multiplication
		fresh: std_logic;
		-- what to do
		operation:MulDivOp_t;
		--! request to start multiplication
		start: std_logic;
		--! is first operand signed		
		asigned: std_logic;
		--! is second operand signed
		bsigned: std_logic;
		--! first operand
		a: BusWidth_t;
		--! second operand
        b: BusWidth_t;
		--! result of the outsourced clz
		clz_result: std_logic_vector(5 downto 0); 
	end record;

	type Intcon_MUL2ALU is record
		--! result of multiplication
		c: std_logic_vector(31 downto 0);
		--! signals that multiplication is done
        done: std_logic;
		--! division requires clz; outsourcing it allows reuse of resources
		clz_request: BusWidth_t;
	end record;

	type Intcon_B2ALU is record
		--! result bit operation
		result: std_logic_vector(5 downto 0);
	end record;

	type Intcon_ALU2B is record
		--! operand 1
		srcA: BusWidth_t;
		--! operand 2
		srcB: BusWidth_t;
	end record;

	---------------------------------------------------------------------
	-- Branch unit

	type BranchControl_t is (
		BCtrl_None,
		BCtrl_JALR,
		BCtrl_JAL,
		BCtrl_BEQ,
		BCtrl_BNE,
		BCtrl_BLT,
		BCtrl_BGE,
		BCtrl_BLTU,
		BCtrl_BGEU
	);

	type Intcon_M2BU is record
		--! Result from ALU.
		alu_out: BusWidth_t;
		--! Result flags from ALU.
		alu_flag: AluFlag_t;
		--! Program Counter + Immediate.
		pc_plus_imm: BusWidth_t;
		--! Branch control.
		branch: BranchControl_t;
	end record;

	type Intcon_BU2HU is record
		--! Whether a branch will be taken.
		branch_outcome: std_logic;
	end record;

	type Intcon_BU2PC is record
		--! Whether to override pc.
		pc_override: std_logic;
		--! The value to override pc with.
		pc_value: BusWidth_t;
	end record;

	---------------------------------------------------------------------
	---------------------------------------------------------------------

	type AluSource_t is (
		AluSource_Reg,
		AluSource_Imm	
	);

	type ControlUnit_t is record
		--! Branch control.
		branch: BranchControl_t;

		--! Result written to register.
		reg_write: std_logic;
		--! Loaded value comes from memory (otherwise comes from ALU).
		mem_to_reg: std_logic;
		--! Instruction writes to memory.
		mem_write: std_logic;
		--! Memory load/store width and extend behavior.
		mem_control: MemoryControl_t;
		--! Result is taken from pc + immediate.
		pc_plus_imm_to_reg: std_logic;
		--! Trap instruction.
		trap: Trap_t;
	end record;

	-- Program counter

	type Intcon_F2PC is record
		--! Program Counter + 4.
		pc_plus_4: BusWidth_t;
	end record;
	
	-- Fetch

	type Intcon_PC2F is record
		--! Program Counter value.
		pc: BusWidth_t;
	end record;

	-- Decode

	type Intcon_F2D is record
		--! Program Counter value.
		pc: BusWidth_t;
		--! Program Counter + 4.
		pc_plus_4: BusWidth_t;
		--! Instruction machine code read at PC address.
		instruction: BusWidth_t;
		--! '0'=>the memory reads immediately, '1'=>instead data is returned at next clock
		delayed_by_clk: std_logic;
	end record;

	-- Execute

	type Intcon_D2E is record
		--! Program Counter value.
		pc: BusWidth_t;
		--! Program Counter + 4.
		pc_plus_4: BusWidth_t;

		--! First source register selector.
		rs1: RegSel_t;
		--! Second source register selector.
		rs2: RegSel_t;
		--! Destination register selector.
		rd: RegSel_t;

		--! Value of rs1 register.
		val1: BusWidth_t;
		--! Value of rs2 register.
		val2: BusWidth_t;
		--! Value of immediate.
		valI: BusWidth_t;

		--! Control unit.
		control: ControlUnit_t;
		
		--! ALU function selection.
		alu_control: AluControl_t;
		--! ALU source.
		alu_source: AluSource_t;
		--! Multiplication signs
		multype: MulDivOp_t;
	end record;

	type Intcon_M2E is record
		--! Forwarded result from ALU or pc_plus_4.
		result_data: BusWidth_t;
	end record;

	type Intcon_W2E is record
		--! Resulting value.
		result: BusWidth_t;
	end record;

	-- Memory

	type Intcon_E2M is record
		--! Program Counter + 4.
		pc_plus_4: BusWidth_t;
		--! Program Counter + Immediate.
		pc_plus_imm: BusWidth_t;

		--! Result from ALU.
		alu_out: BusWidth_t;
		--! Result flags from ALU.
		alu_flag: AluFlag_t;
		--! Value to be written to memory.
		write_data: BusWidth_t;

		--! Destination register selector.
		rd: RegSel_t;

		--! Control unit.
		control: ControlUnit_t;
	end record;

	-- Writeback

	type Intcon_M2W is record
		--! Value read from memory.
		read_data: BusWidth_t;
		--! Result from ALU or pc_plus_4.
		result_data: BusWidth_t;

		--! Destination register selector.
		rd: RegSel_t;

		--! Result written to register.
		reg_write: std_logic;
		--! Loaded value comes from memory (otherwise comes from ALU).
		mem_to_reg: std_logic;
		--! to bypass data read
		delayed_by_clk : std_logic;
	end record;

	type Intcon_M2M is record--feedback of mem_unalign so that it does not need a clock
		--! Data read on previous clock.
		addrdelayer: BusWidth_t;
		--! Data read on previous clock.
		datadelayer: BusWidth_t;
		--! State of unaligned memory access.
		step: std_logic_vector(1 downto 0);
		--! Memory control command on previous clock.
		mem_control: MemoryControl_t;
		--! Memory read is done, no bypass required.
		no_delay: std_logic;
		--! Bit mask register.
		bitreg: std_logic_vector(7 downto 0);
	end record;

	type Intcon_I2F is record
		--! pause execution
		pause: std_logic;
		--! new debugger instruction
		issue: std_logic;
		--! execute one instruction on rising edge of this signal
		step: std_logic;
		--! TODO: get rid of this nonsense
	--	issue_old:std_logic;
		--! the debug instruction
		instruction: BusWidth_t;
	end record;
	---------------------------------------------------------------------
	---------------------------------------------------------------------
	-- Stage components

	component StageProgramCounter is
		port (
			from_f: in Intcon_F2PC;
			from_bu: in Intcon_BU2PC;
			to_f: out Intcon_PC2F
		);
	end component;

	component StageFetch is
		port (
			from_pc: in Intcon_PC2F;
			from_im: in Intcon_IM2F;
			from_i: in Intcon_I2F;
			to_hu: out Intcon_F2HU;
			to_im: out Intcon_F2IM;
			to_pc: out Intcon_F2PC;
			to_d: out Intcon_F2D
		);
	end component;

	component StageDecode is
		generic (
        	enable_M	:integer:=0
    		);
		port (
			from_f: in Intcon_F2D;
			from_gpr: in Intcon_GPR2D;
			to_hu: out Intcon_D2HU;
			to_gpr: out Intcon_D2GPR;
			to_e: out Intcon_D2E
		);
	end component;

	component StageExecute is
		port (
			from_d: in Intcon_D2E;
			from_hu: in Intcon_HU2E;
			from_alu: in Intcon_ALU2E;
			from_m: in Intcon_M2E;
			from_w: in Intcon_W2E;
			to_hu: out Intcon_E2HU;
			to_alu: out Intcon_E2ALU;
			to_m: out Intcon_E2M;
			fresh: in std_logic
		);
	end component;

	component StageMemory is
		port (
			from_e: in Intcon_E2M;
			from_dm: in Intcon_DM2M;
			to_hu: out Intcon_M2HU;
			to_bu: out Intcon_M2BU;
			to_dm: out Intcon_M2DM;
			to_e: out Intcon_M2E;
			to_w: out Intcon_M2W
		);
	end component;

	component StageMemoryUnalign is
		port (
			from_e: in Intcon_E2M;
			from_dm: in Intcon_DM2M;
			to_hu: out Intcon_M2HU;
			to_bu: out Intcon_M2BU;
			to_dm: out Intcon_M2DM;
			to_e: out Intcon_M2E;
			to_w: out Intcon_M2W;
			to_m: out Intcon_M2M;
			from_m: in Intcon_M2M;
			fresh: in std_logic;	
			mem_req: out std_logic
		);
	end component;

	component StageWriteback is
		port (
			from_m: in Intcon_M2W;
			to_hu: out Intcon_W2HU;
			to_gpr: out Intcon_W2GPR;
			to_e: out Intcon_W2E;
			fresh: in std_logic;
			clock: in Clock_t
		);
	end component;

	---------------------------------------------------------------------
	-- Logic components

	component HazardUnit is
		generic (
			register_bypass      : std_logic := '1'--1 is enable, 0 disable (for singlecycle operation)      
			);
		port (
			from_bu: in Intcon_BU2HU;
			from_f: in Intcon_F2HU;
			from_d: in Intcon_D2HU;
			from_e: in Intcon_E2HU;
			from_m: in Intcon_M2HU;
			from_w: in Intcon_W2HU;			
			to_e: out Intcon_HU2E;

			hazards: out Hazards_t
		);
	end component;

	component BarrelShifter is
		port (
			data: in BusWidth_t;
			shift: in std_logic_vector(4 downto 0);
			direction: in ShiftDirection_t;
			extend: in std_logic;
			result: out BusWidth_t
		);
	end component;

	component ArithmeticLogicUnit is
		generic(
			enable_M	:integer:=0
			);
		port (
			clock:in Clock_t;
			from_e: in Intcon_E2ALU;
			to_e: out Intcon_ALU2E
		);
	end component;

	component BranchUnit is
		port (
			from_m: in Intcon_M2BU;
			to_hu: out Intcon_BU2HU;
			to_pc: out Intcon_BU2PC
		);
	end component;

	component Multiply is
		port (
			clock:in Clock_t;
			from_alu:in Intcon_ALU2MUL;
			to_alu:out Intcon_MUL2ALU
		);
	end component;

	component Bitmanip is
	port (
        from_alu:in Intcon_ALU2B;
        to_alu:out Intcon_B2ALU
	);
	end component;
	---------------------------------------------------------------------
	-- Memory components

	component GeneralPurposeRegister is
		generic (
			register_bypass      : std_logic := '1'--1 is enable, 0 disable (for singlecycle operation)      
			);
		port (
			clock: in Clock_t;

			from_d: in Intcon_D2GPR;
			from_w: in Intcon_W2GPR;
			to_d: out Intcon_GPR2D
		);
	end component;

	component CombinedMemory is
		port (
			clock: in Clock_t;
			
			from_f: in Intcon_F2IM;
			from_m: in Intcon_M2DM;
			from_dm: in Intcon_DM2Vo;
			from_im: in Intcon_DM2Vo;			
			to_f: out Intcon_IM2F;
			to_m: out Intcon_DM2M;
			to_dm: out Intcon_DM2Vi;
			to_im: out Intcon_DM2Vi			
		);
	end component;

	---------------------------------------------------------------------
	-- Top level components
---------------------------------------------------------------------
	-- interconnect
		component InterconnectPipeline is
		generic (
			enable_M	:integer;
			debug      	:integer; --1 is enable, 0 disable 
			unalign		:integer
			);
		port (
			clk: in std_logic;
			rst: in std_logic;
			trapped: out std_logic_vector(5 downto 0);

			addr_hm: out  std_logic_vector(31 downto 0);
			din_hm: out  std_logic_vector(31 downto 0);
			en_hm: out std_logic;
			rst_hm: out std_logic;
			we_hm: out std_logic_vector(3 downto 0);
			dsel: in std_logic_vector(7 downto 0);

			addr_dm: out std_logic_vector(31 downto 0);
			din_dm: out  std_logic_vector(31 downto 0);
			dout_dm: in  std_logic_vector(31 downto 0);
			en_dm: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
			rst_dm: out std_logic;--! resets output. Fixed '0' should suffice.	
			we_dm: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
			delayed_by_clk_dm: in std_logic;
			wait_dm: in std_logic;
			req_dm: out std_logic;
			
			addr_im: out  std_logic_vector(31 downto 0);
			din_im: out  std_logic_vector(31 downto 0);
			dout_im: in  std_logic_vector(31 downto 0);
			en_im: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
			rst_im: out std_logic;--! resets output. Fixed '0' should suffice.	
			we_im: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
			delayed_by_clk_im: in std_logic;
			wait_im: in std_logic;
			req_im: out std_logic;

		pause: in std_logic;
		issue: in std_logic;
		step: in std_logic;		
		debug_instr: in std_logic_vector(31 downto 0);
		result: out std_logic_vector(31 downto 0)
		);
	end component;

	component InterconnectSimple is
		generic (
			debug      	:integer; --1 is enable, 0 disable 
			unalign		:integer
			);
		port (
			clk: in std_logic;
			rst: in std_logic;
			trapped: out std_logic_vector(5 downto 0);

			addr_hm: out  std_logic_vector(31 downto 0);
			din_hm: out  std_logic_vector(31 downto 0);
			en_hm: out std_logic;
			rst_hm: out std_logic;
			we_hm: out std_logic_vector(3 downto 0);
			dsel: in std_logic_vector(7 downto 0);

			addr_dm: out std_logic_vector(31 downto 0);
			din_dm: out  std_logic_vector(31 downto 0);
			dout_dm: in  std_logic_vector(31 downto 0);
			en_dm: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
			rst_dm: out std_logic;--! resets output. Fixed '0' should suffice.	
			we_dm: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
			delayed_by_clk_dm: in std_logic;
			
			addr_im: out  std_logic_vector(31 downto 0);
			din_im: out  std_logic_vector(31 downto 0);
			dout_im: in  std_logic_vector(31 downto 0);
			en_im: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
			rst_im: out std_logic;--! resets output. Fixed '0' should suffice.	
			we_im: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
			delayed_by_clk_im: in std_logic
		);
	end component;

	component InterconnectIce40 is
	generic (
    	debug      	:integer; --1 is enable, 0 disable 
		unalign		:integer
    	);
	port (
		clk: in std_logic;
		rst: in std_logic;
		trapped: out std_logic_vector(5 downto 0);

		addr_hm: out  std_logic_vector(31 downto 0);
		din_hm: out  std_logic_vector(31 downto 0);
		en_hm: out std_logic;rst_hm: out std_logic;
		we_hm: out std_logic_vector(3 downto 0);
		dsel: in std_logic_vector(7 downto 0);

		addr_dm: out std_logic_vector(31 downto 0);
		din_dm: out  std_logic_vector(31 downto 0);
		dout_dm: in  std_logic_vector(31 downto 0);
		en_dm: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
		rst_dm: out std_logic;--! resets output. Fixed '0' should suffice.	
		we_dm: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
		delayed_by_clk_dm: in std_logic;
		wait_dm: in std_logic;
		
		addr_im: out  std_logic_vector(31 downto 0);
		din_im: out  std_logic_vector(31 downto 0);
		dout_im: in  std_logic_vector(31 downto 0);
		en_im: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
		rst_im: out std_logic;--! resets output. Fixed '0' should suffice.	
		we_im: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
		delayed_by_clk_im: in std_logic
	);
	end component;

	---------------------------------------------------------------------
	-- Target components
	component GeneralPurposeRegister_Target is
		generic (
			register_bypass      : std_logic := '1'--1 is enable, 0 disable (for singlecycle operation)      
			);
		port (
			clock: in Clock_t;

			addr_1: in RegSel_t;
			read_1: out BusWidth_t;
			
			addr_2: in RegSel_t;
			read_2: out BusWidth_t;

			addr_3: in RegSel_t;
			write_enable_3: in std_logic;
			write_3: in BusWidth_t
		);
	end component;

	component CombinedMemory_Target is
		generic (
			dump_mem:	boolean;
			output_filename:string;
			delayed      	:std_logic
			);
		port (
			clock: in Clock_t;

			addr_1: in BusWidth_t;
			read_1: out BusWidth_t;
			write_1: in BusWidth_t;
			web: in std_logic_vector(3 downto 0);

			addr_2: in BusWidth_t;
			read_2: out BusWidth_t
		);
	end component;

	component CombinedMemory_Delayed is--actually (synthetisable) ice40 mem, TODO rename
	port (
		clock: in Clock_t;

		addr_1: in BusWidth_t;
		read_1: out BusWidth_t;
		write_1: in BusWidth_t;
		web: in std_logic_vector(3 downto 0);

		addr_2: in BusWidth_t;
		read_2: out BusWidth_t
	);
	end component;
	---------------------------------------------------------------------
	-- Wrappers

	component WrapperSPI is
	generic (
	axi      	:integer;
	simple		:integer;
	debug      	:integer; 
	unalign		:integer
		);
	port (
		clk: in std_logic;
		rst: in std_logic;
		trapped: out std_logic_vector(5 downto 0);

		addr_hm: out  std_logic_vector(31 downto 0);
		din_hm: out  std_logic_vector(31 downto 0);
		en_hm: out std_logic;rst_hm: out std_logic;
		we_hm: out std_logic_vector(3 downto 0);
		dsel: in std_logic_vector(7 downto 0);

		addr_dm: out std_logic_vector(31 downto 0);
		din_dm: out  std_logic_vector(31 downto 0);
		dout_dm: in  std_logic_vector(31 downto 0);
		en_dm: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
		rst_dm: out std_logic;--! resets output. Fixed '0' should suffice.	
		we_dm: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
		delayed_dm: in std_logic;
		
		addr_im: out  std_logic_vector(31 downto 0);
		din_im: out  std_logic_vector(31 downto 0);
		dout_im: in  std_logic_vector(31 downto 0);
		en_im: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
		rst_im: out std_logic;--! resets output. Fixed '0' should suffice.	
		we_im: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
		delayed_im: in std_logic;

		spi0_mosi:out std_logic;--master port to psram 		
		spi0_miso:in std_logic;
		spi0_sclk:out std_logic;
		spi0_cs0:out std_logic;

		SPI_CSL:in std_logic;--slave port to riscv
		SPI_MOSI:in std_logic;
		SPI_MISO:out std_logic;
		SPI_SCLK:in std_logic
	);
	end component;

	component WrapperSPIMemd is
	generic (
		axi      	:integer:=1;
		simple		:integer:=0;
		debug      	:integer:=0; 
		unalign		:integer:=0
		);
	port (
		clk_12MHz:in std_logic;	

		spi0_mosi:out std_logic;--master port to psram 		
		spi0_miso:in std_logic;
		spi0_sclk:out std_logic;
		spi0_cs0:out std_logic;

		SPI_CSL:in std_logic;--slave port to riscv
		SPI_MOSI:in std_logic;
		SPI_MISO:out std_logic;
		SPI_SCLK:in std_logic;

		RGB0: out std_logic;
		RGB1: out std_logic;
		RGB2: out std_logic
	);
	end component;
	---------------------------------------------------------------------
	-- Constants

	constant BUSWIDTH_ZERO: BusWidth_t := (others => '0');
	constant BUSWIDTH_ONE: BusWidth_t := x"00000001";

	constant RESET_VECTOR_STACK: BusWidth_t := BUSWIDTH_ZERO;
	constant RESET_VECTOR_RESET: BusWidth_t := x"00000004";
	
	constant REGSEL_ZERO: RegSel_t := (others => '0');
	constant REGSEL_STACK: RegSel_t := "00010";

	constant CONTROL_UNIT_ZERO: ControlUnit_t := (
			branch => BCtrl_None,
			reg_write => '0',
			mem_to_reg => '0',
			mem_write => '0',
			mem_control => MemoryControl_Word,
			pc_plus_imm_to_reg => '0',
			trap => Trap_None
		);
	
	constant PC2F_ZERO: Intcon_PC2F := (
		pc => BUSWIDTH_ZERO
	);
	constant F2D_ZERO: Intcon_F2D := (
		pc => BUSWIDTH_ZERO,
		pc_plus_4 => BUSWIDTH_ZERO,
		instruction => BUSWIDTH_ZERO,
		delayed_by_clk => '0'
	);
	constant D2E_ZERO: Intcon_D2E := (
		pc => BUSWIDTH_ZERO,
		pc_plus_4 => BUSWIDTH_ZERO,
		rs1 => REGSEL_ZERO,
		rs2 => REGSEL_ZERO,
		rd => REGSEL_ZERO,
		val1 => BUSWIDTH_ZERO,
		val2 => BUSWIDTH_ZERO,
		vali => BUSWIDTH_ZERO,
		control => CONTROL_UNIT_ZERO,
		alu_control => AluControl_ADD,
		alu_source => AluSource_Reg,
		multype => MulDivOp_Mul
	);
	constant E2M_ZERO: Intcon_E2M := (
		pc_plus_4 => BUSWIDTH_ZERO,
		pc_plus_imm => BUSWIDTH_ZERO,
		alu_out => BUSWIDTH_ZERO,
		alu_flag => (
			zero => '0'
		),
		write_data => BUSWIDTH_ZERO,
		rd => REGSEL_ZERO,
		control => CONTROL_UNIT_ZERO
	);
	constant M2W_ZERO: Intcon_M2W := (
		read_data => BUSWIDTH_ZERO,
		result_data => BUSWIDTH_ZERO,
		rd => REGSEL_ZERO,
		reg_write => '0',
		mem_to_reg => '0',
		delayed_by_clk => '0'
	);

	constant D2GPR_ZERO: Intcon_D2GPR := (
		rs1 => REGSEL_ZERO,
		rs2 => REGSEL_ZERO
	);
	constant W2GPR_ZERO: Intcon_W2GPR := (
		result => BUSWIDTH_ZERO,
		rd => REGSEL_ZERO,
		reg_write => '0'
	);
	constant M2DM_ZERO: Intcon_M2DM := (
		alu_out => BUSWIDTH_ZERO,
		write_data => BUSWIDTH_ZERO,
		mem_write => '0',
		mem_bytes => (others=>'0')
	);
	constant DM2M_ZERO: Intcon_DM2M := (
		read_data => BUSWIDTH_ZERO,
		delayed_by_clk => '0',
		mem_wait => '0'
	);

	constant HU2E_ZERO: Intcon_HU2E := (
		forwardA => ForwardValue_None,
		forwardB => ForwardValue_None
	);

	constant I2F_ZERO:Intcon_I2F:=(
		pause => '0',
		issue => '0',
		step => '0',
--		issue_old => '0',
		instruction=>BUSWIDTH_ZERO
		);
	constant F2HU_ZERO: Intcon_F2HU := (
		mem_wait => '0'
	);

	constant M2M_ZERO: Intcon_M2M := (
		addrdelayer => BUSWIDTH_ZERO,
		datadelayer => BUSWIDTH_ZERO,
		step => (others=>'0'),
		mem_control => MemoryControl_Word,
		no_delay => '0',
		bitreg => (others=>'0')
		);

	---------------------------------------------------------------------
	-- Functions

	function make_instruction_R(
		opcode: std_logic_vector(6 downto 0);
		rd: RegSel_t;
		funct3: std_logic_vector(2 downto 0);
		rs1: RegSel_t;
		rs2: RegSel_t;
		funct7: std_logic_vector(6 downto 0)
	) return BusWidth_t;

	function make_instruction_I(
		opcode: std_logic_vector(6 downto 0);
		rd: RegSel_t;
		funct3: std_logic_vector(2 downto 0);
		rs1: RegSel_t;
		imm: std_logic_vector(11 downto 0)
	) return BusWidth_t;

	function make_instruction_S(
		opcode: std_logic_vector(6 downto 0);
		funct3: std_logic_vector(2 downto 0);
		rs1: RegSel_t;
		rs2: RegSel_t;
		imm: std_logic_vector(11 downto 0)
	) return BusWidth_t;

	function make_instruction_B(
		opcode: std_logic_vector(6 downto 0);
		funct3: std_logic_vector(2 downto 0);
		rs1: RegSel_t;
		rs2: RegSel_t;
		imm: std_logic_vector(12 downto 1)
	) return BusWidth_t;

	function make_instruction_U(
		opcode: std_logic_vector(6 downto 0);
		rd: RegSel_t;
		imm: std_logic_vector(31 downto 12)
	) return BusWidth_t;

	function make_instruction_J(
		opcode: std_logic_vector(6 downto 0);
		rd: RegSel_t;
		imm: std_logic_vector(20 downto 1)
	) return BusWidth_t;

	function shift_left(
		value: BusWidth_t;
		extend: std_logic
	) return BusWidth_t;

	function shift_right(
		value: BusWidth_t;
		extend: std_logic
	) return BusWidth_t;

	function trap_enum2vec(
		enum: Trap_t
	) return std_logic_vector;

	function datashift(ctrl: MemoryControl_t;mask:BusWidth_t) return BusWidth_t;
	function memctrl2mask(ctrl: MemoryControl_t) return std_logic_vector;
	function reverse_vector(a: in std_logic_vector)return std_logic_vector; 
	function reverse_endian(a: in BusWidth_t)return BusWidth_t;
	function extender(ctrl: MemoryControl_t;data:BusWidth_t) return BusWidth_t;
	function clz32(value: BusWidth_t)return std_logic_vector;
	PROCEDURE leading_zeroes32( m : IN STD_LOGIC_VECTOR (31 DOWNTO 0);n : IN UNSIGNED;VARIABLE r : OUT STD_LOGIC_VECTOR (31 DOWNTO 0) );
end package rvapo_pkg;

package body rvapo_pkg is
		function datashift(ctrl: MemoryControl_t;mask:BusWidth_t)return BusWidth_t is	
		variable result: BusWidth_t;
		begin
			case ctrl is
				when MemoryControl_Word =>result:=	mask; 
				when MemoryControl_Half =>result:=	mask(15 downto 0)&x"0000";
				when MemoryControl_HalfUnsigned =>result:=mask(15 downto 0)&x"0000";
				--when MemoryControl_Byte =>result:=	"0001";	
				--when MemoryControl_ByteUnsigned =>result:= "0001";
			when others => result:= mask(7 downto 0)&x"000000";
			end case;
			return reverse_endian(mask);--result;
		end function;

	function memctrl2mask(ctrl: MemoryControl_t) return std_logic_vector is
		variable result:std_logic_vector(3 downto 0);
		begin
		case ctrl is
			when MemoryControl_Word =>result:=	"1111"; 
			when MemoryControl_Half =>result:=	"1100";--"0011";
			when MemoryControl_HalfUnsigned =>result:= "1100";--"0011";	
			--when MemoryControl_Byte =>result:=	"0001";	
			--when MemoryControl_ByteUnsigned =>result:= "0001";
			when others => result:= "1000";--"0001";
		end case;
		return result;
		end function;	

	function reverse_vector (a: in std_logic_vector)return std_logic_vector is
  		variable result: std_logic_vector(a'RANGE);
  		alias aa: std_logic_vector(a'REVERSE_RANGE) is a;
		begin
		  for i in aa'RANGE loop
		    result(i) := aa(i);
		  end loop;
		  return result;
		end; 

	function reverse_endian(a: in BusWidth_t)return BusWidth_t is
		variable result:BusWidth_t;
		begin
			result(7 downto 0):=a(31 downto 24);		
			result(15 downto 8):=a(23 downto 16);
			result(23 downto 16):=a(15 downto 8);
			result(31 downto 24):=a(7 downto 0);
			return result;
		end function;	

	function extender(ctrl: MemoryControl_t;data:BusWidth_t) return BusWidth_t is
		variable result:BusWidth_t;
		begin
			case ctrl is
				when MemoryControl_Byte =>
					result(31 downto 8) := (others => data(7));
					result(7 downto 0) := data(7 downto 0);
				when MemoryControl_Half =>
					result(31 downto 16) := (others => data(15));
					result(15 downto 0) := data(15 downto 0);
				when MemoryControl_Word =>
					result := data;
				when MemoryControl_ByteUnsigned =>
					result(31 downto 8) := (others => '0');
					result(7 downto 0) := data(7 downto 0);
				when MemoryControl_HalfUnsigned =>
					result(31 downto 16) := (others => '0');
					result(15 downto 0) := data(15 downto 0);
			end case;
			return result;
		end function;	

	function trap_enum2vec(
		enum: Trap_t
	)return std_logic_vector is
		variable result: std_logic_vector(5 downto 0);
	begin
		case enum is
			when Trap_Break=>result:="000001";
			when Trap_Call=>result:="000010";
			when Trap_Fault=>result:="000011";
			when Trap_None=>result:=(others=>'0');
		end case;
		return result;		
	end function;

	------------------------------------------INSTRUCTION BUILDING START; TYPES R,I,S,B,U,J------------------------------------------------
	function make_instruction_R(
		opcode: std_logic_vector(6 downto 0);
		rd: RegSel_t;
		funct3: std_logic_vector(2 downto 0);
		rs1: RegSel_t;
		rs2: RegSel_t;
		funct7: std_logic_vector(6 downto 0)
	) return BusWidth_t is
		variable result: BusWidth_t;
	begin
		result(6 downto 0) := opcode;
		result(11 downto 7) := rd;
		result(14 downto 12) := funct3;
		result(19 downto 15) := rs1;
		result(24 downto 20) := rs2;
		result(31 downto 25) := funct7;

		return result;
	end function;

	function make_instruction_I(
		opcode: std_logic_vector(6 downto 0);
		rd: RegSel_t;
		funct3: std_logic_vector(2 downto 0);
		rs1: RegSel_t;
		imm: std_logic_vector(11 downto 0)
	) return BusWidth_t is
		variable result: BusWidth_t;
	begin
		result(6 downto 0) := opcode;
		result(11 downto 7) := rd;
		result(14 downto 12) := funct3;
		result(19 downto 15) := rs1;
		result(31 downto 20) := imm;

		return result;
	end function;

	function make_instruction_S(
		opcode: std_logic_vector(6 downto 0);
		funct3: std_logic_vector(2 downto 0);
		rs1: RegSel_t;
		rs2: RegSel_t;
		imm: std_logic_vector(11 downto 0)
	) return BusWidth_t is
		variable result: BusWidth_t;
	begin
		result(6 downto 0) := opcode;
		result(11 downto 7) := imm(4 downto 0);
		result(14 downto 12) := funct3;
		result(19 downto 15) := rs1;
		result(24 downto 20) := rs2;
		result(31 downto 25) := imm(11 downto 5);

		return result;
	end function;

	function make_instruction_B(
		opcode: std_logic_vector(6 downto 0);
		funct3: std_logic_vector(2 downto 0);
		rs1: RegSel_t;
		rs2: RegSel_t;
		imm: std_logic_vector(12 downto 1)
	) return BusWidth_t is
		variable result: BusWidth_t;
	begin
		result(6 downto 0) := opcode;
		result(7) := imm(11);
		result(11 downto 8) := imm(4 downto 1);
		result(14 downto 12) := funct3;
		result(19 downto 15) := rs1;
		result(24 downto 20) := rs2;
		result(30 downto 25) := imm(10 downto 5);
		result(31) := imm(12);

		return result;
	end function;

	function make_instruction_U(
		opcode: std_logic_vector(6 downto 0);
		rd: RegSel_t;
		imm: std_logic_vector(31 downto 12)
	) return BusWidth_t is
		variable result: BusWidth_t;
	begin
		result(6 downto 0) := opcode;
		result(11 downto 7) := rd;
		result(31 downto 12) := imm;

		return result;
	end function;

	function make_instruction_J(
		opcode: std_logic_vector(6 downto 0);
		rd: RegSel_t;
		imm: std_logic_vector(20 downto 1)
	) return BusWidth_t is
		variable result: BusWidth_t;
	begin
		result(6 downto 0) := opcode;
		result(11 downto 7) := rd;
		result(19 downto 12) := imm(19 downto 12);
		result(20) := imm(11);
		result(30 downto 21) := imm(10 downto 1);
		result(31) := imm(20);

		return result;
	end function;

	function shift_left(
		value: BusWidth_t;
		extend: std_logic
	) return BusWidth_t is
		variable extend_bit: std_logic;
	begin
		if extend = '1' then
			extend_bit := value(0);
		else
			extend_bit := '0';
		end if;

		return value(30 downto 0) & extend_bit;
	end function;

	function shift_right(
		value: BusWidth_t;
		extend: std_logic
	) return BusWidth_t is
		variable extend_bit: std_logic;
	begin
		if extend = '1' then
			extend_bit := value(31);
		else
			extend_bit := '0';
		end if;

		return extend_bit & value(31 downto 1);
	end function;

	function clz32(
		value: BusWidth_t
	) return std_logic_vector is
		variable res_v: std_logic_vector(5 downto 0);
	begin
		if value(31)='1' then return "000000";
		elsif value(30)='1' then return "000001";
		elsif value(29)='1' then return "000010";
		elsif value(28)='1' then return "000011";
		elsif value(27)='1' then return "000100";
		elsif value(26)='1' then return "000101";
		elsif value(25)='1' then return "000110";
		elsif value(24)='1' then return "000111";
		elsif value(23)='1' then return "001000";
		elsif value(22)='1' then return "001001";
		elsif value(21)='1' then return "001010";
		elsif value(20)='1' then return "001011";
		elsif value(19)='1' then return "001100";
		elsif value(18)='1' then return "001101";
		elsif value(17)='1' then return "001110";
		elsif value(16)='1' then return "001111";
		elsif value(15)='1' then return "010000";
		elsif value(14)='1' then return "010001";
		elsif value(13)='1' then return "010010";
		elsif value(12)='1' then return "010011";
		elsif value(11)='1' then return "010100";
		elsif value(10)='1' then return "010101";
		elsif value(9)='1' then return "010110";
		elsif value(8)='1' then return "010111";
		elsif value(7)='1' then return "011000";
		elsif value(6)='1' then return "011001";
		elsif value(5)='1' then return "011010";
		elsif value(4)='1' then return "011011";
		elsif value(3)='1' then return "011100";
		elsif value(2)='1' then return "011101";
		elsif value(1)='1' then return "011110";
		elsif value(0)='1' then return "011111";
		else return "100000";	
		end if;
--CLZ_GENERATE:for i in 0 to 31 generate		
--end generate CLZ_GENERATE;	
	--	for i in 0 to 31 loop
	--		if value(i)='1' then return std_logic_vector(31-to_unsigned(i,6)); end if;
	--	end loop;	
	--	return "100000";	
	--	return extend_bit & value(31 downto 1);
	end function;

	--from https://gitlab.com/pikron/projects/lx_cpu/tumbl/-/blob/master/hw/mbl_pkg.vhd?ref_type=heads	
	PROCEDURE leading_zeroes32( m : IN STD_LOGIC_VECTOR (31 DOWNTO 0);
	                            n : IN UNSIGNED;
                               VARIABLE r : OUT STD_LOGIC_VECTOR (31 DOWNTO 0) ) IS
	BEGIN
		-- It would be easier if it could be generated, but it's a sequence
		IF ( m(31) = '1' ) THEN
		  r := STD_LOGIC_VECTOR ( n );
		ELSE
		  leading_zeroes32 ( m (30 DOWNTO 0) & '1', UNSIGNED (n + 1), r);
		END IF;
	END PROCEDURE leading_zeroes32;
end;
