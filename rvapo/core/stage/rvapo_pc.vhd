library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity StageProgramCounter is
	port (
		from_f: in Intcon_F2PC;
		from_bu: in Intcon_BU2PC;
		to_f: out Intcon_PC2F
	);
end;

architecture rtl of StageProgramCounter is
begin
	process (from_f, from_bu)
	begin
		if from_bu.pc_override = '1' then
			to_f.pc <= from_bu.pc_value;
		else
			to_f.pc <= from_f.pc_plus_4;
		end if;
	end process;
end;