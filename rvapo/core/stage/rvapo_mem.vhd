library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity StageMemory is
	port (
		from_e: in Intcon_E2M;
		from_dm: in Intcon_DM2M;
		to_hu: out Intcon_M2HU;
		to_bu: out Intcon_M2BU;
		to_dm: out Intcon_M2DM;
		to_e: out Intcon_M2E;
		to_w: out Intcon_M2W
	);
end;

architecture rtl of StageMemory is
	signal result_data: BusWidth_t;
	signal read_data: BusWidth_t;
	signal memadr: BusWidth_t:= (others => '0');
	signal web: std_logic_vector(3 downto 0);
begin
	to_hu <= (
		from_e.rd,
		from_e.control.reg_write,
		from_e.control.trap,
		from_dm.mem_wait,
		'0'--unalign is ignored in this version
	);

	to_bu <= (
		from_e.alu_out,
		from_e.alu_flag,
		from_e.pc_plus_imm,
		from_e.control.branch
	);

	to_dm <= (
		memadr,
		from_e.write_data,
		from_e.control.mem_write,
		web
	);

	to_e <= (
		result_data => result_data
	);

	memadr<=from_e.alu_out when (from_e.control.mem_write or from_e.control.mem_to_reg)='1' else (others=>'0');

	to_w <= (
		read_data,
		result_data,
		from_e.rd,
		from_e.control.reg_write,
		from_e.control.mem_to_reg,
		(from_dm.delayed_by_clk and from_e.control.mem_to_reg)--TODO: should this and not be removed just like in unalign?
	);

	read: process (from_e, from_dm)
	begin
		case from_e.control.mem_control is
			when MemoryControl_Byte =>
				read_data(31 downto 8) <= (others => from_dm.read_data(7));
				read_data(7 downto 0) <= from_dm.read_data(7 downto 0);
			when MemoryControl_Half =>
				read_data(31 downto 16) <= (others => from_dm.read_data(15));
				read_data(15 downto 0) <= from_dm.read_data(15 downto 0);
			when MemoryControl_Word =>
				read_data <= from_dm.read_data;
			when MemoryControl_ByteUnsigned =>
				read_data(31 downto 8) <= (others => '0');
				read_data(7 downto 0) <= from_dm.read_data(7 downto 0);
			when MemoryControl_HalfUnsigned =>
				read_data(31 downto 16) <= (others => '0');
				read_data(15 downto 0) <= from_dm.read_data(15 downto 0);
		end case;
	end process;

	write_ctrl: process (from_e)
	begin
		case from_e.control.mem_control is
			when MemoryControl_Byte | MemoryControl_ByteUnsigned =>
				web<="0001";
			when MemoryControl_Half | MemoryControl_HalfUnsigned =>
				web<="0011";
			when MemoryControl_Word =>
				web<="1111";
		end case;
	end process;


	process (from_e, from_dm)
	begin
		if from_e.control.branch = BCtrl_JAL or from_e.control.branch = BCtrl_JALR then
			result_data <= from_e.pc_plus_4;
		elsif from_e.control.pc_plus_imm_to_reg = '1' then
			result_data <= from_e.pc_plus_imm;
		else 
			result_data <= from_e.alu_out;
		end if;
	end process;
end;
