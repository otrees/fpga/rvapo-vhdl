library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;

entity StageFetch is
	port (
		from_pc: in Intcon_PC2F;
		from_im: in Intcon_IM2F;
		from_i: in Intcon_I2F;
		to_hu: out Intcon_F2HU;
		to_im: out Intcon_F2IM;
		to_pc: out Intcon_F2PC;
		to_d: out Intcon_F2D
	);
end;

architecture rtl of StageFetch is
begin
	to_hu.mem_wait<=from_im.mem_wait;

	process (from_pc, from_im,from_i)
		variable instruction:BusWidth_t:=(others=>'0');
		variable pc_plus_4: BusWidth_t;
		variable pc_less_4: BusWidth_t;
	begin
		pc_plus_4 := std_logic_vector(unsigned(from_pc.pc) + 4);		
		pc_less_4 := std_logic_vector(unsigned(from_pc.pc) - 4);		

		if from_i.pause='1' and from_i.step='0' then
			if from_i.issue='1' then --and from_i.issue_old='0' then
				instruction:=from_i.instruction;
			else
				instruction:=(others=>'0');	
			end if;
			to_pc <= (pc_plus_4 => from_pc.pc);
			to_im <= (pc => pc_less_4);	

			to_d <= (
				from_pc.pc,
				pc_plus_4,
				instruction,
				'0'--in debug mode memory is not used, so instruction is gained without delay
				);
		else
			instruction:=from_im.instruction;
			to_pc <= (pc_plus_4 => pc_plus_4);	
			to_im <= (pc => from_pc.pc);
		
			to_d <= (
				from_pc.pc,
				pc_plus_4,
				instruction,
				from_im.delayed_by_clk
				);
		end if;			

		
	end process;
end;