library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;

entity StageDecode is
	generic (
        enable_M	:integer:=0
    	);
	port (
		from_f: in Intcon_F2D;
		from_gpr: in Intcon_GPR2D;
		to_hu: out Intcon_D2HU;
		to_gpr: out Intcon_D2GPR;
		to_e: out Intcon_D2E
	);
end;

architecture rtl of StageDecode is
	type InstructionType_t is (
		InstructionType_None,
		InstructionType_R,
		InstructionType_I,
		InstructionType_S,
		InstructionType_B,
		InstructionType_U,
		InstructionType_J
	);

	signal result: Intcon_D2E;
	signal instruction_type: InstructionType_t;
begin
	result.pc <= from_f.pc;
	result.pc_plus_4 <= from_f.pc_plus_4;

	result.val1 <= from_gpr.val1;
	result.val2 <= from_gpr.val2;

	to_hu <= (result.rs1, result.rs2);
	to_gpr <= (result.rs1, result.rs2);

	to_e <= result;

	control_unit: process (from_f,instruction_type,result)
		variable opcode: std_logic_vector(6 downto 0);
		variable funct3: std_logic_vector(2 downto 0);
		variable funct7: std_logic_vector(6 downto 0);

		variable is_immediate: std_logic;
	begin
		opcode := from_f.instruction(6 downto 0);
		funct3 := from_f.instruction(14 downto 12);
		funct7 := from_f.instruction(31 downto 25);

		result.control <= (
			branch => BCtrl_None,
			reg_write => '0',
			mem_to_reg => '0',
			mem_write => '0',
			mem_control => MemoryControl_Word,
			pc_plus_imm_to_reg => '0',
			trap => Trap_None
		);

		result.alu_control <= AluControl_ADD; -- don't care
		result.alu_source <= AluSource_Imm; -- don't care
		if enable_M=1 then
			case funct3 is--differentiates instructions in M. We use separate signal so that it can be set always.
				when "000" =>result.multype<=MulDivOp_Mul;
				when "001" =>result.multype<=MulDivOp_MulH;
				when "010" =>result.multype<=MulDivOp_MulHSU;
				when "011" =>result.multype<=MulDivOp_MulHU;
				when "100" =>result.multype<=MulDivOp_Div;
				when "101" =>result.multype<=MulDivOp_DivU;
				when "110" =>result.multype<=MulDivOp_Rem;
				when "111" =>result.multype<=MulDivOp_RemU;
				when others =>result.control.trap <= Trap_Fault;--should not be possible
			end case;			
		else
			result.multype<=MulDivOp_Mul;
		end if;
		case opcode is
			-- JAL
			when "1101111" =>
				instruction_type <= InstructionType_J;

				result.control.branch <= BCtrl_JAL;
				result.control.reg_write <= '1';
			-- JALR
			when "1100111" =>
				instruction_type <= InstructionType_I;

				result.control.branch <= BCtrl_JALR;
				result.control.reg_write <= '1';
			-- BRANCH
			when "1100011" =>
				instruction_type <= InstructionType_B;

				result.alu_source <= AluSource_Reg;

				case funct3 is
					when "000" =>
						result.alu_control <= AluControl_SUB;
						result.control.branch <= BCtrl_BEQ;
					when "001" =>
						result.alu_control <= AluControl_SUB;
						result.control.branch <= BCtrl_BNE;
					when "100" =>
						result.alu_control <= AluControl_SLT;
						result.control.branch <= BCtrl_BLT;
					when "101" =>
						result.alu_control <= AluControl_SLT;
						result.control.branch <= BCtrl_BGE;
					when "110" =>
						result.alu_control <= AluControl_SLTU;
						result.control.branch <= BCtrl_BLTU;
					when "111" =>
						result.alu_control <= AluControl_SLTU;
						result.control.branch <= BCtrl_BGEU;
					when others => result.control.trap <= Trap_Fault;
				end case;

			-- LOAD
			when "0000011" =>
				instruction_type <= InstructionType_I;

				result.control.reg_write <= '1';
				result.control.mem_to_reg <= '1';

				case funct3 is 
					when "000" => result.control.mem_control <= MemoryControl_Byte;
					when "001" => result.control.mem_control <= MemoryControl_Half;
					when "010" => result.control.mem_control <= MemoryControl_Word;
					when "100" => result.control.mem_control <= MemoryControl_ByteUnsigned;
					when "101" => result.control.mem_control <= MemoryControl_HalfUnsigned;
					when others => result.control.trap <= Trap_Fault;
				end case;

			-- STORE
			when "0100011" =>
				instruction_type <= InstructionType_S;

				result.control.mem_write <= '1';

				case funct3 is
					when "000" => result.control.mem_control <= MemoryControl_Byte;
					when "001" => result.control.mem_control <= MemoryControl_Half;
					when "010" => result.control.mem_control <= MemoryControl_Word;
					when others => result.control.trap <= Trap_Fault;
				end case;
			-- LUI
			when "0110111" =>
				instruction_type <= InstructionType_U;
				result.control.reg_write <= '1';
			-- AUIPC
			when "0010111" =>
				instruction_type <= InstructionType_U;
				result.control.reg_write <= '1';
				result.control.pc_plus_imm_to_reg <= '1';
			-- OP-IMM, OP
			when "0010011" | "0110011" =>
				-- "0010011" is OP-IMM, "0110011" is OP
				is_immediate := not opcode(5);

				result.control.reg_write <= '1';

				if is_immediate = '1' then
					-- OP-IMM
					instruction_type <= InstructionType_I;
					result.alu_source <= AluSource_Imm;
				else
					-- OP
					instruction_type <= InstructionType_R;
					result.alu_source <= AluSource_Reg;
				end if;				

				if funct7="0000001" and instruction_type=InstructionType_R and enable_M=1 then --M extension (it has just one opcode)
					result.alu_control<=AluControl_MUL;	
				else
					case funct3 is
						when "000" =>
							-- ADDI, ADD, SUB

							if is_immediate = '1' or funct7 = "0000000" then
								result.alu_control <= AluControl_ADD;
							elsif funct7 = "0100000" then
								result.alu_control <= AluControl_SUB;
							else
								result.control.trap <= Trap_Fault;
							end if;

						when "001" =>
							-- SLLI, SLL
							if is_immediate='1' and funct7 = "0110000" and result.rs2=REGSEL_ZERO then
								result.alu_control <= AluControl_CLZ;
							elsif funct7 = "0000000" then
								result.alu_control <= AluControl_SLL;
							else
								result.control.trap <= Trap_Fault;
							end if;

						when "010" =>
							-- SLTI, SLT

							if is_immediate = '1' or funct7 = "0000000" then
								result.alu_control <= AluControl_SLT;
							else
								result.control.trap <= Trap_Fault;
							end if;

						when "011" =>
							-- SLTUI, SLTU

							if is_immediate = '1' or funct7 = "0000000" then
								result.alu_control <= AluControl_SLTU;
							else
								result.control.trap <= Trap_Fault;
							end if;

						when "100" =>
							-- XORI, XOR

							if is_immediate = '1' or funct7 = "0000000" then
								result.alu_control <= AluControl_XOR;
							else
								result.control.trap <= Trap_Fault;
							end if;

						when "101" =>
							-- SRLI, SRAI, SRL, SRA

							if funct7 = "0000000" then
								result.alu_control <= AluControl_SRL;
							elsif funct7 = "0100000" then
								result.alu_control <= AluControl_SRA;
							else
								result.control.trap <= Trap_Fault;
							end if;

						when "110" =>
							-- ORI, OR

							if is_immediate = '1' or funct7 = "0000000" then
								result.alu_control <= AluControl_OR;
							else
								result.control.trap <= Trap_Fault;
							end if;

						when "111" =>
							-- ANDI, AND

							if is_immediate = '1' or funct7 = "0000000" then
								result.alu_control <= AluControl_AND;
							else
								result.control.trap <= Trap_Fault;
							end if;
						
						when others => result.control.trap <= Trap_Fault;
					end case;
				end if;
			-- SYSTEM
			when "1110011" =>
				instruction_type <= InstructionType_I;

				case from_f.instruction(31 downto 20) is
					-- ECALL
					when "000000000000" => result.control.trap <= Trap_Call;
					-- EBREAK
					when "000000000001" => result.control.trap <= Trap_Break; 
					when others => result.control.trap <= Trap_Fault;
				end case;
			-- FENCE
			when "0001111" =>
				instruction_type <= InstructionType_I;
			-- zeroes
			when others =>
				instruction_type <= InstructionType_None;
				if from_f.instruction /= x"00000000" then
					result.control.trap <= Trap_Fault;
				end if;
		end case;
	end process;

	imm_decode: process (from_f, instruction_type)
	begin
		case instruction_type is
			when InstructionType_None | InstructionType_R =>
				result.valI <= (others => '0');

			when InstructionType_I =>
				result.valI(31 downto 11) <= (others => from_f.instruction(31));
				result.valI(10 downto 0) <= from_f.instruction(30 downto 20);
			
			when InstructionType_S =>
				result.valI(31 downto 11) <= (others => from_f.instruction(31));
				result.valI(10 downto 5) <= from_f.instruction(30 downto 25);
				result.valI(4 downto 0) <= from_f.instruction(11 downto 7);

			when InstructionType_B =>
				result.valI(31 downto 12) <= (others => from_f.instruction(31));
				result.valI(11) <= from_f.instruction(7);
				result.valI(10 downto 5) <= from_f.instruction(30 downto 25);
				result.valI(4 downto 1) <= from_f.instruction(11 downto 8);
				result.valI(0) <= '0';

			when InstructionType_U =>
				result.valI(31 downto 12) <= from_f.instruction(31 downto 12);
				result.valI(11 downto 0) <= (others => '0');

			when InstructionType_J =>
				result.valI(31 downto 20) <= (others => from_f.instruction(31));
				result.valI(19 downto 12) <= from_f.instruction(19 downto 12);
				result.valI(11) <= from_f.instruction(20);
				result.valI(10 downto 1) <= from_f.instruction(30 downto 21);
				result.valI(0) <= '0';
		end case;
	end process;

	reg_decode: process (from_f, instruction_type)
	begin
		result.rs1 <= from_f.instruction(19 downto 15);
		result.rs2 <= from_f.instruction(24 downto 20);
		result.rd <= from_f.instruction(11 downto 7);

		case instruction_type is
			when InstructionType_None =>
				result.rs1 <= REGSEL_ZERO;
				result.rs2 <= REGSEL_ZERO;
				result.rd <= REGSEL_ZERO;
			when InstructionType_R => null;		
			when InstructionType_I =>
				result.rs2 <= REGSEL_ZERO;
			when InstructionType_S =>
				result.rd <= REGSEL_ZERO;
			when InstructionType_B =>
				result.rd <= REGSEL_ZERO;
			when InstructionType_U =>
				result.rs1 <= REGSEL_ZERO;
				result.rs2 <= REGSEL_ZERO;
			when InstructionType_J =>
				result.rs1 <= REGSEL_ZERO;
				result.rs2 <= REGSEL_ZERO;
		end case;
	end process;
end;
