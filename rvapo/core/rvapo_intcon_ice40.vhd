library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;
use ieee.numeric_std.all;

entity InterconnectIce40 is
	generic (
    	debug      	:integer; --1 is enable, 0 disable 
		unalign		:integer
    	);
	port (
		clk: in std_logic;
		rst: in std_logic;
		trapped: out std_logic_vector(5 downto 0);

		addr_hm: out  std_logic_vector(31 downto 0);
		din_hm: out  std_logic_vector(31 downto 0);
		en_hm: out std_logic;rst_hm: out std_logic;
		we_hm: out std_logic_vector(3 downto 0);
		dsel: in std_logic_vector(7 downto 0);

		addr_dm: out std_logic_vector(31 downto 0);
		din_dm: out  std_logic_vector(31 downto 0);
		dout_dm: in  std_logic_vector(31 downto 0);
		en_dm: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
		rst_dm: out std_logic;--! resets output. Fixed '0' should suffice.	
		we_dm: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
		delayed_by_clk_dm: in std_logic;
		wait_dm: in std_logic;
		
		addr_im: out  std_logic_vector(31 downto 0);
		din_im: out  std_logic_vector(31 downto 0);
		dout_im: in  std_logic_vector(31 downto 0);
		en_im: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
		rst_im: out std_logic;--! resets output. Fixed '0' should suffice.	
		we_im: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
		delayed_by_clk_im: in std_logic
	);
end entity;

architecture rtl of InterconnectIce40 is
	---------------------------------------------------------------------
	-- pipelined signals --
	signal PC2F_in: Intcon_PC2F;
	signal PC2F_out: Intcon_PC2F;

	signal F2D: Intcon_F2D;
	signal F2D_in: Intcon_F2D;
	signal F2D_out: Intcon_F2D;
	signal D2E: Intcon_D2E;
	signal E2M: Intcon_E2M;
	signal M2W: Intcon_M2W;
	---------------------------------------------------------------------
	-- backward signals --
	signal F2PC: Intcon_F2PC;
	signal BU2PC: Intcon_BU2PC;
	signal M2E: Intcon_M2E;
	signal W2E: Intcon_W2E;
	---------------------------------------------------------------------
	-- hazard signals --
	signal D2HU: Intcon_D2HU;
	signal E2HU: Intcon_E2HU;
	signal M2HU: Intcon_M2HU;
	signal W2HU: Intcon_W2HU;
	signal BU2HU: Intcon_BU2HU;

	signal HU2E: Intcon_HU2E;

	signal M2BU: Intcon_M2BU;
	---------------------------------------------------------------------
	-- data signals (gpr, im, dm, alu) --
	signal E2ALU: Intcon_E2ALU;
	signal ALU2E: Intcon_ALU2E;

	signal F2IM: Intcon_F2IM;
	signal IM2F: Intcon_IM2F;

	signal D2GPR_in: Intcon_D2GPR;
	signal D2GPR_out: Intcon_D2GPR;
	signal GPR2D: Intcon_GPR2D;

	signal M2DM_in: Intcon_M2DM;
	signal M2DM_out: Intcon_M2DM;
	signal DM2M_in: Intcon_DM2M;
	signal DM2M_out: Intcon_DM2M;

	signal W2GPR_in: Intcon_W2GPR;
	signal W2GPR_out: Intcon_W2GPR;
	---------------------------------------------------------------------
	-- interconnect signals --
	type RunState_t is (
		RunState_Begin,
		RunState_SetupStack,
		RunState_SetupPc,
	--	RunState_SetupPc2,
		RunState_Run,
		RunState_Trapped
	);

	signal D2GPR_setup: Intcon_D2GPR;
	signal W2GPR_setup: Intcon_W2GPR;

	signal M2DM_setup: Intcon_M2DM;

	signal run_state: RunState_t:=RunState_Trapped;
	signal hazards: Hazards_t;

	signal to_dm: Intcon_DM2Vi;
	signal to_im: Intcon_DM2Vi;
	signal from_dm: Intcon_DM2Vo;
	signal from_im: Intcon_DM2Vo;

	signal clock: clock_t;
	signal clock_fast: clock_t;

	signal CycleCounter:std_logic_vector(7 downto 0):=(others=>'0');
	signal DebugCounter:std_logic_vector(7 downto 0):=(others=>'0');
	signal DA:std_logic_vector(15 downto 0):=X"1010";


	signal clk_halver:std_logic:='0';
	signal trapped_bp:std_logic_vector(5 downto 0):=(others=>'0');
	signal wait_intercept:std_logic:='0';

	signal bp1B:std_logic_vector(7 downto 0):=(others=>'0');
	signal bp2B:std_logic_vector(15 downto 0):=(others=>'0');
	signal bp3B:std_logic_vector(23 downto 0):=(others=>'0');
	signal bp4B:std_logic_vector(31 downto 0):=(others=>'0');
	signal dmread_delay:std_logic_vector(31 downto 0):=(others=>'0');
begin
	clock.reset <= rst;
	clock.enable <= '1';
	clock.pulse <= clk;

	clock_fast.reset <= rst;
	clock_fast.enable <= '1';
	clock_fast.pulse <= clk;
	---------------------------------------------------------------------
	-- STAGES
	inst_pc: StageProgramCounter port map (
		from_f => F2PC,
		from_bu => BU2PC,
		to_f => PC2F_in
	);

	inst_f: StageFetch port map (
		from_pc => PC2F_out,
		from_im => IM2F,
		from_i => I2F_ZERO,
		to_im => F2IM,
		to_pc => F2PC,
		to_d => F2D_in
	);

	inst_d: StageDecode port map (
		from_f => F2D_out,
		from_gpr => GPR2D,
		to_hu => D2HU,
		to_gpr => D2GPR_in,
		to_e => D2E
	);

	HU2E <= HU2E_ZERO;

	inst_e: StageExecute port map (
		from_d => D2E,
		from_hu => HU2E,
		from_alu => ALU2E,
		from_m => M2E,
		from_w => W2E,
		to_hu => E2HU,
		to_alu => E2ALU,
		to_m => E2M,
		fresh=>'1'
	);

	inst_m: StageMemory port map (
		from_e => E2M,
		from_dm => DM2M_out,
		to_hu => M2HU,
		to_bu => M2BU,
		to_dm => M2DM_in,
		to_e => M2E,
		to_w => M2W
	);

	inst_w: StageWriteback port map (
		from_m => M2W,
		to_hu => W2HU,
		to_gpr => W2GPR_in,
		to_e => W2E,
		fresh => '1',
		clock => clock
	);
	---------------------------------------------------------------------
	-- Logic units
	inst_hu: HazardUnit 
	generic map (
        register_bypass    => '0'
    	)
	port map (
		from_bu => BU2HU,
		from_f => F2HU_ZERO,
		from_d => D2HU,
		from_e => E2HU,
		from_m => M2HU,
		from_w => W2HU,
		to_e => open,

		hazards => hazards
	);

	inst_alu: ArithmeticLogicUnit
	generic map (
        enable_M    => 0
    	)
	 port map (
		clock => clock,
		from_e => E2ALU,
		to_e => ALU2E
	);

	inst_bu: BranchUnit port map (
		from_m => M2BU,
		to_hu => BU2HU,
		to_pc => BU2PC
	);
	---------------------------------------------------------------------
	-- Memories
	D2GPR_out <= D2GPR_in when run_state = RunState_Run else D2GPR_setup;
	--W2GPR_out <= W2GPR_in when run_state = RunState_Run else W2GPR_setup;
	gprw_filter:process(clock.pulse,W2GPR_in,clk_halver)begin
		if run_state=RunState_Run then
			W2GPR_out.result<=W2GPR_in.result;
			W2GPR_out.rd<=W2GPR_in.rd;
			W2GPR_out.reg_write<=W2GPR_in.reg_write and clk_halver;
		else 
			W2GPR_out<=W2GPR_setup;
		end if;
	end process;

	inst_gpr: GeneralPurposeRegister 	
	generic map (
        register_bypass    => '0'
    	)
	port map (
		clock => clock,

		from_d => D2GPR_out,
		from_w => W2GPR_out,
		to_d => GPR2D
	);

	M2DM_out <= M2DM_in when run_state = RunState_Run else M2DM_setup;
	DM2M_out <= DM2M_in when run_state = RunState_Run else DM2M_ZERO;

	inst_cm: CombinedMemory port map (
		clock => clock,

		from_f => F2IM,
		from_m => M2DM_out,
		from_dm => from_dm, 
		from_im => from_im,
		to_f => IM2F,
		to_m => DM2M_in,
		to_dm => to_dm, 
		to_im => to_im 
	);

	--DATA MEM
	addr_dm<=to_dm.addr when unsigned(CycleCounter)=2 else (to_dm.addr and x"00000FFF");
	din_dm<=to_dm.din;
	from_dm.dout<=dout_dm;
	en_dm<=to_dm.en;
	rst_dm<=to_dm.rst;
	we_dm<=to_dm.we;
	from_dm.delayed_by_clk<=delayed_by_clk_dm;
	from_dm.mem_wait<=wait_dm;
	--INSTR MEM
	addr_im<=to_im.addr;
	din_im<=to_im.din;
	from_im.dout<=dout_im;
	en_im<=to_im.en;
	rst_im<=to_im.rst;
	we_im<=to_im.we;
	from_im.delayed_by_clk<=delayed_by_clk_im;
	from_im.mem_wait<='0';
	---------------------------------------------------------------------
	clk_halver<='1' when unsigned(CycleCounter)=3 else '0';
	F2D_out<=F2D_in;	
	F2D<=F2D_out;
	trapped<=trapped_bp;--"111111" when run_state=RunState_Run else "000000";
	process (clock)
	begin
	if rising_edge(clock.pulse) then	
		if clock.reset = '1' then
	--		trapped<=(others=>'0');
			CycleCounter<=(others=>'0');

			PC2F_out <= PC2F_ZERO;

			D2GPR_setup <= D2GPR_ZERO;
			W2GPR_setup <= W2GPR_ZERO;
			M2DM_setup <= M2DM_ZERO;

			run_state <= RunState_Begin;
			trapped_bp <= (others=>'1');--Trap_None;
		elsif clock.enable = '1' and wait_dm='0' then
			if unsigned(CycleCounter)<3 then
				if wait_dm='0' then CycleCounter<=std_logic_vector(unsigned(CycleCounter) + 1); end if;
			else
				CycleCounter<=(others=>'0');
			end if;

			if unsigned(CycleCounter)=0 then
				case run_state is
					when RunState_Begin => null;								
					when RunState_SetupStack =>
						M2DM_setup <= (
							alu_out => RESET_VECTOR_STACK,
							write_data => BUSWIDTH_ZERO,
							mem_write => '0',
							mem_bytes => (others=>'1')
						);		
					when RunState_SetupPc =>
						W2GPR_setup <= W2GPR_ZERO;	
						M2DM_setup <= (
							alu_out => RESET_VECTOR_RESET,
							write_data => BUSWIDTH_ZERO,
							mem_write => '0',
							mem_bytes => (others=>'1')
							);
					when RunState_Run =>
					--	if hazards.trap /= Trap_None then
					--		run_state <= RunState_Trapped;
					--		trapped <= trap_enum2vec(hazards.trap);
					--	els
						if wait_dm='0' then
							PC2F_out <= PC2F_in;
						end if;
					when RunState_Trapped =>
						null;
				end case;
			elsif unsigned(CycleCounter)=3 then
				case run_state is
					when RunState_Begin =>
						run_state <= RunState_SetupStack;
					when RunState_SetupStack =>
						run_state <= RunState_SetupPc;
						W2GPR_setup <= (
							result =>DM2M_in.read_data,
							rd => REGSEL_STACK,
							reg_write => '1'
							);
						if DM2M_in.read_data=X"00001000" then
							trapped_bp<="000001";
						end if;
					when RunState_SetupPc =>
						run_state<=RunState_Run;
						PC2F_out.pc<=std_logic_vector(unsigned(DM2M_in.read_data)-4); --<= (pc => DM2M_in.read_data);
						if DM2M_in.read_data=X"00000200" then
							trapped_bp<="000010";
						end if;
					when RunState_Run =>
						if hazards.trap /= Trap_None then
							run_state <= RunState_Trapped;
					--		trapped <= trap_enum2vec(hazards.trap);
						end if;
					when RunState_Trapped =>
						null;
				end case;	
			end if;

			

		
		end if;	
	end if;
	end process;
end architecture;
