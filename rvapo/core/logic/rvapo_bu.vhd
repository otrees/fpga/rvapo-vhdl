library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity BranchUnit is
	port (
		from_m: in Intcon_M2BU;
		to_hu: out Intcon_BU2HU;
		to_pc: out Intcon_BU2PC
	);
end;

architecture rtl of BranchUnit is
	signal pc_override: std_logic;
	signal pc_value: BusWidth_t;
begin
	to_hu <= (
		branch_outcome => pc_override
	);

	to_pc <= (
		pc_override,
		pc_value
	);

	process (from_m)
	begin
		pc_override <= '0';
		pc_value <= from_m.pc_plus_imm;

		case from_m.branch is
			when BCtrl_None => null;
			when BCtrl_JALR =>
				pc_override <= '1';
				pc_value <= from_m.alu_out;
			when BCtrl_JAL =>
				pc_override <= '1';
			when BCtrl_BEQ =>
				pc_override <= from_m.alu_flag.zero;
			when BCtrl_BNE =>
				pc_override <= not from_m.alu_flag.zero;
			when BCtrl_BLT =>
				pc_override <= not from_m.alu_flag.zero;
			when BCtrl_BGE =>
				pc_override <= from_m.alu_flag.zero;
			when BCtrl_BLTU =>
				pc_override <= not from_m.alu_flag.zero;
			when BCtrl_BGEU =>
				pc_override <= from_m.alu_flag.zero;
		end case;
	end process;
end;