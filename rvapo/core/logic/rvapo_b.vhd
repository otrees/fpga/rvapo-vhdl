library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;

entity Bitmanip is
	port (
        from_alu:in Intcon_ALU2B;
        to_alu:out Intcon_B2ALU
	);
end;

architecture rtl of Bitmanip is
signal clz:std_logic_vector(5 downto 0);
begin
	to_alu <= (
		result=>clz
	);
    clz<=clz32(from_alu.srcA);
end;