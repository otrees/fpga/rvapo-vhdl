# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "axi" -parent ${Page_0}
  ipgui::add_param $IPINST -name "debug" -parent ${Page_0}
  ipgui::add_param $IPINST -name "simple" -parent ${Page_0}
  ipgui::add_param $IPINST -name "unalign" -parent ${Page_0}


}

proc update_PARAM_VALUE.axi { PARAM_VALUE.axi } {
	# Procedure called to update axi when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.axi { PARAM_VALUE.axi } {
	# Procedure called to validate axi
	return true
}

proc update_PARAM_VALUE.debug { PARAM_VALUE.debug } {
	# Procedure called to update debug when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.debug { PARAM_VALUE.debug } {
	# Procedure called to validate debug
	return true
}

proc update_PARAM_VALUE.simple { PARAM_VALUE.simple } {
	# Procedure called to update simple when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.simple { PARAM_VALUE.simple } {
	# Procedure called to validate simple
	return true
}

proc update_PARAM_VALUE.unalign { PARAM_VALUE.unalign } {
	# Procedure called to update unalign when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.unalign { PARAM_VALUE.unalign } {
	# Procedure called to validate unalign
	return true
}


proc update_MODELPARAM_VALUE.axi { MODELPARAM_VALUE.axi PARAM_VALUE.axi } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.axi}] ${MODELPARAM_VALUE.axi}
}

proc update_MODELPARAM_VALUE.simple { MODELPARAM_VALUE.simple PARAM_VALUE.simple } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.simple}] ${MODELPARAM_VALUE.simple}
}

proc update_MODELPARAM_VALUE.debug { MODELPARAM_VALUE.debug PARAM_VALUE.debug } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.debug}] ${MODELPARAM_VALUE.debug}
}

proc update_MODELPARAM_VALUE.unalign { MODELPARAM_VALUE.unalign PARAM_VALUE.unalign } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.unalign}] ${MODELPARAM_VALUE.unalign}
}

