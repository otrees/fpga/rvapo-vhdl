library IEEE;
use IEEE.std_logic_1164.all;
--use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use WORK.rvapo_pkg.all;
--use WORK.rvapo_wrappers.all;
entity WrapperSPI is
generic (
	axi      	:integer;
	simple		:integer;
	debug      	:integer; 
	unalign		:integer
	);
port (
    clk: in std_logic;
    rst: in std_logic;
    trapped: out std_logic_vector(5 downto 0);

	addr_hm: out  std_logic_vector(31 downto 0);
	din_hm: out  std_logic_vector(31 downto 0);
	en_hm: out std_logic;rst_hm: out std_logic;
	we_hm: out std_logic_vector(3 downto 0);
	dsel: in std_logic_vector(7 downto 0);

	addr_dm: out std_logic_vector(31 downto 0);
	din_dm: out  std_logic_vector(31 downto 0);
	dout_dm: in  std_logic_vector(31 downto 0);
	en_dm: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
	rst_dm: out std_logic;--! resets output. Fixed '0' should suffice.	
	we_dm: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
	delayed_dm: in std_logic;
	
	addr_im: out  std_logic_vector(31 downto 0);
	din_im: out  std_logic_vector(31 downto 0);
	dout_im: in  std_logic_vector(31 downto 0);
	en_im: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
	rst_im: out std_logic;--! resets output. Fixed '0' should suffice.	
	we_im: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
	delayed_im: in std_logic;

	spi0_mosi:out std_logic;--master port to psram 		
	spi0_miso:in std_logic;
	spi0_sclk:out std_logic;
	spi0_cs0:out std_logic;

	SPI_CSL:in std_logic;--slave port to riscv
	SPI_MOSI:in std_logic;
	SPI_MISO:out std_logic;
	SPI_SCLK:in std_logic
);
end;

architecture rtl of WrapperSPI is
	component spi_master is
	generic (
		data_width	: integer	:= 64;
		spi_clkdiv	: integer	:= 1
	);
	port (
		reset_in   	: in std_logic;

		clk_in   	: in std_logic;
		clk_en   	: in std_logic;

		spi_clk   	: out std_logic;
		spi_cs   	: out std_logic;
		spi_mosi   	: out std_logic;
		spi_miso  	: in std_logic;

		tx_data		: in std_logic_vector(data_width-1 downto 0);
		rx_data		: out std_logic_vector(data_width-1 downto 0);

		trasfer_rq	: in std_logic;
		transfer_ready	: out std_logic
	);
	end component;

    signal c_addr_dm:std_logic_vector(31 downto 0):=x"00000000";
	signal c_din_dm:std_logic_vector(31 downto 0);
	signal c_dout_dm:std_logic_vector(31 downto 0);
	signal c_en_dm:std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
	signal c_rst_dm:std_logic;--! resets output. Fixed '0' should suffice.	
	signal c_we_dm:std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
	signal c_delayed_dm:std_logic;
	
	signal c_addr_im:std_logic_vector(31 downto 0);
	signal c_din_im:std_logic_vector(31 downto 0);
	signal c_dout_im:std_logic_vector(31 downto 0);
	signal c_en_im:std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
	signal c_rst_im:std_logic;--! resets output. Fixed '0' should suffice.	
	signal c_we_im:std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
	signal c_delayed_im:std_logic;

	signal axi_stall: std_logic := '0';--this must be initialised, because it will never be assigned again if no axi
	signal axir_data: BusWidth_t;
	signal axir_result:std_logic_vector(1 downto 0);
	signal axir_start: std_logic := '0';
	signal axir_complete: std_logic := '0';
	signal axiw_given: std_logic := '0';
	signal axir_given: std_logic := '0';
	signal axiw_result:std_logic_vector(1 downto 0);
	signal axiw_start: std_logic := '0';
	signal axiw_complete: std_logic := '0';
	signal axi_any: std_logic:='0';

    signal clock: clock_t;

	type spi_state is (free,fpga,riscv,ctrl);
	signal spi_owner:spi_state:=free;	

--	signal CLKHF:std_logic;
--	signal rx_data_trash:std_logic_vector(63 downto 0);
--	signal spi_reset:std_logic:='1';
	signal SPI_CSL_old:std_logic:='1';
	signal psram_req:std_logic;
	signal psram_req_old:std_logic:='0';
signal A_SIGNAL_THAT_MUST_EXIST:std_logic:='0';
signal ANOTHER_SIGNAL_THAT_MUST_EXIST:std_logic:='0';	
--signal psram_req_old:std_logic;
	signal stall_all:std_logic:='0';
	signal spi_working:std_logic;
	signal spi_req:std_logic:='0';

	signal spi_clk_boilerplate:std_logic;
	signal spi_cs_boilerplate:std_logic;
	signal spi_mosi_boilerplate:std_logic;
	signal spi_miso_boilerplate:std_logic;

	signal psram_read:std_logic_vector(63 downto 0);
	signal psram_write:std_logic_vector(63 downto 0);

	signal cpu_reset:std_logic:='0';

	signal spi_trash_counter:std_logic_vector(7 downto 0):=x"00";
	CONSTANT cmp:std_logic_vector(7 downto 0):=x"06";
	CONSTANT cmp2:std_logic_vector(31 downto 0):=x"FFFFFFFF";
	CONSTANT cmp3:std_logic_vector(3 downto 0):="0000";
--	signal generic_boilerplate:std_logic:='0';
begin
	clock.reset <= rst;
	clock.enable <= '1';
	clock.pulse <= clk;

	core: InterconnectIce40 
	generic map(debug       =>debug,unalign		=>unalign)
	port map (
		clk=>clk, rst=>rst, 	trapped=>trapped,addr_hm=>addr_hm,			din_hm=>din_hm,en_hm=>en_hm,rst_hm=>rst_hm,we_hm=>we_hm,
		dsel=>dsel,addr_dm=>	c_addr_dm,din_dm=>	c_din_dm,			dout_dm=>	c_dout_dm,en_dm=>		en_dm,
		rst_dm=>	rst_dm,we_dm=>		c_we_dm,			delayed_by_clk_dm=>c_delayed_dm,			wait_dm=>	stall_all,
		addr_im=>	addr_im,din_im=>	din_im,dout_im=>	dout_im,en_im=>		en_im,			rst_im=>	rst_im,we_im=>		we_im,delayed_by_clk_im=>delayed_im
		);
	addr_dm<=c_addr_dm;
	din_dm<=c_din_dm;
	we_dm<=c_we_dm;

	spi0_mosi<=				SPI_MOSI when spi_owner=riscv else spi_mosi_boilerplate;--master port to psram 		
	spi_miso_boilerplate<=	spi0_miso when spi_owner=fpga else '0';
	SPI_MISO<=				spi0_miso when spi_owner=riscv else '0';
	spi0_sclk<=				SPI_SCLK when spi_owner=riscv else spi_clk_boilerplate;
	spi0_cs0<=				SPI_CSL when spi_owner=riscv else spi_cs_boilerplate;

	psram_req<='1' when unsigned(c_addr_dm)>=4096 else '0';--not spi_reset;
	cpu_reset<='1' when (spi_owner=ctrl) else '0'; --and c_dout_dm=cmp2) else '0'; 

	spi_switch:process(clk,spi_req,spi_working,psram_req,spi_owner) 
	begin
		if psram_req='1' then
			if spi_owner=riscv or spi_owner=ctrl or spi_req='1' or spi_working='1' or psram_req_old='0' then--if owner was riscv then fpga must hang on the same request
				stall_all<='1';
			else
				stall_all<='0';
			end if;
		else 
			stall_all<='0';
		end if;

		if rising_edge(clk) then
			psram_req_old<=psram_req;

			if spi_req='1' then
				spi_trash_counter <= std_logic_vector(unsigned(spi_trash_counter) + 1);
				if unsigned(spi_trash_counter) >= unsigned(cmp) then
					spi_req<='0';			
				end if;
			end if;

			if spi_owner=free and SPI_CSL='0' and SPI_CSL_old='1' then
				spi_owner<=riscv;			
			elsif spi_owner=riscv and SPI_CSL='1' and SPI_CSL_old='0' then
				spi_owner<=ctrl;--free;
				spi_req<='1';
				spi_trash_counter<=(others=>'0');		
			end if;

			if psram_req='1' and spi_owner=free then
				spi_owner<=fpga;	
				spi_req<='1';		
				spi_trash_counter<=(others=>'0');	
			end if;
			
			if (spi_owner=fpga or spi_owner=ctrl) and spi_working='0' and spi_req='0' then
				spi_owner<=free;
			end if;

			SPI_CSL_old<=SPI_CSL;
		end if;
	end process;

	writeset:process(clk,c_addr_dm,c_we_dm,c_din_dm)begin
		if spi_owner=ctrl then
			psram_write<=x"03"&x"000000"&x"00000000";
		elsif c_we_dm=cmp3 then--we=0000, reading
			psram_write<=x"03"&c_addr_dm(23 downto 0)&x"00000000";
		else
			psram_write<=x"02"&c_addr_dm(23 downto 0)&c_din_dm;
		end if;
	end process;
	c_dout_dm<=dout_dm when spi_owner=free else reverse_vector(psram_read(63 downto 32));
	c_delayed_dm<=delayed_dm and (not stall_all); --delayed_dm when stall_all='0' else '0';--actually irrelevant since ice40 is singlecycle
												
	psram: spi_master port map(
		reset_in=>'0',--TODO use cpu_reset
		clk_in=>clk,
		clk_en=>'1',
		spi_clk =>  spi_clk_boilerplate,
		spi_cs   =>	spi_cs_boilerplate,
		spi_mosi   =>	spi_mosi_boilerplate,
		spi_miso  	=>spi_miso_boilerplate,
		tx_data=>reverse_vector(psram_write),--reverse_vector(x"0200000057575757"),
		rx_data=>psram_read,
		trasfer_rq=>spi_req,
		transfer_ready=>spi_working
		);
end;


