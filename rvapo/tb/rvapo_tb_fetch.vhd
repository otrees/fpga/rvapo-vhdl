library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity rvapo_tb_fetch is
end entity;

architecture tb of rvapo_tb_fetch is
	type test_vector is record
		from_pc: Intcon_PC2F;
		from_im: Intcon_IM2F;
		to_im: Intcon_F2IM;
		to_pc: Intcon_F2PC;
		to_d: Intcon_F2D;
	end record;

	type test_vector_array is array (natural range <>) of test_vector;
	constant test_vectors: test_vector_array := (
		(
			(pc => x"11111111"),
			(instruction => x"22222222"),
			(pc => x"11111111"),
			(pc_plus_4 => x"11111115"),
			(x"11111111", x"11111115", x"22222222")
		),
		(
			(pc => x"00000000"),
			(instruction => x"12345678"),
			(pc => x"00000000"),
			(pc_plus_4 => x"00000004"),
			(x"00000000", x"00000004", x"12345678")
		)
	);

	signal actual_test: test_vector;
	signal expected_test: test_vector;
begin
	eut: StageFetch port map (
		actual_test.from_pc,
		actual_test.from_im,
		actual_test.to_im,
		actual_test.to_pc,
		actual_test.to_d
	);

	process
		variable expected_test_var: test_vector;
	begin
		for i in test_vectors'range loop
			expected_test_var := test_vectors(i);
			expected_test <= expected_test_var;

			actual_test.from_pc <= expected_test_var.from_pc;
			actual_test.from_im <= expected_test_var.from_im;
			wait for 1 ns;

			assert actual_test.to_im = expected_test_var.to_im
			report "invalid to_im" severity failure;

			assert actual_test.to_pc = expected_test_var.to_pc
			report "invalid to_pc" severity failure;

			assert actual_test.to_d = expected_test_var.to_d
			report "invalid to_d" severity failure;
		end loop;

		assert false report "Fetch tests passed" severity note;
		wait;
	end process;
end architecture;