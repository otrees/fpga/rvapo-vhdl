library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity rvapo_tb_dec is
end entity;

architecture tb of rvapo_tb_dec is
	type test_vector is record
		from_f: Intcon_F2D;
		from_gpr: Intcon_GPR2D;
		to_hu: Intcon_D2HU;
		to_gpr: Intcon_D2GPR;
		to_e: Intcon_D2E;
	end record;

	type test_vector_array is array (natural range <>) of test_vector;
	constant test_vectors: test_vector_array := (
		-- SUB
		(
			from_f => (
				x"11111111",
				x"11111115",
				make_instruction_R(
					opcode => "0110011",
					rd => "00001",
					funct3 => "000",
					rs1 => "00010",
					rs2 => "00011",
					funct7 => "0100000"
				)
			),
			from_gpr => (x"22222222", x"33333333"),
			to_hu => ("00010", "00011"),
			to_gpr => ("00010", "00011"),
			to_e => (
				x"11111111", x"11111115",
				rs1 => "00010",
				rs2 => "00011",
				rd => "00001",
				val1 => x"22222222",
				val2 => x"33333333",
				valI => x"00000000",
				control => (
					BranchControl_None,
					reg_write => '1',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				),
				alu_control => AluControl_SUB,
				alu_source => AluSource_Reg
			)
		),
		-- ADDI
		(
			from_f => (
				x"11111111",
				x"11111115",
				make_instruction_I(
					opcode => "0010011",
					rd => "00011",
					funct3 => "000",
					rs1 => "00010",
					imm => x"801"
				)
			),
			from_gpr => (x"22222222", x"33333333"),
			to_hu => ("00010", REGSEL_ZERO),
			to_gpr => ("00010", REGSEL_ZERO),
			to_e => (
				x"11111111", x"11111115",
				rs1 => "00010",
				rs2 => REGSEL_ZERO,
				rd => "00011",
				val1 => x"22222222",
				val2 => x"33333333",
				valI => x"FFFFF801",
				control => (
					BranchControl_None,
					reg_write => '1',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				),
				alu_control => AluControl_ADD,
				alu_source => AluSource_Imm
			)
		),
		-- SW
		(
			from_f => (
				x"00000000",
				x"00000004",
				make_instruction_S(
					opcode => "0100011",
					funct3 => "010",
					rs1 => "01111",
					rs2 => "10101",
					imm => x"7FF"
				)
			),
			from_gpr => (x"22222222", x"33333333"),
			to_hu => ("01111", "10101"),
			to_gpr => ("01111", "10101"),
			to_e => (
				x"00000000", x"00000004",
				rs1 => "01111",
				rs2 => "10101",
				rd => REGSEL_ZERO,
				val1 => x"22222222",
				val2 => x"33333333",
				valI => x"000007FF",

				control => (
					BranchControl_None,
					reg_write => '0',
					mem_to_reg => '0',
					mem_write => '1',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				),
				alu_control => AluControl_ADD,
				alu_source => AluSource_Imm
			)
		),
		-- BEQ
		(
			from_f => (
				x"00000000",
				x"00000004",
				make_instruction_B(
					opcode => "1100011",
					funct3 => "000",
					rs1 => "01111",
					rs2 => "10101",
					imm => x"FFF"
				)
			),
			from_gpr => (x"00000001", x"0000FFFF"),
			to_hu => ("01111", "10101"),
			to_gpr => ("01111", "10101"),
			to_e => (
				x"00000000", x"00000004",
				rs1 => "01111",
				rs2 => "10101",
				rd => REGSEL_ZERO,
				val1 => x"00000001",
				val2 => x"0000FFFF",
				valI => x"FFFFFFFE",

				control => (
					BranchControl_BEQ,
					reg_write => '0',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				),
				alu_control => AluControl_SUB,
				alu_source => AluSource_Reg
			)
		),
		-- JAL
		(
			from_f => (
				x"00000000",
				x"00000004",
				make_instruction_J(
					opcode => "1101111",
					rd => "01010",
					imm => "01111111111111111111"
				)
			),
			from_gpr => (x"00000001", x"0000FFFF"),
			to_hu => (REGSEL_ZERO, REGSEL_ZERO),
			to_gpr => (REGSEL_ZERO, REGSEL_ZERO),
			to_e => (
				x"00000000", x"00000004",
				rs1 => REGSEL_ZERO,
				rs2 => REGSEL_ZERO,
				rd => "01010",
				val1 => x"00000001",
				val2 => x"0000FFFF",
				valI => x"000FFFFE",
				control => (
					BranchControl_JAL,
					reg_write => '1',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				),
				alu_control => AluControl_ADD,
				alu_source => AluSource_Imm
			)
		),
		-- LBU
		(
			from_f => (
				x"11111111",
				x"11111115",
				make_instruction_I(
					opcode => "0000011",
					rd => "00011",
					funct3 => "100",
					rs1 => "00010",
					imm => x"701"
				)
			),
			from_gpr => (x"22222222", x"33333333"),
			to_hu => ("00010", REGSEL_ZERO),
			to_gpr => ("00010", REGSEL_ZERO),
			to_e => (
				x"11111111", x"11111115",
				rs1 => "00010",
				rs2 => REGSEL_ZERO,
				rd => "00011",
				val1 => x"22222222",
				val2 => x"33333333",
				valI => x"00000701",
				control => (
					BranchControl_None,
					reg_write => '1',
					mem_to_reg => '1',
					mem_write => '0',
					mem_control => MemoryControl_ByteUnsigned,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				),
				alu_control => AluControl_ADD,
				alu_source => AluSource_Imm
			)
		),
		-- JALR
		(
			from_f => (
				x"11111111",
				x"11111115",
				make_instruction_I(
					opcode => "1100111",
					rd => "00011",
					funct3 => "000",
					rs1 => "00010",
					imm => x"701"
				)
			),
			from_gpr => (x"22222222", x"33333333"),
			to_hu => ("00010", REGSEL_ZERO),
			to_gpr => ("00010", REGSEL_ZERO),
			to_e => (
				x"11111111", x"11111115",
				rs1 => "00010",
				rs2 => REGSEL_ZERO,
				rd => "00011",
				val1 => x"22222222",
				val2 => x"33333333",
				valI => x"00000701",
				control => (
					BranchControl_JALR,
					reg_write => '1',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				),
				alu_control => AluControl_ADD,
				alu_source => AluSource_Imm
			)
		),
		-- AUIPC
		(
			from_f => (
				x"11111111",
				x"11111115",
				make_instruction_U(
					opcode => "0010111",
					rd => "00001",
					imm => "01111111111111111111"
				)
			),
			from_gpr => (x"22222222", x"33333333"),
			to_hu => (REGSEL_ZERO, REGSEL_ZERO),
			to_gpr => (REGSEL_ZERO, REGSEL_ZERO),
			to_e => (
				x"11111111", x"11111115",
				rs1 => REGSEL_ZERO,
				rs2 => REGSEL_ZERO,
				rd => "00001",
				val1 => x"22222222",
				val2 => x"33333333",
				valI => x"7FFFF000",
				control => (
					BranchControl_None,
					reg_write => '1',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '1',
					trap => Trap_None
				),
				alu_control => AluControl_ADD,
				alu_source => AluSource_Imm
			)
		)
	);

	signal actual_test: test_vector;
	signal expected_test: test_vector;
begin
	eut: StageDecode port map (
		actual_test.from_f,
		actual_test.from_gpr,
		actual_test.to_hu,
		actual_test.to_gpr,
		actual_test.to_e
	);

	process
		variable expected_test_var: test_vector;
	begin
		for i in test_vectors'range loop
			expected_test_var := test_vectors(i);
			expected_test <= expected_test_var;

			actual_test.from_f <= expected_test_var.from_f;
			actual_test.from_gpr <= expected_test_var.from_gpr;
			wait for 1 ns;

			assert actual_test.to_hu = expected_test_var.to_hu
			report "invalid to_hu" severity failure;

			assert actual_test.to_gpr = expected_test_var.to_gpr
			report "invalid to_gpr" severity failure;

			assert actual_test.to_e = expected_test_var.to_e
			report "invalid to_e" severity failure;
		end loop;

		assert false report "Decode tests passed" severity note;
		wait;
	end process;
end architecture;