library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;
use WORK.rvapo_cm_target_init.all;--thus is fileloader imported
use work.lx_fncapprox_pkg.all;

entity rvapo_tb_mmap is
generic (
        AXI_BASE 	:std_logic_vector:=x"00001000";
    	axi      	:integer:=1;
		simple		:integer:=0;
		debug      	:integer:=0; 
		unalign		:integer:=1;
	    mem_delayed :std_logic:='1';
		enable_M	:integer:=1
        );
end entity;

architecture tb of rvapo_tb_mmap is
	signal clock: clock_t := (
		pulse => '1',
		enable => '0',
		reset => '1'
	);

	component InterruptController is
	generic (
		debug_exec_delay:integer
		);
	port (
		clk				:in std_logic;
		rst				:in std_logic;

		instr			:out std_logic_vector(31 downto 0);
		pause			:out std_logic;
		issue			:out std_logic;

		irq				:in std_logic_vector(3 downto 0);
		irq_address		:in std_logic_vector(31 downto 0);
		trap			:in std_logic_vector(5 downto 0)
	);
	end component;

    component MemoryMap is
        generic (
    --	AXI_BASE 		:std_logic_vector;
        debug      		:integer;
    --	Allow Vivado to adjust widths to match the rest of AXI interconnect
        C_M_AXI_ID_WIDTH	: integer	:= 1;
        C_M_AXI_AWUSER_WIDTH	: integer	:= 5;
        C_M_AXI_ARUSER_WIDTH	: integer	:= 5;
        C_M_AXI_WUSER_WIDTH	: integer	:= 4;
        C_M_AXI_RUSER_WIDTH	: integer	:= 4;
        C_M_AXI_BUSER_WIDTH	: integer	:= 5
        );
    port (
	aclk: in std_logic;
	rst: in std_logic;

	addr: in  std_logic_vector(31 downto 0);
	din: in  std_logic_vector(31 downto 0);
	dout: out  std_logic_vector(31 downto 0);
	we: in std_logic_vector(3 downto 0);
	req: in std_logic;
	delayed: out std_logic;
	stall: out std_logic;

	addr_dm: out std_logic_vector(31 downto 0);
	din_dm: out  std_logic_vector(31 downto 0);
	dout_dm: in  std_logic_vector(31 downto 0);
	en_dm: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
	we_dm: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
	delayed_dm: in std_logic;

	addr_fnc	: out std_logic_vector(4 downto 0);
	en_fnc      : out std_logic;
	din_fnc     : out std_logic_vector(31 downto 0);
	dout_fnc    : in std_logic_vector(31 downto 0);
	we_fnc		: out std_logic_vector(3 downto 0);
	
	-- Global Signals
	-- No reset
	-- Write address channel signals
	M_AXI_AWID          :   out std_logic_vector(5 DOWNTO 0);
	M_AXI_AWADDR        :   out std_logic_vector(31 downto 0);
	M_AXI_AWLEN         :   out std_logic_vector(7 downto 0);
	M_AXI_AWSIZE        :   out std_logic_vector(2 downto 0);
	M_AXI_AWBURST       :   out std_logic_vector(1 downto 0);
	M_AXI_AWLOCK        :   out std_logic_vector(1 downto 0);
	M_AXI_AWCACHE       :   out std_logic_vector(3 downto 0);
	M_AXI_AWPROT        :   out std_logic_vector(2 downto 0);
	M_AXI_AWQOS         :   out std_logic_vector(3 downto 0);
	M_AXI_AWUSER        :   out std_logic_vector(4 downto 0);
	M_AXI_AWVALID       :   out std_logic;
	M_AXI_AWREADY       :   in  std_logic;
	-- Write data channel signals
	M_AXI_WDATA         :   out std_logic_vector(31 downto 0);--(2**axi_data_width_log2b - 1 downto 0);
	M_AXI_WSTRB         :   out std_logic_vector(3 downto 0);--(2**(axi_data_width_log2b - 3) - 1 downto 0);
	M_AXI_WLAST         :   out std_logic;
	M_AXI_WUSER         :   out std_logic_vector(3 downto 0);
	M_AXI_WVALID        :   out std_logic;
	M_AXI_WREADY        :   in  std_logic;
	--  Write response channel signals
	M_AXI_BID           :   in  std_logic_vector(5 downto 0);
	M_AXI_BRESP         :   in  std_logic_vector(1 downto 0);
	M_AXI_BUSER        	:   in std_logic_vector(4 downto 0);
	M_AXI_BVALID        :   in  std_logic;
	M_AXI_BREADY        :   out std_logic;
	--  Read address channel signals
	M_AXI_ARID          :   out std_logic_vector(5 downto 0);
	M_AXI_ARADDR        :   out std_logic_vector(31 downto 0);
	M_AXI_ARLEN         :   out std_logic_vector(7 downto 0);
	M_AXI_ARSIZE        :   out std_logic_vector(2 downto 0);
	M_AXI_ARBURST       :   out std_logic_vector(1 downto 0);
	M_AXI_ARLOCK        :   out std_logic_vector(1 downto 0);
	M_AXI_ARCACHE       :   out std_logic_vector(3 downto 0);
	M_AXI_ARPROT        :   out std_logic_vector(2 downto 0);
	M_AXI_ARQOS         :   out std_logic_vector(3 downto 0);
	M_AXI_ARUSER        :   out std_logic_vector(4 downto 0);
	M_AXI_ARVALID       :   out std_logic;
	M_AXI_ARREADY       :   in  std_logic;
	-- Read data channel signals
	M_AXI_RID           :   in  std_logic_vector(5 downto 0);
	M_AXI_RDATA         :   in  std_logic_vector(31 downto 0);--(2**axi_data_width_log2b - 1 downto 0);
	M_AXI_RRESP         :   in  std_logic_vector(1 downto 0);
	M_AXI_RLAST         :   in  std_logic;
	M_AXI_RUSER         :   in std_logic_vector(3 downto 0);
	M_AXI_RVALID        :   in  std_logic;
	M_AXI_RREADY        :   out std_logic	
    );
    end component;

	signal trapped: std_logic_vector(5 downto 0);
	signal done: std_logic := '0';

	signal M_AXI_AWID          :    std_logic_vector(5 DOWNTO 0);
    signal M_AXI_AWADDR        :    std_logic_vector(31 downto 0);
    signal M_AXI_AWLEN         :    std_logic_vector(7 downto 0);
    signal M_AXI_AWSIZE        :    std_logic_vector(2 downto 0);
    signal M_AXI_AWBURST       :    std_logic_vector(1 downto 0);
    signal M_AXI_AWLOCK        :    std_logic_vector(1 downto 0);
    signal M_AXI_AWCACHE       :    std_logic_vector(3 downto 0);
    signal M_AXI_AWPROT        :    std_logic_vector(2 downto 0);
    signal M_AXI_AWQOS         :    std_logic_vector(3 downto 0);
    signal M_AXI_AWUSER        :    std_logic_vector(4 downto 0);
    signal M_AXI_AWVALID       :    std_logic;
	signal M_AXI_AWREADY	   :    std_logic;		
	
    signal M_AXI_WDATA         :    std_logic_vector(31 downto 0);
    signal M_AXI_WSTRB         :    std_logic_vector(3 downto 0);
    signal M_AXI_WLAST         :    std_logic;
    signal M_AXI_WVALID        :    std_logic;
	signal M_AXI_WREADY		   :    std_logic;
		
	signal M_AXI_BRESP		   :	std_logic_vector(1 downto 0);
	signal M_AXI_BVALID		   :    std_logic;
	signal M_AXI_BREADY        :    std_logic;
	signal M_AXI_BUSER         :    std_logic_vector(4 downto 0);

	signal M_AXI_ARID          :    std_logic_vector(5 downto 0);
    signal M_AXI_ARADDR        :    std_logic_vector(31 downto 0);
    signal M_AXI_ARLEN         :    std_logic_vector(7 downto 0);
    signal M_AXI_ARSIZE        :    std_logic_vector(2 downto 0);
    signal M_AXI_ARBURST       :    std_logic_vector(1 downto 0);
    signal M_AXI_ARLOCK        :    std_logic_vector(1 downto 0);
    signal M_AXI_ARCACHE       :    std_logic_vector(3 downto 0);
    signal M_AXI_ARPROT        :    std_logic_vector(2 downto 0);
    signal M_AXI_ARQOS         :    std_logic_vector(3 downto 0);
    signal M_AXI_ARUSER        :    std_logic_vector(4 downto 0);
    signal M_AXI_ARVALID       :    std_logic;
	signal M_AXI_ARREADY	   :	std_logic;
	
	signal M_AXI_RDATA         :    std_logic_vector(31 downto 0);--(2**axi_data_width_log2b - 1 downto 0);
	signal M_AXI_RRESP         :    std_logic_vector(1 downto 0);
	signal M_AXI_RVALID        :    std_logic;
	signal M_AXI_RREADY		   :	std_logic;		
    signal M_AXI_RUSER         :    std_logic_vector(3 downto 0);

    signal addr_dm:std_logic_vector(31 downto 0):=x"00000000";
	signal din_dm:std_logic_vector(31 downto 0);
	signal dout_dm:std_logic_vector(31 downto 0);
--	signal en_dm:std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
--	signal c_rst_dm:std_logic;--! resets output. Fixed '0' should suffice.	
	signal we_dm:std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
	signal delayed_dm:std_logic;
	signal c_req_dm:std_logic;
    signal req_dm:std_logic;

    signal c2_addr_dm:std_logic_vector(31 downto 0):=x"00000000";
	signal c2_din_dm:std_logic_vector(31 downto 0);
	signal c2_dout_dm:std_logic_vector(31 downto 0);
	signal c2_we_dm:std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
    signal dm_wait:std_logic;
    signal we_dm_filtered:std_logic_vector(3 downto 0);

	signal addr_fnc:std_logic_vector(4 downto 0);
	signal din_fnc:std_logic_vector(31 downto 0);
	signal dout_fnc:std_logic_vector(31 downto 0);
	signal en_fnc:std_logic;
	signal we_fnc:std_logic_vector(3 downto 0);

    signal addr_im:std_logic_vector(31 downto 0);
	signal dout_im:std_logic_vector(31 downto 0);

    signal im_delay_counter:std_logic_vector(7 downto 0):=(others=>'0');
	signal dout_im_filtered:std_logic_vector(31 downto 0);
	signal im_wait:std_logic;
	signal im_wait_delay:std_logic;
	signal trigger_delay:std_logic_vector(7 downto 0):=(others=>'0');
	signal irq_hold:std_logic_vector(7 downto 0):=(others=>'0');
	signal counter:std_logic_vector(31 downto 0):=(others=>'0');

	signal irq_req:std_logic_vector(3 downto 0):=(others=>'0');
	signal irq_address:std_logic_vector(31 downto 0):=x"00000240";

    signal pause:std_logic;
	signal issue:std_logic;
	signal step:std_logic;
	signal debug_instr:std_logic_vector(31 downto 0);

	signal pause_man:std_logic:='0';
	signal issue_man:std_logic:='0';
	signal step_man:std_logic:='0';
	signal debug_instr_man:std_logic_vector(31 downto 0):=x"0100006f";--x"00200033";--(others=>'0');

	signal pause_irq:std_logic:='0';
	signal issue_irq:std_logic:='0';
	signal debug_instr_irq:std_logic_vector(31 downto 0);

	signal irq_requested:std_logic:='0';
begin
	pause<=pause_man or pause_irq;
	issue<=issue_man or issue_irq;
	step<=step_man;
	debug_instr<=debug_instr_irq when pause_irq='1' else debug_instr_man;

--	clock.reset <= '0' after 25 ns;
	clock.enable <= '1' after 10 ns;
	clock.pulse <= not clock.pulse after 10 ns when done /= '1' else '0';
	step<='0';--'1' when counter(2 downto 1)="00" else '0';--not pause after 20 ns when done/='1' else '0';
	--issue_man<='1' when counter(2 downto 1)="10" else '0';

    im_wait<='1' when unsigned(im_delay_counter)/=0 else '0';
    --This is an ugly hack. Unaligned memory handling needs to know *at the beginning* whether memory requires register bypass 
    --to decide whether to do two or three step read. This uses delayed_dm which is given for *previous* mem request.
    req_dm<=c_req_dm;-- when addr_dm/=x"00000000" else '0';
    we_dm_filtered<="0000" when addr_dm=x"00000000" else we_dm;
    dout_im_filtered<=dout_im when (im_wait_delay and im_wait)='0' else x"DEC0DE00";
	irq_req<="0000" when irq_hold=x"00" else "0010";

    settings_manager:process(clock)begin
		if rising_edge(clock.pulse)then
			counter<=std_logic_vector(unsigned(counter)+1);

			if addr_dm=x"00000000" and req_dm='1' and we_dm="1111" then
				case(din_dm)is
					when x"00000001"=>im_delay_counter<=x"03";
					when x"00000003"=>trigger_delay<=x"03";
					when x"00000004"=>
						irq_requested<='1';
						irq_hold<=x"04";
					when others=>null;
				end case;
			end if;

			if(trigger_delay=x"01")then 
				im_delay_counter<=x"03";
			end if;

			if unsigned(trigger_delay)>0 then
				trigger_delay<=std_logic_vector(unsigned(trigger_delay)-1);				
			end if;

			if unsigned(im_delay_counter)>0 then
				im_delay_counter<=std_logic_vector(unsigned(im_delay_counter)-1);
			end if;

			if unsigned(irq_hold)>0 then
				irq_hold<=std_logic_vector(unsigned(irq_hold)-1);				
			end if;
		end if;
	end process;

	im_delayer:process(clock)begin
		if(rising_edge(clock.pulse))then
			im_wait_delay<=im_wait;
		end if;
	end process;

	process
	begin
        clock.reset<='1';
		-- reset
		wait for 25 ns;
		clock.reset<='0';
		-- running code

	--	wait for 500 ns;
	--	irq_req<="0010";
	--	wait for 40 ns;
	--	irq_req<="0000";

		wait until trapped /= "000000" and (trapped/="000001" or irq_requested='0');
	--	wait for 10 ns;
        clock.reset<='1';
        wait for 40 ns;
		-- turn off the clock
		done <= '1';
		assert false report "Done, check wave output for result" severity note;
		wait;
	end process;


 	fnc:lx_fncapprox port map
	(
		clk_i      => clock.pulse,
		reset_i    => clock.reset,
		-- Data bus
		address_i  => addr_fnc,
		ce_i       => en_fnc,
		data_i     => din_fnc,
		data_o     => dout_fnc,
		--
		bls_i      => we_fnc
	);

    simple_gen:if simple=1 generate
		core: InterconnectSimple 
		generic map(
			debug       =>debug,
			unalign		=>unalign	--	we_dm<=c_we_dm;--TODO memory/axi switch?
			)
		port map (
			clk=>clock.pulse, 
			rst=>clock.reset, 
			trapped=>trapped,

			addr_hm=>   open,
			din_hm=>    open,
			en_hm=>     open,
			rst_hm=>    open,
			we_hm=>     open,
			dsel=>      x"00",

			addr_dm=>	addr_dm,
			din_dm=>	din_dm,
			dout_dm=>	dout_dm,
			en_dm=>		open,
			rst_dm=>	open,
			we_dm=>		we_dm,
			delayed_by_clk_dm=>delayed_dm,
			
			addr_im=>	addr_im,
			din_im=>	open,
			dout_im=>	dout_im,
			en_im=>		open,
			rst_im=>	open,
			we_im=>		open,
			delayed_by_clk_im=>mem_delayed
			);
	end generate simple_gen;
	pipe_gen:if simple=0 generate
		core: InterconnectPipeline 
		generic map(
			enable_M	=>enable_M,
			debug       =>debug,
			unalign		=>unalign
			)
		port map (
			clk=>clock.pulse, 
			rst=>clock.reset, 
			trapped=>trapped,

			addr_hm=>   open,
			din_hm=>    open,
			en_hm=>     open,
			rst_hm=>    open,
			we_hm=>     open,
			dsel=>      x"00",

			addr_dm=>	addr_dm,
			din_dm=>	din_dm,
			dout_dm=>	dout_dm,
			en_dm=>		open,
			rst_dm=>	open,
			we_dm=>		we_dm,
			delayed_by_clk_dm=>delayed_dm,
			wait_dm=>	dm_wait,
			req_dm=>	c_req_dm,
			
			addr_im=>	addr_im,
			din_im=>	open,
			dout_im=>	dout_im_filtered,
			en_im=>		open,
			rst_im=>	open,
			we_im=>		open,
			wait_im=>	im_wait,
			delayed_by_clk_im=>mem_delayed,
            req_im=>    open,

			pause=>     pause,
			issue=>     issue,
			step=>		step,
			debug_instr=>debug_instr,
			result=>    open
			);
	--	addr_dm<=c_addr_dm;
	--	din_dm<=c_din_dm;
	end generate pipe_gen;	

	irctrl:component InterruptController
	generic map(
		debug_exec_delay=>2
		)
	port map(
		clk				=>clock.pulse,
		rst				=>clock.reset,

		instr			=>debug_instr_irq,
		pause			=>pause_irq,
		issue			=>issue_irq,

		irq				=>irq_req,
		irq_address		=>irq_address,
		trap			=>trapped
	);

    memory: CombinedMemory_Target 
	generic map(
		dump_mem		=>true,
		output_filename	=>"rvapo_mem_dump.txt",
		delayed			=>mem_delayed
		)
	port map(
        clock=>clock, 

		addr_1=>c2_addr_dm,
		read_1=>c2_dout_dm,
		write_1=>c2_din_dm,
		web=>c2_we_dm,

		addr_2=>addr_im,
		read_2=>dout_im
        );

	mmap: MemoryMap 
    generic map(
    --    AXI_BASE	=>AXI_BASE,
		debug       =>debug,
		C_M_AXI_ID_WIDTH => 6
        )
    port map (
		aclk=>clock.pulse,
		rst=>clock.reset,

        addr                => addr_dm,
		din                 => din_dm,
		dout                => dout_dm,
		we                  => we_dm_filtered,
		req                 => req_dm,
        delayed             => delayed_dm,
        stall               => dm_wait,

        addr_dm             => c2_addr_dm,
        din_dm              => c2_din_dm,
        dout_dm             => c2_dout_dm,
        en_dm               => open,
        we_dm               => c2_we_dm,
        delayed_dm          => mem_delayed,

		addr_fnc			=> addr_fnc,	
		en_fnc				=> en_fnc,
		din_fnc				=> din_fnc,
		dout_fnc    		=> dout_fnc,
		we_fnc				=> we_fnc,

		M_AXI_AWID			=> M_AXI_AWID,
		M_AXI_AWADDR        => M_AXI_AWADDR,
        M_AXI_AWLEN         => M_AXI_AWLEN,
        M_AXI_AWSIZE        => M_AXI_AWSIZE,
        M_AXI_AWBURST       => M_AXI_AWBURST,
        M_AXI_AWCACHE       => M_AXI_AWCACHE,
        M_AXI_AWUSER        => M_AXI_AWUSER,
        M_AXI_AWVALID       => M_AXI_AWVALID,
        M_AXI_AWREADY       => '1',
        
        M_AXI_WDATA         => M_AXI_WDATA,
        M_AXI_WSTRB         => M_AXI_WSTRB,
        M_AXI_WLAST         => M_AXI_WLAST,
        M_AXI_WVALID        => M_AXI_WVALID,
        M_AXI_WREADY        => '1',
        
        M_AXI_BID          	=> "000000",
        M_AXI_BRESP         => "00", --response to write: OK
        M_AXI_BUSER         => M_AXI_BUSER,
        M_AXI_BVALID        => '1',
        M_AXI_BREADY        => M_AXI_BREADY,
        
        M_AXI_ARID				=>M_AXI_ARID,
        M_AXI_ARADDR            => M_AXI_ARADDR,
        M_AXI_ARLEN             => M_AXI_ARLEN,
        M_AXI_ARSIZE            => M_AXI_ARSIZE,
        M_AXI_ARBURST           => M_AXI_ARBURST,
        M_AXI_ARCACHE           => M_AXI_ARCACHE,
        M_AXI_ARUSER            => M_AXI_ARUSER,
        M_AXI_ARVALID           => M_AXI_ARVALID,   
        M_AXI_ARREADY           => '1',--mock, always read to accept address
        
        M_AXI_RID				=>"000000",
        M_AXI_RDATA             => x"2BCDDCBA",
        M_AXI_RRESP             => "00",
        M_AXI_RLAST             => '1',
        M_AXI_RUSER             => M_AXI_RUSER,
        M_AXI_RVALID            => '1',
        M_AXI_RREADY            => M_AXI_RREADY
		);
end architecture;
