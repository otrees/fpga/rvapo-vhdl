library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity rvapo_tb_alu is
end entity;

architecture tb of rvapo_tb_alu is
	type test_vector is record
		from_e: Intcon_E2ALU;
		to_e: Intcon_ALU2E;
	end record;

	type test_vector_array is array (natural range <>) of test_vector;
	constant test_vectors: test_vector_array := (
		(
			from_e => (alu_control => AluControl_ADD, srcA => x"00000010", srcB => x"FF00FF00"),
			to_e => (x"FF00FF10", (zero => '0'))
		),
		(
			from_e => (alu_control => AluControl_SUB, srcA => x"11111111", srcB => x"11111111"),
			to_e => (x"00000000", (zero => '1'))
		),
		(
			from_e => (alu_control => AluControl_SLL, srcA => x"12345678", srcB => x"00000001"),
			to_e => (x"2468ACF0", (zero => '0'))
		),
		(
			from_e => (alu_control => AluControl_SLT, srcA => x"F2222222", srcB => x"11111111"),
			to_e => (x"00000001", (zero => '0'))
		),
		(
			from_e => (alu_control => AluControl_SLTU, srcA => x"F2222222", srcB => x"11111111"),
			to_e => (x"00000000", (zero => '1'))
		),
		(
			from_e => (alu_control => AluControl_XOR, srcA => x"00000F01", srcB => x"00000F10"),
			to_e => (x"00000011", (zero => '0'))
		),
		(
			from_e => (alu_control => AluControl_SRL, srcA => x"12345678", srcB => x"00000008"),
			to_e => (x"00123456", (zero => '0'))
		),
		(
			from_e => (alu_control => AluControl_SRA, srcA => x"F2345678", srcB => x"00000009"),
			to_e => (x"FFF91A2B", (zero => '0'))
		),
		(
			from_e => (alu_control => AluControl_OR, srcA => x"11111111", srcB => x"0111111F"),
			to_e => (x"1111111F", (zero => '0'))
		),
		(
			from_e => (alu_control => AluControl_AND, srcA => x"30000010", srcB => x"1F00FFF0"),
			to_e => (x"10000010", (zero => '0'))
		)
	);

	signal actual_test: test_vector;
	signal expected_test: test_vector;
begin
	eut: ArithmeticLogicUnit port map (
		actual_test.from_e,
		actual_test.to_e
	);

	process
		variable expected_test_var: test_vector;
	begin
		for i in test_vectors'range loop
			expected_test_var := test_vectors(i);
			expected_test <= expected_test_var;

			actual_test.from_e <= expected_test_var.from_e;
			wait for 1 ns;

			assert actual_test.to_e = expected_test_var.to_e
			report "invalid to_e" severity error;
		end loop;

		assert false report "Arithmetic logic unit tests passed" severity note;
		wait;
	end process;
end architecture;