library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity rvapo_tb_exec is
end entity;

architecture tb of rvapo_tb_exec is
	type test_vector is record
		from_d: Intcon_D2E;
		from_hu: Intcon_HU2E;
		from_alu: Intcon_ALU2E;
		from_m: Intcon_M2E;
		from_w: Intcon_W2E;
		to_hu: Intcon_E2HU;
		to_alu: Intcon_E2ALU;
		to_m: Intcon_E2M;
	end record;

	type test_vector_array is array (natural range <>) of test_vector;
	constant test_vectors: test_vector_array := (
		(
			from_d => (
				pc => x"00000000", pc_plus_4 => x"00000004",
				rs1 => "00001", rs2 => "00010", rd => "00101",
				val1 => x"00000001", val2 => x"11111111", valI => x"22222222",
				control => (
					BranchControl_None,
					reg_write => '0',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				),
				alu_control => AluControl_ADD,
				alu_source => AluSource_Reg
			),
			from_hu => (ForwardValue_None, ForwardValue_None),
			from_alu => (x"33333333", (zero => '0')),
			from_m => (result_data => x"44444444"),
			from_w => (result => x"55555555"),
			to_hu => (
				rs1 => "00001", rs2 => "00010", rd => "00101", mem_to_reg => '0'
			),
			to_alu => (AluControl_ADD, x"00000001", x"11111111"),
			to_m => (
				pc_plus_4 => x"00000004", pc_plus_imm => x"22222222",
				alu_out => x"33333333", alu_flag => (zero => '0'),
				write_data => x"11111111",
				rd => "00101",
				control => (
					BranchControl_None,
					reg_write => '0',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				)
			)
		),
		(
			from_d => (
				pc => x"00000000", pc_plus_4 => x"00000004",
				rs1 => "00001", rs2 => "00010", rd => "00101",
				val1 => x"00000001", val2 => x"11111111", valI => x"22222222",
				control => (
					BranchControl_None,
					reg_write => '0',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				),
				alu_control => AluControl_ADD,
				alu_source => AluSource_Reg
			),
			from_hu => (ForwardValue_Memory, ForwardValue_Writeback),
			from_alu => (x"33333333", (zero => '0')),
			from_m => (result_data => x"44444444"),
			from_w => (result => x"55555555"),
			to_hu => (
				rs1 => "00001", rs2 => "00010", rd => "00101", mem_to_reg => '0'
			),
			to_alu => (AluControl_ADD, x"44444444", x"55555555"),
			to_m => (
				pc_plus_4 => x"00000004", pc_plus_imm => x"22222222",
				alu_out => x"33333333", alu_flag => (zero => '0'),
				write_data => x"55555555",
				rd => "00101",
				control => (
					BranchControl_None,
					reg_write => '0',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				)
			)
		),
		(
			from_d => (
				pc => x"00000000", pc_plus_4 => x"00000004",
				rs1 => "00001", rs2 => "00010", rd => "00101",
				val1 => x"00000001", val2 => x"11111111", valI => x"22222222",
				control => (
					BranchControl_BNE,
					reg_write => '1',
					mem_to_reg => '1',
					mem_write => '1',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '1',
					trap => Trap_Break
				),
				alu_control => AluControl_ADD,
				alu_source => AluSource_Imm
			),
			from_hu => (ForwardValue_Writeback, ForwardValue_Writeback),
			from_alu => (x"00000000", (zero => '1')),
			from_m => (result_data => x"44444444"),
			from_w => (result => x"55555555"),
			to_hu => (
				rs1 => "00001", rs2 => "00010", rd => "00101", mem_to_reg => '1'
			),
			to_alu => (AluControl_ADD, x"55555555", x"22222222"),
			to_m => (
				pc_plus_4 => x"00000004", pc_plus_imm => x"22222222",
				alu_out => x"00000000", alu_flag => (zero => '1'),
				write_data => x"55555555",
				rd => "00101",
				control => (
					BranchControl_BNE,
					reg_write => '1',
					mem_to_reg => '1',
					mem_write => '1',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '1',
					trap => Trap_Break
				)
			)
		)
	);

	signal actual_test: test_vector;
	signal expected_test: test_vector;
begin
	eut: StageExecute port map (
		actual_test.from_d,
		actual_test.from_hu,
		actual_test.from_alu,
		actual_test.from_m,
		actual_test.from_w,
		actual_test.to_hu,
		actual_test.to_alu,
		actual_test.to_m
	);

	process
		variable expected_test_var: test_vector;
	begin
		for i in test_vectors'range loop
			expected_test_var := test_vectors(i);
			expected_test <= expected_test_var;

			actual_test.from_d <= expected_test_var.from_d;
			actual_test.from_hu <= expected_test_var.from_hu;
			actual_test.from_alu <= expected_test_var.from_alu;
			actual_test.from_m <= expected_test_var.from_m;
			actual_test.from_w <= expected_test_var.from_w;
			wait for 1 ns;

			assert actual_test.to_hu = expected_test_var.to_hu
			report "invalid to_hu" severity failure;
			assert actual_test.to_alu = expected_test_var.to_alu
			report "invalid to_alu" severity failure;
			assert actual_test.to_m = expected_test_var.to_m
			report "invalid to_m" severity failure;
		end loop;

		assert false report "Execute tests passed" severity note;
		wait;
	end process;
end architecture;
