library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity rvapo_tb_mem is
end entity;

architecture tb of rvapo_tb_mem is
	type test_vector is record
		from_e: Intcon_E2M;
		from_dm: Intcon_DM2M;
		to_hu: Intcon_M2HU;
		to_bu: Intcon_M2BU;
		to_dm: Intcon_M2DM;
		to_e: Intcon_M2E;
		to_w: Intcon_M2W;
	end record;

	type test_vector_array is array (natural range <>) of test_vector;
	constant test_vectors: test_vector_array := (
		(
			from_e => (
				pc_plus_4 => x"11111115", pc_plus_imm => x"22222222",
				alu_out => x"33333333", alu_flag => (zero => '0'),
				write_data => x"44444444",
				rd => "00001",
				control => (
					BranchControl_None,
					reg_write => '0',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				)
			),
			from_dm => (read_data => x"55555555"),
			to_hu => ("00001", reg_write => '0', trap => Trap_None),
			to_bu => (
				x"33333333", (zero => '0'),
				x"22222222", BranchControl_None
			),
			to_dm => (x"33333333", x"44444444", mem_write => '0', mem_control => MemoryControl_Word),
			to_e => (result_data => x"33333333"),
			to_w => (x"55555555", x"33333333", "00001", reg_write => '0', mem_to_reg => '0')
		),
		(
			from_e => (
				pc_plus_4 => x"11111115", pc_plus_imm => x"22222222",
				alu_out => x"33333333", alu_flag => (zero => '0'),
				write_data => x"44444444",
				rd => "00000",
				control => (
					BranchControl_JALR,
					reg_write => '1',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Byte,
					pc_plus_imm_to_reg => '0',
					trap => Trap_None
				)
			),
			from_dm => (read_data => x"55555555"),
			to_hu => ("00000", reg_write => '1', trap => Trap_None),
			to_bu => (
				x"33333333", (zero => '0'),
				x"22222222", BranchControl_JALR
			),
			to_dm => (x"33333333", x"44444444", mem_write => '0', mem_control => MemoryControl_Byte),
			to_e => (result_data => x"11111115"),
			to_w => (x"55555555", x"11111115", "00000", reg_write => '1', mem_to_reg => '0')
		),
		(
			from_e => (
				pc_plus_4 => x"11111115", pc_plus_imm => x"22222222",
				alu_out => x"33333333", alu_flag => (zero => '0'),
				write_data => x"44444444",
				rd => "00001",
				control => (
					BranchControl_BEQ,
					reg_write => '0',
					mem_to_reg => '1',
					mem_write => '1',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '0',
					trap => Trap_Fault
				)
			),
			from_dm => (read_data => x"55555555"),
			to_hu => ("00001", reg_write => '0', trap => Trap_Fault),
			to_bu => (
				x"33333333", (zero => '0'),
				x"22222222", BranchControl_BEQ
			),
			to_dm => (x"33333333", x"44444444", mem_write => '1', mem_control => MemoryControl_Word),
			to_e => (result_data => x"33333333"),
			to_w => (x"55555555", x"33333333", "00001", reg_write => '0', mem_to_reg => '1')
		),
		(
			from_e => (
				pc_plus_4 => x"11111115", pc_plus_imm => x"22222222",
				alu_out => x"33333333", alu_flag => (zero => '0'),
				write_data => x"44444444",
				rd => "00001",
				control => (
					BranchControl_None,
					reg_write => '0',
					mem_to_reg => '0',
					mem_write => '0',
					mem_control => MemoryControl_Word,
					pc_plus_imm_to_reg => '1',
					trap => Trap_None
				)
			),
			from_dm => (read_data => x"55555555"),
			to_hu => ("00001", reg_write => '0', trap => Trap_None),
			to_bu => (
				x"33333333", (zero => '0'),
				x"22222222", BranchControl_None
			),
			to_dm => (x"33333333", x"44444444", mem_write => '0', mem_control => MemoryControl_Word),
			to_e => (result_data => x"22222222"),
			to_w => (x"55555555", x"22222222", "00001", reg_write => '0', mem_to_reg => '0')
		)
	);

	signal actual_test: test_vector;
	signal expected_test: test_vector;
begin
	eut: StageMemory port map (
		actual_test.from_e,
		actual_test.from_dm,
		actual_test.to_hu,
		actual_test.to_bu,
		actual_test.to_dm,
		actual_test.to_e,
		actual_test.to_w
	);

	process
		variable expected_test_var: test_vector;
	begin
		for i in test_vectors'range loop
			expected_test_var := test_vectors(i);
			expected_test <= expected_test_var;

			actual_test.from_e <= expected_test_var.from_e;
			actual_test.from_dm <= expected_test_var.from_dm;
			wait for 1 ns;

			assert actual_test.to_hu = expected_test_var.to_hu
			report "invalid to_hu" severity failure;
			assert actual_test.to_bu = expected_test_var.to_bu
			report "invalid to_bu" severity failure;
			assert actual_test.to_dm = expected_test_var.to_dm
			report "invalid to_dm" severity failure;
			assert actual_test.to_e = expected_test_var.to_e
			report "invalid to_e" severity failure;
			assert actual_test.to_w = expected_test_var.to_w
			report "invalid to_w" severity failure;
		end loop;

		assert false report "Memory tests passed" severity note;
		wait;
	end process;
end architecture;