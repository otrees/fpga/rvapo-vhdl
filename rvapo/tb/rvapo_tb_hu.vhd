library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity rvapo_tb_hu is
end entity;

architecture tb of rvapo_tb_hu is
	type test_vector is record
		from_bu: Intcon_BU2HU;
		from_d: Intcon_D2HU;
		from_e: Intcon_E2HU;
		from_m: Intcon_M2HU;
		from_w: Intcon_W2HU;
		to_e: Intcon_HU2E;
		hazards: Hazards_t;
	end record;

	type test_vector_array is array (natural range <>) of test_vector;
	constant test_vectors: test_vector_array := (
		-- no collisions
		(
			from_bu => (branch_outcome => '0'),
			from_d => (rs1 => "00001", rs2 => "00010"),
			from_e => (rs1 => "00011", rs2 => "00100", rd => "00101", mem_to_reg => '0'),
			from_m => (rd => "00110", reg_write => '0', trap => Trap_None),
			from_w => (rd => "00111", reg_write => '0'),
			to_e => (ForwardValue_None, ForwardValue_None),
			hazards => (
				stall_fetch => '0',
				stall_decode => '0',
				flush_decode => '0',
				flush_execute => '0',
				flush_memory => '0',
				trap => Trap_None
			)
		),
		-- branch is happening
		(
			from_bu => (branch_outcome => '1'),
			from_d => (rs1 => "00001", rs2 => "00010"),
			from_e => (rs1 => "00011", rs2 => "00100", rd => "00101", mem_to_reg => '0'),
			from_m => (rd => "00110", reg_write => '0', trap => Trap_None),
			from_w => (rd => "00111", reg_write => '0'),
			to_e => (ForwardValue_None, ForwardValue_None),
			hazards => (
				stall_fetch => '0',
				stall_decode => '0',
				flush_decode => '1',
				flush_execute => '1',
				flush_memory => '1',
				trap => Trap_None
			)
		),
		-- trap instruction
		(
			from_bu => (branch_outcome => '0'),
			from_d => (rs1 => "00001", rs2 => "00010"),
			from_e => (rs1 => "00011", rs2 => "00100", rd => "00101", mem_to_reg => '0'),
			from_m => (rd => "00110", reg_write => '0', trap => Trap_Call),
			from_w => (rd => "00111", reg_write => '0'),
			to_e => (ForwardValue_None, ForwardValue_None),
			hazards => (
				stall_fetch => '0',
				stall_decode => '0',
				flush_decode => '0',
				flush_execute => '0',
				flush_memory => '0',
				trap => Trap_Call
			)
		),
		-- destination collision without reg_write
		(
			from_bu => (branch_outcome => '0'),
			from_d => (rs1 => "00001", rs2 => "00010"),
			from_e => (rs1 => "00011", rs2 => "00100", rd => "00101", mem_to_reg => '0'),
			from_m => (rd => "00011", reg_write => '0', trap => Trap_None),
			from_w => (rd => "00100", reg_write => '0'),
			to_e => (ForwardValue_None, ForwardValue_None),
			hazards => (
				stall_fetch => '0',
				stall_decode => '0',
				flush_decode => '0',
				flush_execute => '0',
				flush_memory => '0',
				trap => Trap_None
			)
		),
		-- forwarding
		(
			from_bu => (branch_outcome => '0'),
			from_d => (rs1 => "00001", rs2 => "00010"),
			from_e => (rs1 => "00011", rs2 => "00100", rd => "00101", mem_to_reg => '0'),
			from_m => (rd => "00011", reg_write => '1', trap => Trap_None),
			from_w => (rd => "00100", reg_write => '1'),
			to_e => (ForwardValue_Memory, ForwardValue_Writeback),
			hazards => (
				stall_fetch => '0',
				stall_decode => '0',
				flush_decode => '0',
				flush_execute => '0',
				flush_memory => '0',
				trap => Trap_None
			)
		),
		-- D reads from register which is filled from memory by E - stall
		(
			from_bu => (branch_outcome => '0'),
			from_d => (rs1 => "00001", rs2 => "00010"),
			from_e => (rs1 => "00011", rs2 => "00100", rd => "00001", mem_to_reg => '1'),
			from_m => (rd => "00110", reg_write => '0', trap => Trap_None),
			from_w => (rd => "00111", reg_write => '0'),
			to_e => (ForwardValue_None, ForwardValue_None),
			hazards => (
				stall_fetch => '1',
				stall_decode => '1',
				flush_decode => '0',
				flush_execute => '1',
				flush_memory => '0',
				trap => Trap_None
			)
		),
		-- D reads from register which is written to by W - stall
		(
			from_bu => (branch_outcome => '0'),
			from_d => (rs1 => "00001", rs2 => "00010"),
			from_e => (rs1 => "00011", rs2 => "00100", rd => "00101", mem_to_reg => '0'),
			from_m => (rd => "00110", reg_write => '0', trap => Trap_None),
			from_w => (rd => "00001", reg_write => '1'),
			to_e => (ForwardValue_None, ForwardValue_None),
			hazards => (
				stall_fetch => '1',
				stall_decode => '1',
				flush_decode => '0',
				flush_execute => '1',
				flush_memory => '0',
				trap => Trap_None
			)
		)
	);

	signal actual_test: test_vector;
	signal expected_test: test_vector;
begin
	eut: HazardUnit port map (
		actual_test.from_bu,
		actual_test.from_d,
		actual_test.from_e,
		actual_test.from_m,
		actual_test.from_w,
		actual_test.to_e,
		actual_test.hazards
	);

	process
		variable expected_test_var: test_vector;
	begin
		for i in test_vectors'range loop
			expected_test_var := test_vectors(i);
			expected_test <= expected_test_var;

			actual_test.from_bu <= expected_test_var.from_bu;
			actual_test.from_d <= expected_test_var.from_d;
			actual_test.from_e <= expected_test_var.from_e;
			actual_test.from_m <= expected_test_var.from_m;
			actual_test.from_w <= expected_test_var.from_w;
			wait for 1 ns;

			assert actual_test.to_e = expected_test_var.to_e
			report "invalid to_e" severity failure;

			assert actual_test.hazards = expected_test_var.hazards
			report "invalid hazards" severity failure;
		end loop;

		assert false report "Hazard unit tests passed" severity note;
		wait;
	end process;
end architecture;