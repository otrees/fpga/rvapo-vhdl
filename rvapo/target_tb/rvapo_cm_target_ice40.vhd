library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;
use WORK.ice40_memfill.all;
--use WORK.rvapo_ice40_mem_init.all;

entity CombinedMemory_Delayed is
	port (
		clock: in Clock_t;

		addr_1: in BusWidth_t;
		read_1: out BusWidth_t;
		write_1: in BusWidth_t;
		web: in std_logic_vector(3 downto 0);

		addr_2: in BusWidth_t;
		read_2: out BusWidth_t
	);
end;

architecture rtl of CombinedMemory_Delayed is
	component SB_SPRAM256KA is port(
		DATAIN:in std_logic_vector(15 downto 0);
		ADDRESS:in std_logic_vector(13 downto 0);
		MASKWREN:in std_logic_vector(3 downto 0);
		WREN: in std_logic;
		CHIPSELECT: in std_logic;
		CLOCK: in std_logic;
		STANDBY:in std_logic;
		SLEEP:in std_logic;
		POWEROFF:in std_logic;
		DATAOUT:out std_logic_vector(15 downto 0)
		);
	end component;

	component SB_RAM40_4K is generic(
  		INIT_0:std_logic_vector(255 downto 0);
		INIT_1:std_logic_vector(255 downto 0);
		INIT_2:std_logic_vector(255 downto 0);
		INIT_3:std_logic_vector(255 downto 0);
		INIT_4:std_logic_vector(255 downto 0);
		INIT_5:std_logic_vector(255 downto 0);
		INIT_6:std_logic_vector(255 downto 0);
		INIT_7:std_logic_vector(255 downto 0);
		INIT_8:std_logic_vector(255 downto 0);
		INIT_9:std_logic_vector(255 downto 0);
		INIT_A:std_logic_vector(255 downto 0);
		INIT_B:std_logic_vector(255 downto 0);
		INIT_C:std_logic_vector(255 downto 0);
		INIT_D:std_logic_vector(255 downto 0);
		INIT_E:std_logic_vector(255 downto 0);
		INIT_F:std_logic_vector(255 downto 0);
  		WRITE_MODE:integer;
  		READ_MODE:integer
		); 
	port (
		RDATA:	out std_logic_vector(15 downto 0);
		RADDR:	in std_logic_vector(10 downto 0);
		WADDR:	in std_logic_vector(10 downto 0);
		MASK:	in std_logic_vector(15 downto 0);
		WDATA:	in std_logic_vector(15 downto 0);
		RCLKE: 	in std_logic;	
		RCLK: 	in std_logic;
		RE: 	in std_logic;		
		WCLKE: 	in std_logic;		
		WCLK: 	in std_logic;
		WE: 	in std_logic
		);
	end component;

--	subtype Byte_t is std_logic_vector(7 downto 0);
--	type memory_array_t is array (0 to 4+4) of Byte_t;
--	shared variable memory: memory_array_t;-- := (others=>x"00");--MEMORY_INIT
--	subtype Index_t is unsigned(BusWidth_t'range);
--	constant MEMORY_HIGH_INDEX: Index_t := to_unsigned(memory'high - 4, BusWidth_t'length);
	constant BYTE_1:std_logic_vector(7 downto 0):=x"01";
	signal ei:std_logic_vector(7 downto 0):=(others=>'0');
	signal red:std_logic_vector(7 downto 0):=(others=>'0');
	signal wed:std_logic_vector(7 downto 0):=(others=>'0');
	subtype Half_t is std_logic_vector(15 downto 0);
	type ram_out is array(0 to 7) of Half_t; 
	signal ri_lo:ram_out;
	signal ri_hi:ram_out;
	signal rd_lo:ram_out;
	signal rd_hi:ram_out;
	signal we_mask:std_logic_vector(31 downto 0);
begin
	IRAM_GENERATE: for i in 0 to 6 generate
		ram256x16_inst_ilo : SB_RAM40_4K generic map (
INIT_0=>ICE40MEM1LO_0,
INIT_1=>ICE40MEM1LO_1,
INIT_2=>ICE40MEM1LO_2,
INIT_3=>ICE40MEM1LO_3,
INIT_4=>ICE40MEM1LO_4,
INIT_5=>ICE40MEM1LO_5,
INIT_6=>ICE40MEM1LO_6,
INIT_7=>ICE40MEM1LO_7,
INIT_8=>ICE40MEM1LO_8,
INIT_9=>ICE40MEM1LO_9,
INIT_A=>ICE40MEM1LO_10,
INIT_B=>ICE40MEM1LO_11,
INIT_C=>ICE40MEM1LO_12,
INIT_D=>ICE40MEM1LO_13,
INIT_E=>ICE40MEM1LO_14,
INIT_F=>ICE40MEM1LO_15,
  		WRITE_MODE=>0,
  		READ_MODE=>0
			)
		port map (
			RDATA => ri_lo(i),
			RADDR => addr_2(12 downto 2),
			RCLK => clock.pulse,
			RCLKE => '1',
			RE => ei(i),
			WADDR => (others=>'0'),
			WCLK=> clock.pulse,
			WCLKE => '1',
			WDATA => (others=>'0'),
			MASK => (others=>'1'),
			WE => '0'
		);
		ram256x16_inst_ihi : SB_RAM40_4K generic map (
INIT_0=>ICE40MEM1HI_0,
INIT_1=>ICE40MEM1HI_1,
INIT_2=>ICE40MEM1HI_2,
INIT_3=>ICE40MEM1HI_3,
INIT_4=>ICE40MEM1HI_4,
INIT_5=>ICE40MEM1HI_5,
INIT_6=>ICE40MEM1HI_6,
INIT_7=>ICE40MEM1HI_7,
INIT_8=>ICE40MEM1HI_8,
INIT_9=>ICE40MEM1HI_9,
INIT_A=>ICE40MEM1HI_10,
INIT_B=>ICE40MEM1HI_11,
INIT_C=>ICE40MEM1HI_12,
INIT_D=>ICE40MEM1HI_13,
INIT_E=>ICE40MEM1HI_14,
INIT_F=>ICE40MEM1HI_15,
  		WRITE_MODE=>0,
  		READ_MODE=>0
			)
		port map (
			RDATA => ri_hi(i),
			RADDR => addr_2(12 downto 2),--actually only uses lower 8 bits in mode 0
			RCLK => clock.pulse,
			RCLKE => '1',
			RE => ei(i),
			WADDR => (others=>'0'),
			WCLK=> clock.pulse,
			WCLKE => '1',
			WDATA => (others=>'0'),
			MASK => (others=>'1'),
			WE => '0'
		);
	end generate IRAM_GENERATE;


--INIT_0=>X"0000000000000000000000000000000000000000000000000000000000005757" in lo
--INIT_0=>X"0000000000000000000000000000000000000000000000000000000002005858" in hi 
--produces X"57575858" = WWXX at addr 0

--INIT_0=>X"0000000000000000000000000000000000000000000000000000000000004546" in lo
--INIT_0=>X"0000000000000000000000000000000000000000000000000000000002004748" in hi
--produces X"45464748" = EFGH at addr 0

--INIT_0=>X"000000000000000000000000000000000000000000000000000000000000fe01" in lo
--INIT_0=>X"0000000000000000000000000000000000000000000000000000000002000113" in hi
--produces X"fe010113" at addr 0

--INIT_0=>X"0000000000000000000000000000000000000000000000000000000000000000" in lo
--INIT_0=>X"0000000000000000000000000000000000000000000000000000000002001000" in hi
--produces X"00001000" at addr 0
	DRAM_GENERATE: for i in 0 to 7 generate
		ram256x16_inst_dlo : SB_RAM40_4K generic map (
INIT_0=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_1=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_2=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_3=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_4=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_5=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_6=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_7=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_8=>X"001000F700F7FE84FE44FEF4FFF0FEF40000FEA4044000F0020100810011FE01",
INIT_9=>X"0010FEC40007FEC4FEA40201009100810011FE0100000201018101C100070000",
INIT_A=>X"0005FA1F0007FFE7FEC40005FB5F0007FFF7FEC40300001003800000014000F7",
INIT_B=>X"000000000000000000000000000000000000000002010141018101C1000700F4",
INIT_C=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_D=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_E=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_F=>X"0000000000000000000000000000000000000000000000000000000000000000",
  		WRITE_MODE=>0,
  		READ_MODE=>0
		)
		port map (
			RDATA => rd_lo(i),
			RADDR => addr_1(12 downto 2),
			RCLK => clock.pulse,
			RCLKE => '1',
			RE => red(i),
			WADDR => addr_1(12 downto 2),
			WCLK=> clock.pulse,
			WCLKE => '1',
			WDATA => write_1(31 downto 16),
			MASK => we_mask(31 downto 16),
			WE => wed(i)
		);
		ram256x16_inst_dhi : SB_RAM40_4K generic map (--YZDE
--INIT_0=>X"0000000000000000000000000000000000000000000000000000000002000113",	--this is second n psrd, also in vhdl
INIT_0=>X"0000000000000000000000000000000000000000000000000000000002001000",
INIT_1=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_2=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_3=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_4=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_5=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_6=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_7=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_8=>X"0073242322232703278322230793242317B7262300EF051304132C232E230113",
INIT_9=>X"079327038A632783262304132A232C232E230113806701132403208385130793",
INIT_A=>X"0793F0EF8513879327830493F0EF851387932783006F0793006F0793006F0863",
INIT_B=>X"00000000000000000000000000000000000080670113248324032083851387B3",
INIT_C=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_D=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_E=>X"0000000000000000000000000000000000000000000000000000000000000000",
INIT_F=>X"0000000000000000000000000000000000000000000000000000000000000000",
  		WRITE_MODE=>0,
  		READ_MODE=>0
		)
		port map (
			RDATA => rd_hi(i),
			RADDR => addr_1(12 downto 2),
			RCLK => clock.pulse,
			RCLKE => '1',
			RE => red(i),
			WADDR => addr_1(12 downto 2),
			WCLK=> clock.pulse,
			WCLKE => '1',
			WDATA => write_1(15 downto 0),
			MASK => we_mask(15 downto 0),
			WE => wed(i)
		);
	end generate DRAM_GENERATE;

	ei<=std_logic_vector(shift_left(unsigned(BYTE_1),to_integer(unsigned(addr_2(12 downto 10)))));
--	wed<=x"00" when web="0000" else std_logic_vector(shift_left(unsigned(x"01"),to_integer(unsigned(addr1(10 downto 8)))));
	we_mask(7 downto 0)<=(others=>not web(0));
	we_mask(15 downto 8)<=(others=>not web(1));
	we_mask(23 downto 16)<=(others=>not web(2));
	we_mask(31 downto 24)<=(others=>not web(3));

	data_enable_ctrl:process(clock,addr_1,web) 
	variable shifted:std_logic_vector(7 downto 0);
	begin
		shifted:=std_logic_vector(shift_left(unsigned(BYTE_1),to_integer(unsigned(addr_1(12 downto 10)))));
		if web="0000" then
			red<=shifted;
			wed<=(others=>'0');
		else
			wed<=shifted;
			red<=(others=>'0');
		end if;
	end process;

	read_mux:process(clock,rd_lo,rd_hi,ri_lo,ri_hi,ei,red)begin
		with addr_2(12 downto 10) select read_2<=
			ri_lo(0)&ri_hi(0) when "000",
			ri_lo(1)&ri_hi(1) when "001",
			ri_lo(2)&ri_hi(2) when "010",
			ri_lo(3)&ri_hi(3) when "011",
			ri_lo(4)&ri_hi(4) when "100",
			ri_lo(5)&ri_hi(5) when "101",
			ri_lo(6)&ri_hi(6) when "110",
			ri_lo(7)&ri_hi(7) when "111",
			(others=>'0')  when others;
		with addr_1(12 downto 10) select read_1<=
			rd_lo(0)&rd_hi(0) when "000",
			rd_lo(1)&rd_hi(1) when "001",
			rd_lo(2)&rd_hi(2) when "010",
			rd_lo(3)&rd_hi(3) when "011",
			rd_lo(4)&rd_hi(4) when "100",
			rd_lo(5)&rd_hi(5) when "101",
			rd_lo(6)&rd_hi(6) when "110",
			rd_lo(7)&rd_hi(7) when "111",
			(others=>'0')  when others;
	end process;
end;
