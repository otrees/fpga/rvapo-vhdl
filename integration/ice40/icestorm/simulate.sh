#!/bin/bash
sources=(
	'rvapo_pkg.vhd' '../../integration/ice40/icestorm/ice40memfill.vhd' 'rvapo_gpr_target.vhd'
#	'../target_tb/rvapo_ice40_mem_init.vhd' '../../software/out/vhdl/fiboc.vhdl' 	
	'stage/rvapo_pc.vhd' 'stage/rvapo_fetch.vhd' 'stage/rvapo_dec.vhd' 'stage/rvapo_exec.vhd' 'stage/rvapo_mem.vhd' 'stage/rvapo_wb.vhd'
	'logic/rvapo_hu.vhd' 'logic/rvapo_alu_small.vhd' 'logic/rvapo_bu.vhd'
	'memory/rvapo_gpr.vhd' 'memory/rvapo_cm.vhd' 
	'rvapo_intcon_ice40.vhd' 
	'../spi/spi_master.vhd'
	'../wrapper_spi.vhd' '../wrapper_spimem.vhd'
#	'../target_tb/rvapo_cm_target_init.vhdl'
	'../target_tb/rvapo_cm_target_ice40.vhd'	'../target_tb/rvapo_ice40_mem_impl.vhd' 
	'../tb/rvapo_tb_ice40.vhd'
	)

analyze() {
	#~/Documents/libs/ghdl/ghdl_mcode -a --std=93c --ieee=synopsys -fexplicit -g --workdir='workdir' "$1"
	#~/Documents/libs/ghdl/ghdl_mcode -a --std=93c --ieee=synopsys -fexplicit -g "$1"	
	ghdl -a --std=93c --ieee=synopsys -fexplicit -g "$1"	
	}

elaborate() {
	ghdl -e --std=93c --ieee=synopsys -fexplicit -g "$1"
	}

run() {
	elaborate "$1"
	local assert_level=${2:-failure}
	ghdl -r --std=93c --ieee=synopsys -fexplicit -g "$1" --wave="$1.ghw" --assert-level="$assert_level"
	}	

if [ $# -eq 0 ] ; then
	echo "Provide extensionless name of .vhdl file with memory content."
	exit
fi

rm ./*.cf
rm ./*.o
ln -sf "../../../software/out/ice40/$1.vhdl" ice40memfill.vhd

for file in "${sources[@]}"; do
	analyze "../../../rvapo/core/$file"
done

run "rvapo_tb_ice40";

