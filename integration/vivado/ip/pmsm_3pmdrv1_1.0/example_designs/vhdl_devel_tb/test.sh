#!/bin/bash

set -e

sources=(
	'../../hdl/adc_reader.vhdl'
	'../../hdl/cnt_div.vhdl'
	'../../hdl/dff.vhdl'
	'../../hdl/dff3.vhdl'
	'../../hdl/mcpwm.vhdl'
	'../../hdl/pmsm_3pmdrv1_v1_0_S_AXI_INTR.vhd'
	'../../hdl/pmsm_3pmdrv1_v1_0_S00_AXI.vhd'
	'../../hdl/qcounter_nbit.vhdl'
	'../../hdl/pmsm_3pmdrv1_v1_0.vhd'
	'pmsm_tb.vhd'
)

BSRC=$(dirname $BASH_SOURCE)
clean() {
	rm -rf "$BSRC/workdir"
}

analyze() {
	ghdl -a --std=93c --ieee=synopsys -fexplicit -g --workdir="$BSRC/workdir" "$1"
}

elaborate() {
	ghdl -e --std=93c --ieee=synopsys -fexplicit -g --workdir="$BSRC/workdir" "$1"
}

run() {
	elaborate "$1"
	local assert_level=${2:-failure}
	ghdl -r --std=93c --ieee=synopsys -fexplicit -g --workdir="$BSRC/workdir" "$1" --wave="$BSRC/workdir/$1.ghw" --assert-level="$assert_level"
}

rm -rf workdir
mkdir workdir

for file in "${sources[@]}"; do
	analyze "$BSRC/$file"
done

run "pmsm_tb"
