library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rvapo_ctrl_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 6
	);
	port (
		-- Users to add ports here
		cpu_step: out std_logic;
        cpu_pause: out std_logic;
		cpu_reset	: out std_logic;
       	trapped     : in std_logic_vector(5 downto 0);
        riscv_pc : in std_logic_vector(31 downto 0); 
        instr : in std_logic_vector(31 downto 0); 
        dsel: out std_logic_vector(7 downto 0);       
        debugger_issue:out std_logic;
        debugger_instr:out std_logic_vector(31 downto 0); 
        debugger_result:in std_logic_vector(31 downto 0); 
        arm_base:out std_logic_vector(31 downto 0); 
        arm_top:out std_logic_vector(31 downto 0); 
        xmax:out std_logic_vector(15 downto 0);
        ymax:out std_logic_vector(15 downto 0);
        hsync_len:out std_logic_vector(7 downto 0); 
        hsync_f:out std_logic_vector(7 downto 0); 
        hsync_b:out std_logic_vector(7 downto 0); 
        vsync_len:out std_logic_vector(7 downto 0); 
        vsync_f:out std_logic_vector(7 downto 0); 
        vsync_b:out std_logic_vector(7 downto 0); 
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end rvapo_ctrl_v1_0;

architecture arch_imp of rvapo_ctrl_v1_0 is

	-- component declaration
	component rvapo_ctrl_v1_0_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
		);
		port (
		-- Users to add ports here
        cpu_step: out std_logic;
        cpu_pause: out std_logic;
        cpu_reset    : out std_logic;
        trapped     : in std_logic_vector(5 downto 0);
        riscv_pc : in std_logic_vector(31 downto 0); 
        instr : in std_logic_vector(31 downto 0); 
        dsel: out std_logic_vector(7 downto 0);       
        debugger_issue:out std_logic;
        debugger_instr:out std_logic_vector(31 downto 0); 
        debugger_result:in std_logic_vector(31 downto 0); 
        arm_base:out std_logic_vector(31 downto 0); 
        arm_top:out std_logic_vector(31 downto 0); 
        xmax:out std_logic_vector(15 downto 0);
        ymax:out std_logic_vector(15 downto 0);
        hsync_len:out std_logic_vector(7 downto 0); 
        hsync_f:out std_logic_vector(7 downto 0); 
        hsync_b:out std_logic_vector(7 downto 0); 
        vsync_len:out std_logic_vector(7 downto 0); 
        vsync_f:out std_logic_vector(7 downto 0); 
        vsync_b:out std_logic_vector(7 downto 0); 
        -- User ports ends
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component rvapo_ctrl_v1_0_S00_AXI;

		-- User ports ends

begin

-- Instantiation of Axi Bus Interface S00_AXI
rvapo_ctrl_v1_0_S00_AXI_inst : rvapo_ctrl_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
	    cpu_step   => cpu_step,
        cpu_pause	=> cpu_pause,
		cpu_reset	=> cpu_reset,
		trapped =>trapped,
		riscv_pc=>riscv_pc,
		instr=>instr,
		dsel=>dsel,
		debugger_issue=>debugger_issue,
        debugger_instr=>debugger_instr,
        debugger_result=>debugger_result,
        arm_base=>arm_base,
        arm_top=>arm_top,
        xmax=>xmax,
        ymax=>ymax,
        hsync_len=>hsync_len, 
        hsync_f=>hsync_f, 
        hsync_b=>hsync_b,
        vsync_len=>vsync_len,
        vsync_f=>vsync_f,
        vsync_b=>vsync_b,
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

	-- Add user logic here

	-- User logic ends

end arch_imp;
