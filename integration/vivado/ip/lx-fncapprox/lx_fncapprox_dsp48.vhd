library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.lx_fncapprox_pkg.all;

-- IRC bus interconnect
entity lx_fncapprox_dsp48 is
	port (
		P         : out std_logic_vector(47 downto 0);
		A         : in  std_logic_vector(17 downto 0) := (others => '0');
		B         : in  std_logic_vector(17 downto 0) := (others => '0');
		C         : in  std_logic_vector(47 downto 0) := (others => '0');
		CLK       : in  std_logic;
		CE        : in  std_logic
	);
end lx_fncapprox_dsp48;

architecture Behavioral of lx_fncapprox_dsp48 is

begin

update:
	process
	begin
		wait until CLK'event and CLK= '1' and CE = '1';
		P <= std_logic_vector(signed(C) + signed(A) * signed(B));
	end process;
end Behavioral;

