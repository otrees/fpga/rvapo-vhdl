This repository contains only one integration on Zynq FPGA for demos and development, and no guarantees are therefore made as to its contents. Go to [rvapo-apps](https://gitlab.fel.cvut.cz/otrees/fpga/rvapo-apps) for stable projects with a defined purpose.

# Generating Vivado project
The average Vivado project is too massive to be put in git as-is. Instead it is are generated from a script.
Run `make` with vivado available in command line to call it.

The command to make Vivado available is usually
`source /<Vivado install dir>/Xilinx/Vivado/2018.2/settings.sh`

The complete build is possible by simple
```make```
invocation.

The Vivado project can be recreated by
```make project ```
or
```vivado -mode batch -source ./recreate.tcl```
to allow modifications before build.

After that, run Vivado and click `Generate block design` and then `Generate bitstream`.

Now a bitstream should be ready; `/scripts` describes what to do with it.

# ARM address mapping
This is the default mapping of system memories visible to ARM, assumed by all scripts in this project.
All four memories have 8192B.
| Address      | Purpose     | 
|--------------|-----------|
| 0x40000000 | Instruction memory | 
| 0x42000000      | Data memory |
| 0x44000000      | Debug memory |
| 0x46000000      | Memory over AXI |
| 0x43c00000      | Control registers |

# AXI timing
Transaction per seconds on accessing the PMSM peripheral:
Direct access, 1 AXI interconnect with 1 slave        2.6M
Access through Zynq ACP, 2 AXI interconnects	     2M
Zynq bypass, 2 interconnects            	         1.6M

Transaction per seconds on accessing ARM RAM through ACP AXI:
2M

# Generating script to generate Vivado project.
In case you change the project, you will need to regenerate the files required for project generation. It is not simple, so do not do it unless you have to.

In theory, it should be enough to run 
`write_project_tcl vtp_recreate_git`
in the TCL console, but it is usually not necessary.

In practice, the following problems arise:
- The tcl script requires some files from the project, specifically the constraints file `microzed_apo-rev1.xdc` and wrappers `top.vhd`,`top_wrapper.vhd`. These files are normally located somewhere in project folder, and the project-generating script will expect to find the there. At the same time, the script will expect that the project folder does not exist, and it will fail if it does, with the advice to use `-force` option. This option will delete the project folder, and then the script will fail trying to find the files it just deleted. 
- Vivado by default generates the files in the folder it was launched from. To avoid issues, it is recommended to launch it from `integration/vivado`.
- Sometimes, the script contains non-existent paths. 

Due to this, i am unable to offer a clean-cut solution. Instead, you have to manually copy the script to project root and ensure correctness of paths:

- `origin_dir` shall be `.` All paths in the script are relative to this.
- second parameter of `create_project` should evaluate to `./vtp`. This is where the project will be generated.
- second parameter of `set_property "ip_repo_paths"` shall be 
`"[file normalize "$origin_dir/../../rvapo/core"] [file normalize "$origin_dir/ip"]"`
The user IP repositories are there. If you add an IP of your own, just put it in the `$origin_dir/ip` folder.
- The files `microzed_apo-rev1.xdc`, `top.vhd` should be mentioned twice in the script (plus once in a comment). Ensure they point to up-to-date versions of these files outside the folder where project will be generated.
- Unlike all other paths, the second mention of `top_wrapper.vhd` will not contain `$origin_dir`. This is correct, the path will not be resolved correctly with it.

When block design is changed, the top.tcl update/editing in Vivado should be finished by double-click to the block design  in the project sources window and next menu items has to be selected
```File -> Export -> Export Block Design...```

# Vivado issues
If it throws top_hdl.hwdef not found and bitstream generation failed, ignore the message.

When HDL code is imported to Vivado project, it physicall copies all HDL files inside the project. I do not know how to make it copy non-HDL files such as memory initialization, so the path to them must be absolute to make it work.

If it crashes frequently on Linux, try X11 instead of Wayland.

If Create and Package new IP always fails with `ipx:infer_core` failed due to previous errors, try packaging the same IP in different project. If that helps, it means your project is corrupted and you may have to regenerate it.

If timing analysis fails, but all failing paths are around multiplier, it is probably fine. Zynq has 18x18bit multiplication macros, and Vivado can infer 32x32bit multiplication in one cycle on these, but apparently can not calculate timing for this.

If you add new external pin and it does not work, check the warnings (vanilla ones, not critical). If the pin in block design is not connected to hardware pin, Vivado usually sends messages there.

If the status bar in top right corner says that implementation/synthesis is in progress but you did not launch it, ignore the message, as it is probably a lie.

If you edited an IP before opening the project that uses it, Vivado will not recognize the change. Only changes made while project is opened are sure to be noticed.

If you changed interface of IP, you must repackage it, even if IP update is successful. IP update can not handle interface changes.

Crashes while generating block design are normal. Just restart.

When using automatic connection of AXI, check all clocks. Sometimes Vivado confuses the clock domains, since AXI masters usually have their own clock pin, and then throws errors even if there is only one actual clock in the design.
Also, ext_reset_in pins on Processor System Reset should be connected: FCLK_RESET0_N is a safe choice.

The clock frequency setting in Zynq7 processing system customization in Vivado block design does not affect the hardware.
The default frequency of 100MHz is fine for this project, but if you want different clock speed on the FPGA, refer to 
https://docs.xilinx.com/r/en-US/ug585-zynq-7000-SoC-TRM/Register-TOPSW_CLK_CTRL-Details

For example, this sets CLK0 to 25MHz and CLK1 to 125MHz. (Base clock is 1GHz and there are two cascaded divisor)
./rdwrmemf -b 4 -s 0xF8000170 -F 0x00400A00   
./rdwrmemf -b 4 -s 0xF8000180 -F 0x00200400  