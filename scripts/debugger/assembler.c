#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>
#include "assembler.h"
#include "main.h"
#define PUT(a,at)          (((uint32_t)a)<<at)
uint32_t make_lui(uint8_t regsel,int32_t immediate){
    if(regsel>REGMASK)cursable_print("Regsel %d exceeds limit (%d) in lui\n",regsel,REGMASK+1);
    regsel&=REGMASK;
    if(immediate>LUI_MASK||immediate<-LUI_MASK)cursable_print("LUI immediate %d exceeds limit (%d,%d)\n",immediate,LUI_MASK,-LUI_MASK);
    immediate&=LUI_MASK;
    return make_instruction_U(OP_LUI,regsel,immediate);
    }
uint32_t make_pcread(){
    return make_instruction_U(OP_AUIPC,0,0);
    }
uint32_t make_regread(uint8_t regsel){//add zero,zero,regsel
    if(regsel>REGMASK)cursable_print("Regsel %d exceeds limit (%d) in regread\n",regsel,REGMASK+1);
    regsel&=REGMASK;
    return make_instruction_R(OP_ALU_R,0,F3_ADD,0,regsel,F7_ALU_R);
    }
uint32_t make_memread(uint8_t addrreg,uint8_t datareg,int16_t offset){
    if(addrreg>REGMASK)cursable_print("Addrreg %d exceeds limit (%d) in memwrite\n",addrreg,REGMASK+1);
    addrreg&=REGMASK;
    if(datareg>REGMASK)cursable_print("Datareg %d exceeds limit (%d) in memwrite\n",datareg,REGMASK+1);
    datareg&=REGMASK;
    if(offset>I_IMM_MASK||offset<-I_IMM_MASK)cursable_print("Offset %d exceeds limit (%d,%d) in memread\n",offset,-I_IMM_MASK,I_IMM_MASK);
    offset&=I_IMM_MASK;
    return make_instruction_I(OP_MEM_L,datareg,F3_WORD,addrreg,offset);
    }    
uint32_t make_memwrite(uint8_t addrreg,uint8_t datareg,int16_t offset){
    if(addrreg>REGMASK)cursable_print("Addrreg %d exceeds limit (%d) in memwrite\n",addrreg,REGMASK+1);
    addrreg&=REGMASK;
    if(datareg>REGMASK)cursable_print("Datareg %d exceeds limit (%d) in memwrite\n",datareg,REGMASK+1);
    datareg&=REGMASK;
    if(offset>I_IMM_MASK||offset<-I_IMM_MASK)cursable_print("Offset %d exceeds limit (%d,%d) in memwrite\n",offset,-I_IMM_MASK,I_IMM_MASK);
    offset&=I_IMM_MASK;
    return make_instruction_S(OP_MEM_S,F3_WORD,addrreg,datareg,offset);
    }    
uint32_t make_alu_immediate(uint8_t target,uint8_t source,int16_t immediate,uint8_t funct){
    if(source>REGMASK)cursable_print("Source regsel %d exceeds limit (%d) in alu immediate\n",source,REGMASK+1);
    source&=REGMASK;
    if(target>REGMASK)cursable_print("Target regsel %d exceeds limit (%d) in alu immediate\n",target,REGMASK+1);
    target&=REGMASK;

    if(funct==F3_SRA)immediate|=0x400;
    funct&=F3_MASK;

    return make_instruction_I(OP_ALU_I,target,funct,source,immediate);
    }    
uint32_t make_alu_reg(uint8_t target,uint8_t source1,uint8_t source2,uint8_t funct){
    if(source1>REGMASK)cursable_print("Source1 regsel %d exceeds limit (%d) in alu reg\n",source1,REGMASK+1);
    source1&=REGMASK;
     if(source2>REGMASK)cursable_print("Source2 regsel %d exceeds limit (%d) in alu reg\n",source2,REGMASK+1);
    source2&=REGMASK;
    if(target>REGMASK)cursable_print("Target regsel %d exceeds limit (%d) in alu reg\n",target,REGMASK+1);
    target&=REGMASK;

    uint8_t funct7=F7_ALU_R;
    if(funct==F3_SRA)funct7=F7_ALU_R2;
    funct&=F3_MASK;

    return make_instruction_R(OP_ALU_R,target,funct,source1,source2,funct7);
    }       
uint32_t make_instruction_R(uint8_t opcode,uint8_t rd,uint8_t funct3,uint8_t rs1,uint8_t rs2,uint8_t funct7){
    return PUT(opcode,0)|PUT(rd,7)|PUT(funct3,12)|PUT(rs1,15)|PUT(rs2,20)|PUT(funct7,25);
    }
uint32_t make_instruction_I(uint8_t opcode,uint8_t rd ,uint8_t funct3,uint8_t rs1 ,uint16_t imm){
    return PUT(opcode,0)|PUT(rd,7)|PUT(funct3,12)|PUT(rs1,15)|PUT(imm,20);
    }
uint32_t make_instruction_S(uint8_t opcode,uint8_t funct3,uint8_t rs1 ,uint8_t rs2 ,uint16_t imm){
    return PUT(opcode,0)|PUT(imm&0x1F,7)|PUT(funct3,12)|PUT(rs1,15)|PUT(rs2,20)|PUT((imm>>5),25);
    }
uint32_t make_instruction_B(uint8_t opcode,uint8_t funct3,uint8_t rs1 ,uint8_t rs2 ,uint16_t imm){
    return PUT(opcode,0)|PUT((imm>>10)&1,7)|PUT(imm&0xF,8)|PUT(funct3,12)|PUT(rs1,15)|PUT(rs2,20)|PUT((imm>>4)&0x3F,25)|PUT((imm>>11)&1,31);
    }
uint32_t make_instruction_U(uint8_t opcode,uint8_t rd ,uint32_t imm ){
    return PUT(opcode,0)|PUT(rd,7)|PUT(imm,12);
    }
uint32_t make_instruction_J(uint8_t opcode,uint8_t rd ,uint32_t imm){
    return PUT(opcode,0)|PUT(rd,7)|PUT((imm>>11)&0xFF,12)|PUT((imm>>10)&1,12)|PUT(imm&0x3FF,12)|PUT((imm>>19)&1,12);
    }