#include <stdint.h>
#define OP_ALU_R    0x33
#define OP_ALU_I    0x13
#define OP_MEM_L    0x03
#define OP_MEM_S    0x23
#define OP_AUIPC    0x17 
#define OP_LUI      0x37 

#define F3_ADD      0
#define F3_SLL      1
#define F3_SLT      2
#define F3_SLTU     3
#define F3_XOR      4
#define F3_SRL      5
#define F3_OR       6
#define F3_AND      7

#define F3_SRA      0x15
#define F3_SUB      0x10

#define F3_BYTE     0
#define F3_HALF     1
#define F3_WORD     2
#define F7_ALU_R    0
#define F7_ALU_R2   8
#define REGMASK     0x1F
#define I_IMM_MASK  0xFFF
#define LUI_MASK    0xFFFFF
#define F3_MASK     7
#define REG_SP      2
#define REG_T6      31
#define REG_T5      30
uint32_t make_instruction_R(uint8_t opcode,uint8_t rd,uint8_t funct3,uint8_t rs1,uint8_t rs2,uint8_t funct7);
uint32_t make_instruction_I(uint8_t opcode,uint8_t rd ,uint8_t funct3,uint8_t rs1 ,uint16_t imm);
uint32_t make_instruction_S(uint8_t opcode,uint8_t funct3,uint8_t rs1 ,uint8_t rs2 ,uint16_t imm);
uint32_t make_instruction_B(uint8_t opcode,uint8_t funct3,uint8_t rs1 ,uint8_t rs2 ,uint16_t imm);
uint32_t make_instruction_U(uint8_t opcode,uint8_t rd ,uint32_t imm);
uint32_t make_instruction_J(uint8_t opcode,uint8_t rd ,uint32_t imm);
uint32_t make_regread(uint8_t regsel);
uint32_t make_memread(uint8_t addrreg,uint8_t datareg,int16_t offset);
uint32_t make_memwrite(uint8_t addrreg,uint8_t datareg,int16_t offset);
uint32_t make_pcread();
uint32_t make_lui(uint8_t regsel,int32_t immediate);
uint32_t make_alu_reg(uint8_t target,uint8_t source1,uint8_t source2,uint8_t funct);
uint32_t make_alu_immediate(uint8_t target,uint8_t source,int16_t immediate,uint8_t funct);