#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <locale.h>
#include <stdarg.h>
#include <curses.h>
#include <ctype.h>
#include <time.h>
#include <errno.h> 
#include "mzapo_phys.h"
#include "assembler.h"
//#define MOCK                //This is used for development on systems where rvapo is not available
#define RDWRMEMF            "./rdwrmemf -b 4 -s 0x%x -l %d -F %s"
#define CLEANFILE           "empty.txt"
#define IMEM_SETTER         "-imem"
#define DMEM_SETTER         "-dmem"
#define CTRL_SETTER         "-ctrl"
#define IMEM_PHYS_DEFAULT   0x40000000
#define DMEM_PHYS_DEFAULT   0x42000000
#define CTRL_PHYS_DEFAULT   0x43C00000
#define EXECTRL_OFFSET      0
#define RESET_BIT           0
#define PAUSE_BIT           1
#define ISSUE_BIT           2
#define STEP_BIT            3
#define RESULT_OFFSET       0x4
#define INSTR_OFFSET        0x5
#define STATUS_OFFSET       0x2
#define BRAMSIZE            0x2000
#define CTRLSIZE            0x40
#define FAIL(a,b)           do{printf(a);return b;}while(0)
#define MAXCMDLEN           100
#define V_PTR(a)            *((volatile uint32_t*)(a)) 
#define setbit(reg, bit)    ((V_PTR(reg)) |= (1U << (bit)))
#define clearbit(reg, bit)  ((V_PTR(reg)) &= (~(1U << (bit))))
#define togglebit(reg, bit) ((V_PTR(reg)) ^= (1U << (bit)))
#define getbit(reg, bit)    (((V_PTR(reg)) & (1U << bit)) >> bit)
#define CURSE_MINLINES      33
#define CURSE_MINCOLS       130
#define LPANEW              20
#define HISTORYLEN          16
#define max(a,b)            ((a)>(b)?(a):(b))
#define min(a,b)            ((a)<(b)?(a):(b))
#ifdef MOCK
#define run                 nop
#define reset               nop
#define unpause             nop
#define pause               nop
#define step                nop
#define issue               nop
#define SW(addr)		    trash
#define LW(addr)		    0xABCDDCBA
uint32_t trash;
#else
#define run                 clearbit(ctrl_virt+EXECTRL_OFFSET,RESET_BIT)
#define reset               setbit(ctrl_virt+EXECTRL_OFFSET,RESET_BIT)
#define unpause             clearbit(ctrl_virt+EXECTRL_OFFSET,PAUSE_BIT)
#define pause               setbit(ctrl_virt+EXECTRL_OFFSET,PAUSE_BIT)
#define step                togglebit(ctrl_virt+EXECTRL_OFFSET,STEP_BIT)
#define issue               togglebit(ctrl_virt+EXECTRL_OFFSET,ISSUE_BIT)
#define SW(addr)		    (*(volatile uint32_t*)(addr))	
#define LW(addr)		    *((volatile uint32_t*)(addr))
#endif
#define gettrap             ((LW(ctrl_virt+STATUS_OFFSET)>>26)&3)
#define nop                 for(volatile int not_i=0;not_i<50;not_i++)
#define lastmsg             msglist[msgmax-1]
#define resetcursor         move(LINES-1,cursorx+LPANEW)
int cursed=0;
int verbose=0;
volatile uint32_t*ctrl_virt=NULL;
volatile uint32_t*imem_virt=NULL;
volatile uint32_t*dmem_virt=NULL;
char**msglist;
char**history;
int msgmax;
int hi;
int cursorx=LPANEW;
int attached=1;
const char*REGNAMES[]={
    "zero","ra","sp","gp",
    "tp","t0","t1","t2",
    "s0","s1","a0","a1",
    "a2","a3","a4","a5",
    "a6","a7","s2","s3",
    "s4","s5","s6","s7",
    "s8","s9","s10","s11",
    "t3","t4","t5","t6"
    };
const char*TRAPS[]={
    "Normal",
    "Break",
    "Call",
    "Fault"
    };
const char*HELP[]={
    "Accepted commands:\n",
    "h                  Print this help.\n",
    "q                  Quit.\n",
    "c                  Pause/unpause rvapo.\n",
    "n <count>          Execute count instructions of program.\n",
    "e <4B hex>         Execute given instruction.\n",
    "g                  Switch between terminal and ncurses mode.\n",
    "d                  List content of all registers.\n",
    "m <reg> <offset>   Load word from offset+reg.\n",
    "r <reg>            Read given register.\n",
    "p                  Read program counter and current instruction.\n",
    "s                  Read rvapo status.\n",
    "v                  Verbose mode - commands will print executed instructions.\n",
    "= <reg> <4B hex>   Write hex value to register.\n",
    "w <reg> <offset> <value> Write value at offset+reg.\n"
};
#define n_array(a)      (sizeof (a) / sizeof (const char *))
#define BUF_SIZE 65536
   
int msleep(long msec){
    struct timespec ts;
    int res;
    if(msec<0){
        errno = EINVAL;
        return -1;
        }
    ts.tv_sec = msec / 1000;
    ts.tv_nsec = (msec % 1000) * 1000000;
    do{res=nanosleep(&ts, &ts);}while(res&&errno==EINTR);
    return res;
    }
int count_lines(FILE* file){
    char buf[BUF_SIZE];
    int counter = 0;
    for(;;){
        size_t res = fread(buf, 1, BUF_SIZE, file);
        if(ferror(file))return -1;

        int i;
        for(i=0;i<res;i++)if(buf[i]=='\n')counter++;

        if(feof(file))break;
        }
    return counter;
    }
void msgshift(char**list){
    free(list[0]);
    for(int i=1;i<msgmax;i++)list[i-1]=list[i];
    }
uint32_t parse(const char*instr){
    uint32_t ret=0;
    if(sscanf(instr," %x ",&ret)!=1)if(sscanf(instr," 0x%x ",&ret)!=1)printf("No instruction can be parsed from \"%s\" (hex uint32 expected).\n",instr);
    return ret;
    }
void cursable_print(const char*fmt,...){
    va_list args;
    va_start(args,fmt);
    if(cursed){
        msgshift(msglist);
        lastmsg=(char*)malloc(MAXCMDLEN);
        vsnprintf(lastmsg,MAXCMDLEN,fmt,args);
        }
    else vprintf(fmt,args);
    va_end(args);
    }       
uint32_t execute(uint32_t instr){
    if(getbit(ctrl_virt+EXECTRL_OFFSET,PAUSE_BIT)!=1)return 0;
    if(verbose)cursable_print("Executing 0x%08x\n",instr);
#ifdef MOCK    
    return 10;
#else        
    SW(ctrl_virt+INSTR_OFFSET)=instr;
    issue;
    nop;
    issue;
    return LW(ctrl_virt+RESULT_OFFSET);
#endif     
    }
uint32_t readreg(int regsel){
    execute(make_regread(regsel));   
    }
void save(char*cmd){
    msgshift(history);
    history[HISTORYLEN-1]=malloc(MAXCMDLEN+5);
    strcpy(history[HISTORYLEN-1],cmd);
    }    
void cmd_execute(uint32_t instr){
    uint32_t res=execute(instr);
    cursable_print("Result of 0x%08x is 0x%08x.\n",instr,res);
    }
void cmd_next(char*spec){
    unsigned int steps=1;
    sscanf(spec," %u ",&steps);
    for(;steps>0;steps--){
        step;
        step;
        }
    }    
void cmd_pc(){
    uint32_t pc=execute(make_pcread());
    cursable_print("PC is 0x%08x, instr 0x%x\n",pc,LW(imem_virt+pc/4));
    }    
void cmd_continue(){
    togglebit(ctrl_virt+EXECTRL_OFFSET,PAUSE_BIT);
    cursable_print("toggled pause, current: %d\n",getbit(ctrl_virt+EXECTRL_OFFSET,PAUSE_BIT));
    }    
void setreg(int reg,uint32_t value){
    int16_t immediate=value&I_IMM_MASK;
    uint32_t ui=(value>>12)&LUI_MASK;
    if((immediate&0x800)!=0){
        immediate-=0x1000;    
        execute(make_lui(reg,(ui+0x1)&LUI_MASK));
        }
    else execute(make_lui(reg,ui));    
    execute(make_alu_immediate(reg,reg,immediate,F3_ADD));
    }
uint32_t readmem(uint32_t address){
    execute(make_memwrite(REG_SP,REG_T6,-4));
    setreg(REG_T6,address);
    uint32_t ret=execute(make_memread(REG_T6,0,0));
    execute(make_memread(REG_SP,REG_T6,-4));
    execute(make_memwrite(REG_SP,0,-4));
    return ret;
    }
void cmd_writereg(const char*spec){
    int regnum;
    uint32_t value;
    if(sscanf(spec,"%d %x",&regnum,&value)!=2)return;

    setreg(regnum,value);
    }
void cmd_readmem(const char*spec){
    int regnum=-1,offset;
    if(sscanf(spec," %d %d ",&regnum,&offset)!=2){
        uint32_t address;            
        if(sscanf(spec," %x ",&address)!=1)return;    
        cursable_print("Memory 0x%08x=0x%08x\n",address,readmem(address));
        return;
        }
    cursable_print("Memory 0x%08x=0x%08x\n",readreg(regnum)+offset,execute(make_memread(regnum,0,offset)));
    }

void cmd_writemem(const char*spec){
    int regnum=-1,offset;
    uint32_t value;
    if(sscanf(spec," %d %d %x",&regnum,&offset,&value)!=3){
        uint32_t address;
        if(sscanf(spec," %x %x",&address,&value)!=2)return;
        execute(make_memwrite(REG_SP,REG_T5,-4));
        execute(make_memwrite(REG_SP,REG_T6,-8));
        setreg(REG_T5,address);
        setreg(REG_T6,value);
        execute(make_memwrite(REG_T5,REG_T6,0));
        execute(make_memread(REG_SP,REG_T5,-4));
        execute(make_memread(REG_SP,REG_T6,-8));
        execute(make_memwrite(REG_SP,0,-4));
        execute(make_memwrite(REG_SP,0,-8));    

        cursable_print("Memory 0x%08x=0x%08x\n",address,readmem(address));    
        return;
        }
    int tmpreg=(regnum==REG_T6)?REG_T5:REG_T6;

    execute(make_memwrite(REG_SP,tmpreg,-4));
    setreg(tmpreg,value);
    execute(make_memwrite(regnum,tmpreg,offset));
    execute(make_memread(REG_SP,tmpreg,-4));
    execute(make_memwrite(REG_SP,0,-4));

    cursable_print("Memory 0x%08x=0x%08x\n",readreg(regnum)+offset,execute(make_memread(regnum,0,offset)));
    }
void cmd_readreg(const char*spec){
    int regnum=-1;
    if(sscanf(spec," %d ",&regnum)!=1)return;
    cursable_print("register %02d is 0x%08x\n",regnum,readreg(regnum));
    }     
void cmd_delay(const char*spec){
    int ms;
    if(sscanf(spec,"%d",&ms)!=1)printf("Could not parse miliseconds.");
    else{
        unpause;
        msleep(ms); 
        pause;   
        }
    }    
void curse(){
    int verbose_local=verbose;
    verbose=0;
    uint32_t pc=execute(make_pcread());
    mvprintw(0,0,"Registers:\t\tPC:0x%08x\tInstr:0x%08x\tStatus:%s\tControl:0x%08x",pc,LW(imem_virt+pc/4),TRAPS[gettrap],LW(ctrl_virt+EXECTRL_OFFSET));
    for(int row=0;row<32;row++)mvprintw(row+1,0,"%-4s:0x%08x",REGNAMES[row],readreg(row));
    for(int i=0;i<msgmax;i++)mvaddstr(LINES-msgmax-1+i,20,msglist[i]);
    verbose=verbose_local;

    move(LINES-1,cursorx+LPANEW);
    clrtoeol();
    refresh();
    }    
void cmd_curse(){
    if(cursed)endwin(); 
    else{
        keypad(stdscr,TRUE);
        noecho();
        }  
    cursed=1-cursed;
    }  
uint32_t*parsemap(char*addrstring,char*sizestring){
    uint32_t addr;
    if(sscanf(addrstring,"0x%x",&addr)!=1&&sscanf(addrstring,"%d",&addr)!=1){
        printf("Can not parse address from \"%s\".",addrstring);
        return NULL;
        }
    uint32_t size;    
    if(sscanf(sizestring,"0x%x",&size)!=1&&sscanf(sizestring,"%d",&size)!=1){
        printf("Can not parse size from \"%s\".",sizestring);
        return NULL;
        }    
#ifdef MOCK
    return 1;//a fake address so that the mock execution can continue
#else
    return map_phys_address(addr,size,0);  
#endif      
    }   
int init_program(int argc,char**argv){
    attached=0;
    if(access(argv[1],R_OK)!=0)FAIL("Mem init file can not be read\n",100); 
    if(access(CLEANFILE,R_OK)!=0){
        FILE*clean=fopen(CLEANFILE,"w");
        for(int i=0;i<200;i++)fputs("0x00000000;\n",clean);
        fclose(clean);
        }
    for(int i=2;i<argc;i++){//this does not skip arguments of commands and attempts to interpret them as commands because it would ruin the oneliners.
        if(i+2>=argc){
            printf("Not enough tokens follow %s to attempt interpreting as custom address command\n",argv[i]);
            continue;
            }    
        if(strcmp(argv[i],IMEM_SETTER)==0&&(imem_virt=parsemap(argv[i+1],argv[i+2]))==NULL)FAIL("Could not map custom imem to virtual memory.\n",104);
        if(strcmp(argv[i],DMEM_SETTER)==0&&(dmem_virt=parsemap(argv[i+1],argv[i+2]))==NULL)FAIL("Could not map custom dmem to virtual memory.\n",104);
        if(strcmp(argv[i],CTRL_SETTER)==0&&(ctrl_virt=parsemap(argv[i+1],argv[i+2]))==NULL)FAIL("Could not map custom ctrl to virtual memory.\n",104);
        }    
#ifndef MOCK            
    if(ctrl_virt==NULL&&(ctrl_virt=map_phys_address(CTRL_PHYS_DEFAULT,CTRLSIZE,0))==NULL)FAIL("Could not map rvapo_ctrl to virtual memory.\n",102);
    if(imem_virt==NULL&&(imem_virt=map_phys_address(IMEM_PHYS_DEFAULT,BRAMSIZE,0))==NULL)FAIL("Could not map imem to virtual memory.\n",102);
    if(dmem_virt==NULL&&(dmem_virt=map_phys_address(DMEM_PHYS_DEFAULT,BRAMSIZE,0))==NULL)FAIL("Could not map dmem to virtual memory.\n",102);

    FILE*prog=fopen(argv[1],"r");
    if(prog==NULL)FAIL("Could not open program file.\n",102);
    int bytes=count_lines(prog)*4;
    fclose(prog);
    if(bytes<0)FAIL("Could not count length of program.\n",103);

    reset;
    char cmd[400];
    sprintf(cmd,RDWRMEMF,IMEM_PHYS_DEFAULT,bytes,argv[1]);
    puts(cmd);
    if(system(cmd)==-1)FAIL("Could not initialize imem.\n",101);

    sprintf(cmd,RDWRMEMF,DMEM_PHYS_DEFAULT,BRAMSIZE,CLEANFILE);
    puts(cmd);
    if(system(cmd)==-1)FAIL("Could not clean dmem.\n",101);

    sprintf(cmd,RDWRMEMF,DMEM_PHYS_DEFAULT,bytes,argv[1]);
    puts(cmd);
    if(system(cmd)==-1)FAIL("Could not initialize dmem.\n",101);
#else
    printf("You are running a mock version of debugger, used for development of itself. Undefine MOCK in main.c if you want to debug with it.\n");  
    fflush(NULL);//flush streams before initialising curses  
#endif
    return 1;
    }    
int main(int argc, char *argv[]){
    if(argc<2)FAIL("Mem init file or -a required as first argument.\n",100);
    if(strcmp(argv[1],"-a")!=0){
        int ret=init_program(argc,argv);
        if(ret!=1)return ret;
        }
    setlocale(LC_ALL, "");
    initscr();
    endwin();
    int cursable=1;
    if(COLS<CURSE_MINCOLS||LINES<CURSE_MINLINES){
        cursable=0;
        printf("Ncurses mode requires %d,%d lines,columns but has %d,%d. Curses mode disabled.\n",CURSE_MINLINES,CURSE_MINCOLS,LINES,COLS);    
        }   
    
    msgmax=LINES-3;
    msglist=(char**)malloc(sizeof(char**)*msgmax);
    for(int i=0;i<msgmax;i++)msglist[i]=(char*)calloc(1,1);
    history=(char**)malloc(sizeof(char**)*msgmax);
    for(int i=0;i<msgmax;i++)history[i]=(char*)calloc(1,1);

    if(attached==0)reset;
    pause;
    run;

    int fault=0;
    char cmdbuf[MAXCMDLEN+5];
    char lastcmd[MAXCMDLEN+5];

    while(true){
        int read;
        char*rest;
        if(cursed){
            cbreak();
            while((read=getch())!=EOF){
                if(read==KEY_LEFT)cursorx=max(cursorx-1,0);
                else if(read==KEY_RIGHT&&cmdbuf[cursorx]!=0)cursorx=min(cursorx+1,MAXCMDLEN-1);
                else if(read==KEY_UP){
                    hi=(hi-1)&(HISTORYLEN-1);
                    cursorx=0;
                    resetcursor;
                    clrtoeol();
                    strcpy(cmdbuf,history[hi]);
                    for(int i=strlen(history[hi])-1;i>=0;i--)ungetch(history[hi][i]);                    
                    }
                else if(read==KEY_BACKSPACE&&cmdbuf[cursorx]==0){
                    cursorx=max(cursorx-1,0);
                    move(LINES-1,cursorx+LPANEW);
                    addch(' ');
                    cmdbuf[cursorx]=0;    
                    }
                else if(read=='\n')break;
                else if(isprint(read)){
                    cmdbuf[cursorx]=read;    
                    cursorx=min(cursorx+1,MAXCMDLEN-1);
                    addch(read);
                    }
                resetcursor;
                refresh();    
                }
            }
        else{         
            if(fgets(cmdbuf,MAXCMDLEN,stdin)==NULL)break;            
            }
        int mov;
        read=-1;
        char boilerplate;
        if(sscanf(cmdbuf," %c%n",&boilerplate,&mov)!=1)continue;                
        rest=cmdbuf+mov;    
        read=boilerplate;
        if(cmdbuf[2]!=0)save(cmdbuf);
        if(read=='q')break;
        else if(read=='c')cmd_continue();
        else if(read=='n')cmd_next(rest);
        else if(read=='e')cmd_execute(parse(rest));
        else if(read=='g'&&cursable)cmd_curse();
        else if(read=='h')for(int i=0;i<n_array(HELP);i++)cursable_print(HELP[i]);
        else if(read=='d')for(int i=0;i<32;i++)cursable_print("register %02d is 0x%08x\n",i,readreg(i));
        else if(read=='r')cmd_readreg(rest);
        else if(read=='s')cursable_print("Status:%s, control:0x%08x\n",TRAPS[(LW(ctrl_virt+STATUS_OFFSET)>>26)&3],LW(ctrl_virt+EXECTRL_OFFSET));
        else if(read=='p')cmd_pc();
        else if(read=='m')cmd_readmem(rest);
        else if(read=='w')cmd_writemem(rest);
        else if(read=='v')verbose=1-verbose;
        else if(read=='=')cmd_writereg(rest);
        else if(read=='t')cmd_delay(rest);

        cursorx=0;
        hi=0;
        if(cursed)curse();
        for(int i=0;i<MAXCMDLEN;i++)cmdbuf[i]=0;
        }
    if(cursed)endwin();    
    for(int i=0;i<msgmax;i++)free(msglist[i]);
    unpause;
    reset;
    }