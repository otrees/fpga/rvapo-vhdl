#!/bin/bash

if [ "$1" == "-instr" ]; then
	./rdwrmemf -b 4 -s 0x40000000 -l $2 -m
elif [ "$1" == "-data" ]; then	
	./rdwrmemf -b 4 -s 0x42000000 -l $2 -m
elif [ "$1" == "-ctrl" ]; then
	./rdwrmemf -b 4 -s 0x43c00000 -l $2 -m
elif [ "$1" == "-46" ]; then
	./rdwrmemf -b 4 -s 0x46000000 -l $2 -m
elif [ "$1" == "-debug" ]; then 	
	./rdwrmemf -b 4 -s 0x44000000 -l $2 -m
else 
	 ./rdwrmemf -b 4 -s $1 -l $2 -m
fi
