#!/bin/bash
axibase=$1;
datalen=8;

runtest(){
    echo "Test $1"
    ./rdwrmemf -b 4 -s 0x40000000 -l 2000 -F "./rdwrmem/$1.txt"
    ./rdwrmemf -b 4 -s 0x42000000 -l 2000 -F empty.txt
    ./rdwrmemf -b 4 -s 0x42000000 -l "$datalen" -F "./rdwrmem/$1.txt"
    ./rdwrmemf -b 4 -s 0x420001FC -l 4 -F "$axibase"
    ./rdwrmemf -b 4 -s 0x44000000 -l 2000 -F full.txt
    ./rdwrmemf -b 4 -s 0x43c00000 -l 4 -F 0x00000000
    ./rdwrmemf -b 4 -s "$axibase" -l 1000 empty.txt 	
    sleep 1;
    ./rdwrmemf -b 4 -s 0x43c00000 -l 4 -F 0x00000001
    rdwrmem -b 4 -s 0x42000000 -l 1000 -m > rvapo_mem_dump.txt
    ./comparer.out "testresults/$1.txt" 
    rm comparer_tmp.txt
    }

gcc comparer.c -o comparer.out
runtest mem_unalign_large
runtest exe_stalls
runtest jump_hazards
runtest im_stalls
runtest mem_unalign_stalls
runtest mem_unalign_axi
runtest clz
runtest M
runtest fnc
runtest axi_basic
echo "Following is sorting test. Autocheck not implemented, just look if the numbers are sorted.\n";
datalen=50;
runtest qsort

