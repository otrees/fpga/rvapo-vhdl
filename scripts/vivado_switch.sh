#!/bin/bash

BSRC=$(dirname $BASH_SOURCE)

files=(
    '../rvapo/wrapper_spi' '../rvapo/wrapper_spimem' '../rvapo/wrapper_aximem'
#	'../rvapo/wrapper_axi'
	'../rvapo/wrapper_wb'	
   	'../rvapo/tb/rvapo_tb_pc'
	'../rvapo/tb/rvapo_tb_wishbone'
	'../rvapo/tb/rvapo_tb_fetch'
	'../rvapo/tb/rvapo_tb_dec'
	'../rvapo/tb/rvapo_tb_exec'
	'../rvapo/tb/rvapo_tb_mem'
	'../rvapo/tb/rvapo_tb_wb'
	'../rvapo/tb/rvapo_tb_bu'
	'../rvapo/tb/rvapo_tb_alu'
	'../rvapo/tb/rvapo_tb_hu'
	'../rvapo/tb/rvapo_tb_devel'
	'../rvapo/tb/rvapo_tb_full' 
    '../rvapo/tb/rvapo_tb_axi' 
    '../rvapo/core/rvapo_intcon_ice40' 
    '../rvapo/target_tb/rvapo_ice40_mem_impl'
    '../rvapo/target_tb/rvapo_cm_target_ice40'  
	'../rvapo/target_tb/rvapo_cm_target'  
	'../rvapo/target_tb/rvapo_cm_target_fileloader'  
    '../rvapo/spi/spi_master'  
	'../rvapo/wb/wb_master'  
	'../rvapo/tb/wb_slave/wb_pkg'
	'../rvapo/tb/wb_slave/wbs_pkg'
	'../rvapo/tb/wb_slave/wbs_regs_08'  
	'../rvapo/tb/rvapo_ctrl/rvapo_ctrl_v1_0_S00_AXI'
	'../rvapo/tb/rvapo_ctrl/rvapo_ctrl_v1_0'
        )

case "$1" in
	y|yes)
        for file in "${files[@]}"; do
            mv "$BSRC/$file.vhd" "$BSRC/$file.notvhd";
        done
	;;

	n|no)
         for file in "${files[@]}"; do
            mv "$BSRC/$file.notvhd" "$BSRC/$file.vhd";
        done
	;;

	*) 
		echo "Unknown command $1. Use y/yes to disable non-vivado vhdl files or n/no to enable";
    ;; 
esac