#define _XOPEN_SOURCE 700
#include <fcntl.h> /* open */
#include <stdint.h> /* uint64_t  */
#include <stdio.h> /* printf */
#include <stdlib.h> /* size_t */
#include <unistd.h> /* pread, sysconf */
#define WIDTH       640
#define HEIGHT      480
#define PXBYTES     2
#define IO  0x12345678
#define R   0xF800
#define G   0x07E0
#define B   0x001F
#define W   0xFFFF
#define Z   0x0000

typedef struct {
    uint64_t pfn : 55;
    unsigned int soft_dirty : 1;
    unsigned int file_page : 1;
    unsigned int swapped : 1;
    unsigned int present : 1;
} PagemapEntry;

/* Parse the pagemap entry for the given virtual address.
 *
 * @param[out] entry      the parsed entry
 * @param[in]  pagemap_fd file descriptor to an open /proc/pid/pagemap file
 * @param[in]  vaddr      virtual address to get entry for
 * @return 0 for success, 1 for failure
 */
int pagemap_get_entry(PagemapEntry *entry, int pagemap_fd, uintptr_t vaddr)
{
    size_t nread;
    ssize_t ret;
    uint64_t data;
    uintptr_t vpn;

    vpn = vaddr / sysconf(_SC_PAGE_SIZE);
    nread = 0;
    while (nread < sizeof(data)) {
        ret = pread(pagemap_fd, ((uint8_t*)&data) + nread, sizeof(data) - nread,
                vpn * sizeof(data) + nread);
        nread += ret;
        if (ret <= 0) {
            return 1;
        }
    }
    entry->pfn = data & (((uint64_t)1 << 55) - 1);
    entry->soft_dirty = (data >> 55) & 1;
    entry->file_page = (data >> 61) & 1;
    entry->swapped = (data >> 62) & 1;
    entry->present = (data >> 63) & 1;
    return 0;
}

/* Convert the given virtual address to physical using /proc/PID/pagemap.
 *
 * @param[out] paddr physical address
 * @param[in]  pid   process to convert for
 * @param[in] vaddr virtual address to get entry for
 * @return 0 for success, 1 for failure
 */
int virt_to_phys_user(uintptr_t *paddr, pid_t pid, uintptr_t vaddr)
{
    char pagemap_file[BUFSIZ];
    int pagemap_fd;

    snprintf(pagemap_file, sizeof(pagemap_file), "/proc/%ju/pagemap", (uintmax_t)pid);
    pagemap_fd = open(pagemap_file, O_RDONLY);
    if (pagemap_fd < 0) {
        return 1;
    }
    PagemapEntry entry;
    if (pagemap_get_entry(&entry, pagemap_fd, vaddr)) {
        return 1;
    }
    close(pagemap_fd);
    *paddr = (entry.pfn * sysconf(_SC_PAGE_SIZE)) + (vaddr % sysconf(_SC_PAGE_SIZE));
    return 0;
}

int main(int argc, char **argv){
    pid_t pid;
    uintptr_t vaddr, paddr = 0,aligned,top;
    int w=WIDTH,h=HEIGHT,pb=PXBYTES,debug=0,pagecount=0;
    if(argc>2){
        if(sscanf(argv[1],"%d",&w)+sscanf(argv[2],"%d",&h)!=2){
            printf("Could not parse w,h from %s %s\n",argv[1],argv[2]);
            return EXIT_FAILURE;    
            }    
        if(argc==4)debug=1;
        uint32_t bufsize=w*h*pb;           
        pagecount=bufsize/4096;
        }
    else if(argc==2){
        if(sscanf(argv[1]," %d ",&pagecount)!=1)printf("Could not parse pagecount from \"%s\"\n",argv[1]);
        }
    else{
        printf("w,h required\n");
        return EXIT_FAILURE;    
        }    
    pid=(uintmax_t)getpid();
    void**bufs=(void**)malloc(pagecount*sizeof(void*));
    uint16_t**actuals=(uint16_t**)malloc(pagecount*sizeof(uint16_t*));
    FILE*f=fopen("fpgagpu_mmap.txt","w");
    for(int i=0;i<pagecount;i++){    
        bufs[i]=calloc(8192,1);         
        vaddr=(uintptr_t)bufs[i];
        virt_to_phys_user(&paddr, pid, vaddr);
        aligned=(paddr&0xFFFFF000)+0x1000;
        if(debug){
            printf("BLOCK %d/%d\n",i+1,pagecount);
            printf("vaddr:   0x%jx\n",(uintmax_t)vaddr);
            printf("paddr:   0x%jx\n",(uintmax_t)paddr);        
            printf("aligned: 0x%jx\n",(uintmax_t)aligned);
            printf("remake\n");
            }
        vaddr+=(aligned-paddr);
        actuals[i]=(int16_t*)vaddr;
        for(int j=0;j<2048;j++)(actuals[i][j])=Z;
        virt_to_phys_user(&paddr, pid, vaddr);
        fprintf(f,"0x%jx;//\n",(uintmax_t)paddr);
        if(pagecount<6)printf("0x%jx\n",(uintmax_t)paddr);
        if(debug==1){
            printf("vaddr:   0x%jx\n",(uintmax_t)vaddr);
            printf("paddr:   0x%jx\n",(uintmax_t)paddr);       
            
            printf("check1\n");
            vaddr-=4;
            virt_to_phys_user(&paddr, pid, vaddr);
            printf("vaddr:   0x%jx\n",(uintmax_t)vaddr);
            printf("paddr:   0x%jx\n",(uintmax_t)paddr);        
            printf("check2\n");
            vaddr+=4096;
            virt_to_phys_user(&paddr, pid, vaddr);
            printf("vaddr:   0x%jx\n",(uintmax_t)vaddr);
            printf("paddr:   0x%jx\n",(uintmax_t)paddr);     
            }
    //    top=aligned+bufsize;
    //    printf("top:     0x%jx\n",(uintmax_t)top);
        }
    fclose(f);
    printf("%d pages ready\n",pagecount);
    while(1){
        int read=getchar();
        if(read==EOF)break;
        else if(read=='q')break;
        else if(read=='p'){
            f=fopen("fpga_dump.txt","w");
            int lineskip=0;
            for(int page=0;page<pagecount;page++)for(int i=0;i<2048;i++){
                fprintf(f,"%hx ",actuals[page][i]);    
                lineskip++;
                if(lineskip==w){
                    fprintf(f,"\n");    
                    lineskip=0;    
                    }
                }                
                
            fclose(f);    
            }
        else if(read=='r')for(int i=0;i<pagecount;i++)for(int j=0;j<2048;j++)(actuals[i][j])=R;
        else if(read=='g')for(int i=0;i<pagecount;i++)for(int j=0;j<2048;j++)(actuals[i][j])=G;
        else if(read=='b')for(int i=0;i<pagecount;i++)for(int j=0;j<2048;j++)(actuals[i][j])=B;
        else if(read=='w')for(int i=0;i<pagecount;i++)for(int j=0;j<2048;j++)(actuals[i][j])=W;
        else if(read=='z')for(int i=0;i<pagecount;i++)for(int j=0;j<2048;j++)(actuals[i][j])=Z;
        else if(read=='i')for(int i=0;i<pagecount;i++)for(int j=0;j<2048;j+=2)(actuals[i][j])=Z;
        }
    for(int i=0;i<pagecount;i++)free(bufs[i]);
    return EXIT_SUCCESS;
    }