

# Regression test
`./tests.sh` will run a set of tests in GHDL. No prerequisites.

If you want to run the same tests on Zynq, first set up the enviroment there per instructions below.
Then compile the test programs and copy them on board. Easiest way to do this is:
```
./tests.sh
scp ../software/out/rdwrmem/*.dat <board IP>:/opt/zynq/upbit/rdwrmem
``` 
Finally run this on board:

```
./physget.out 1
./fpgatest.sh <address allocated by physget>
```

No tests are provided for ICE-V.

# Zynq enviroment setup
### First run
Do this once unless developing some of the code involved.

1. Gain ssh access to mzapo board. Instructions here: https://cw.fel.cvut.cz/wiki/courses/b35apo/documentation/mz_apo-howto/start
 
2. Copy these files to board
```
./run.sh
./rdwrmemf.c
./physget.c
./fpgatest.sh
./comparer.c
./debugger/*
./tracer/*
./view.sh
```
Only the first two are necessary for basic operation. The rest is for other tasks, listed here so that this documentation can henceforth assume that reader has all these files onboard.

3. On board, compile `physget.c` and `rdwrmemf.c`:
```
gcc physget.c -o physget.out
gcc rdwrmemf.c -o rdwrmemf
```
Tracer and debugger, if you find yourself needing them, have makefiles.

4. Generate bitstream. See `/integration/vivado` for instructions.

5. Finalize bitstream generation and copy it to board:
    `./bingen.sh <board IP>`
   The bitstream will be at `/opt/zynq/upbit/top_hdl.bit.bin`. 
   Vivado must be available in command line for this step. Use 
   `source /<Vivado install dir>/Xilinx/Vivado/2018.2/settings.sh` 

### All subsequent runs:
Go to `/opt/zynq/upbit` on board and run
`./upbit - top_hdl.bit.bin`

Now the FPGA is ready. Either run a test set as described above, or run a single program like so:

1. Obtain the program. See `/software/c/` for instructions, or for basic example, run 
`make CFILE=tests/alu_smoke.c`
in the same folder, then copy `/software/out/rdwrmem/alu_smoke.dat` to board and run
`./run.sh alu_smoke.dat`

# Examine running code.
Now you should be able to run a program on FPGA.
To see if anything happened, you can use:
* `rdwrmemf` can be used to read and write physical address. With `-h` or `--help` it will output list of commands.
* `./view.sh <address> <length>` is a shorthand to read physical memory with rdwrmemf.
    Address can be replaced with `-instr, -data, -ctrl, -debug` to view the various memories of the system.
* `./view.sh -ctrl 64` will, among others, display a word like this: `0x__FFFFFF`. The `__` is trap signal of the processor, if it is not `00`, something is wrong.

Note that reset affects only the processor, memory content will remain and can be viewed.

## Debugger
Compile it with make and run 
`./d -a` to attach to a running program, or `./d <path to program>`.
This will launch text-based debugger.
Use `h` to list commands. Recommeded to use `g` for ncurses mode with text "GUI".

## Tracer
Program created for tracing motor control behaviour, but it can be readily modified for other purposes.

# Known issues
`./run.sh` always rewrites all 8kB of memory. If the provided file is not large enough, it is repeated. This can cause errors.

# Script list
Here, scripts in this folder are listed, not divided by purpose.

#### scripts/tests.sh - simulated test suite
`./tests.sh` ran from `scripts` folder runs a suite of tests in simulation to verify correctness of HDL design.

#### scripts/comparer.c - check test results
This program compares rvapo memory dumps against given files. Just put it in the same folder as the script that runs tests.
Prints help if no argument.

#### scripts/bingen.sh - finalize bitstream
This script runs Vivado for final step of bitstream generation and then copies it to board.
It can also be used to find out where the bitstream is in the sprawling labyrinth of Vivado projects.

#### scripts/rdwrmemf.c - Access physical memory on Linux
The FPGA is accessed as memory-mapped peripheral of the ARM processor. `/scripts/rdwrmemf.c` is used to access physical memory on the onboard Linux. 
Compile it with `gcc rdwrmemf.c -o rdwrmemf`.

#### scripts/physget.c - Allocate physical memory on Linux.
Run `./physget.out x &` to allocate x pages of physical memory. Their physical addresses will be printed to `fpgagpu_mmap.txt` and also to stdout if there is less that 6 of them. In this project, one is enough. Keep it running in background lest memory is deallocated!

#### scripts/fpgatest.sh - test suite on fpga
`fpgatest.sh <allocated physical memory>` runs the same test suite as `scripts/tests.sh`, but on fpga.

#### scripts/run.sh - run program on fpga
`run.sh <program> <dsel>` loads and runs program in the chip. Check that addresses inside are same as in Vivado address editor (`0x4*` by default).  `<program>` is file from `software/out/rdwrmem/`, `dsel` is selector for debug memory which defaults to 0.

**This script resets processor after a second. Comment out last line to prevent this.**

#### scripts/view.sh - view physical memory of fpga
 `view.sh <area> <length>` is shorthand to view physical memory using `rdwrmemf`.

-`-instr` for instruction memory 0x40000000
-`-data` for data memory 0x42000000
-`-debug` for debug memory 0x44000000
-`-46` for 0x46000000, which is a BRAM accessed through AXI.
- anything else will be directly fed as address to rdwrmemf.

#### scripts/debugger - debug programs on fpga
Copy `scripts/debugger` folder to mzapo, then use `make` to compile it.
Run `./d <program>` to debug a program, or `-a` to attach to a running program. 
It will start in interactive terminal mode with rvapo paused. Use `h` (and Enter) to see available commands. 
It is recommended to switch into ncurses mode using `g`.  

#### Debug memory
You can save one word (4B) of data to a separate memory if enabled by apropriate generic (default system has it enabled).

In interconnect, you can define which signals from the interconnect should be made available for this (up to 255 options).
Then, use the `dsel` signal on the cpu to route one of them into said memory. This signal can be controlled from ARM using memory mapped registers, further described in fpga section.



