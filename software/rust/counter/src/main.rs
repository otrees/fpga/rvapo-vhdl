#![no_std]
#![no_main]

#![feature(asm)]

common::register_main!(main);

static mut DATA: [u32; 4] = [0xCCCCCCCC, 0x00000010, 0x00000100, 0xEEEEEEEE];

pub fn main() {
	unsafe {
		asm!(
			"mv t1, {0}",
			"lw t3, 4(t1)",
			"lw t4, 8(t1)",
			"add t5, t3, t4",
			"addi t5, t5, 1",
			"sw t5, 0(t1)",
			"addi t6, t6, 1",
			"sw t6, 12(t1)",
			"bne t6, t3, -28",
			in(reg) &DATA
		);

		common::__set_simulation_debug_registers(
			DATA[0] as usize, DATA[1] as usize, DATA[2] as usize, DATA[3] as usize
		);
	}
}
