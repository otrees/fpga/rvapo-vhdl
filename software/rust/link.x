/* 1K = 1 KiBi = 1024 bytes */
MEMORY {
	RAM : ORIGIN = 0x00000000, LENGTH = 64K
}

/* Make sure reset vector is included */
EXTERN(_RESET_VECTOR);
ENTRY(_reset);

SECTIONS {
	.reset_vector ORIGIN(RAM) : {
		/* stack pointer */
		LONG(ORIGIN(RAM) + LENGTH(RAM));
		/* reset vector */
		KEEP(*(.reset_vector));
	} > RAM

	.static : {
		*(.eh_frame)
		*(.rodata .rodata.*)
		*(.data .data.*)
	} > RAM

	.text : {
		*(.text .text.*);
	} > RAM

	/* discard? */
}
