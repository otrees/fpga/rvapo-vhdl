#ifndef _TUMBL_ADDR_H
#define _TUMBL_ADDR_H
#include <stdint.h>
//This file defines adresses of rvapo memory mapped peripherals

#ifndef IOPORT32BIT
 #define IO32ADDR(_val) ((volatile uint32_t *)(_val))
#endif /*IOPORT32BIT*/

#define FPGA_FNCAPPROX_BASE     0x00002000

#define FPGA_FNCAPPROX          IO32ADDR(FPGA_FNCAPPROX_BASE)
#define FPGA_FNCAPPROX_RECI     IO32ADDR(FPGA_FNCAPPROX_BASE+0x04)
#define FPGA_FNCAPPROX_SIN      IO32ADDR(FPGA_FNCAPPROX_BASE+0x08)
#define FPGA_FNCAPPROX_COS      IO32ADDR(FPGA_FNCAPPROX_BASE+0x0c)

#define FPGA_PMSM_3PMDRV_BASE   0x43c20000
#define FPGA_PMSM_3PMDRV_PTR    IO32ADDR(FPGA_PMSM_3PMDRV_BASE) 

#define FPGA_IRC0               IO32ADDR(FPGA_PMSM_3PMDRV_BASE+0x0008) //axi reg 2

#define FPGA_PMSM_3PMDRV_PWM0   IO32ADDR(FPGA_PMSM_3PMDRV_BASE+0x10)//axi reg 4,5,6
#define FPGA_PMSM_3PMDRV_PWM1   IO32ADDR(FPGA_PMSM_3PMDRV_BASE+0x14)
#define FPGA_PMSM_3PMDRV_PWM2   IO32ADDR(FPGA_PMSM_3PMDRV_BASE+0x18)

#define FPGA_PMSM_3PMDRV_ADC0   IO32ADDR(FPGA_PMSM_3PMDRV_BASE+0x24)//axi reg 9,10,11
#define FPGA_PMSM_3PMDRV_ADC1   IO32ADDR(FPGA_PMSM_3PMDRV_BASE+0x28)
#define FPGA_PMSM_3PMDRV_ADC2   IO32ADDR(FPGA_PMSM_3PMDRV_BASE+0x2c)

#define FPGA_PWM_SHUTDOWN_BIT   31
#define FPGA_PWM_ENABLE_BIT     30

#define FPGA_PWM_SHUTDOWN_MASK  (1<<FPGA_PWM_SHUTDOWN_BIT)
#define FPGA_PWM_ENABLE_MASK    (1<<FPGA_PWM_ENABLE_BIT)

#define FPGA_STAT_REG           IO32ADDR(FPGA_PMSM_3PMDRV_BASE+0x20)//ideally this register should be read once per iter cause axi slow
#define FPGA_STAT_GET_COUNT(a)  ((a)&0x1FFF)//this gets measur_count from adc_reader in pmsm peripheral       

#endif /* _TUMBL_ADDR_H */
