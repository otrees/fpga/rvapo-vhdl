This is a program for commutation of motors.
To run it:

## Hardware
1. Obtain motor connected via `3p-motor-driver-1` peripheral.
2. Program FPGA with a system that has the `pmsm_3pmdrv1_1.0` peripheral. The system provided in `/integratio/vivado` should do.
3. If the peripheral is not at `0x43c20000`, then edit addresses in the program (by default it is).

## Running program
Compile the [pxmc-linux](https://gitlab.fel.cvut.cz/otrees/motion/pxmc-linux/-/tree/mz_apo-pmsm-rvapo?ref_type=heads) project using make. Then run the `mz_apo-pmsm-rvapo` program, which allows you to control the motor from terminal. This program contains a firmware that it loads into rvapo automatically, assuming default addresses listed in `/integratio/vivado`.
 
Examples:
`help`      to list commands.
`GA:100`    to go to absolute position.
`APA?`      to get current axis position.
`SPDA:100`  to spin at fixed speed.

## Development
To run this program without `pxmc-linux`, first disable the watchdog that checks if `pxmc-linux` is alive. Setting `WATCHDOG_RESET` to 0 suffices. Then use `make` to compile the program and finally run `firmware.dat` as explained in `/software/out/`.

The commutator is controlled through the `pxmcc_data_t` structure. Provided linker script places its address at address `0x10` of rvapo data memory, or `0x42000010` of ARM memory with default vivado project.

Using `rdwrmem`, you can set `trace_address` and `trace_size` to point to an allocated area of ARM physical RAM. This program will then write a trace of motor control values in this area. The code is at the very end of the main `.c` file; edit it at will.

To allocate more than one page of continuous physical memory, download [udmabuf](https://github.com/ikwzm/udmabuf) to the board and compile it, then activate using 
`insmod u-dma-buf.ko udmabuf0=<desired memory size>`
`/sys/class/u-dma-buf/udmabuf0` will then contain address and other information about the allocated space.

You can also use a tracer running on ARM, located in `/scripts/tracer`.
Compile with `make` and run like so:
`./tracer <mode> <address of pxmcc_data_t> <address of pmsm_3pmdrv1_1.0>`

Modes are:
* -w for watch, activates only when PWM duty on motor exceeds a threshold = it started doing something. For tracing with `pxmc-linux`.
* -i injects a fake IRC value to make the motor spin. Requires `pxmcc_data.axis[0].inp_info=&pxmcc_data.axis[0].steps_pos;` in the rvapo software. Intended for standalone use.
