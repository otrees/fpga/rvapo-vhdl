/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  firmware.c - multi axis motion controller coprocessor
               firmware for FPGA RISC-V RVapo CPU
               the code and concept is based on PiKRON's LX_RoCoN
               system design

  (C) 2001-2024 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2024 by PiKRON Ltd. https://www.pikron.com/
  (C) 2024 Damir Gruncl - port to RVapo

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

/* Suppress/replace if-then-else code by masking and other tricks */
#define SUPPRESS_CONDITIONALS 1
/* Compute section 0 to 5 for 3 phase motor during PWM generation */
//#define COMPUTE_PHASE_SECTOR 1  /* not recommended */
/* Include support to store traces into provided memory */
#define WRITE_TRACE 1
/* Compute current from ADC channels corresponding to low(er) PWMs */
//#define CURRENT_FROM_LOWER 1
/* Phase currents are converted by SumInt ADC as averages over low */
//#define SUMINT 1

#include <stdint.h>
#include "pxmcc_types.h"
#include "tumbl_addr.h"

#define PWMLIM(a)           (a)//(a&0xFF)

/* 1 / sqrt(3) * 65536 */
#define RECI16_SQRT3  37837

/* 1 / 3 * 65536 */
#define RECI16_3 21845

#define FP2FIX16(a)      (int32_t)((a)*((double)0x10000))//convert to 16.16 fixed point

#if defined(SUMINT) && !defined(CURRENT_FROM_LOWER)
  #error CURRENT_FROM_LOWER is required for SUMINT
#endif

#define offsbychar(_ptr, _offs) \
   ((typeof(&(_ptr)[0]))((char*)(_ptr) + (_offs)))

volatile pxmcc_data_t pxmcc_data={
    .common={
        .fwversion=0,  /* set to PXMCC_FWVERSION to indicate ready */
        .pwm_cycle=5000,
        .min_idle=0x7fff,
        .irc_base=(uint32_t)FPGA_IRC0
    },
    .axis={
        {
        .mode=PXMCC_MODE_BLDC,
        .inp_info=(uint32_t)FPGA_IRC0,
        .out_info = 0,
        .ptirc  = 1000,
        .ptreci=(1ll<<32)/1000,
        .pwmtx_info=0x00060504,
        .cur_cal_matrix={{FP2FIX16(-0.968973),FP2FIX16( 0.001363)},
                         {FP2FIX16( 0.002462),FP2FIX16(-0.967489)}},
        }
    },
};

void init_defvals(void){}

void main(void)
{
  uint32_t last_rx_done_sqn = 0xffffffff;  /* this is last adc measure count */
  pxmcc_axis_data_t *pxmcc;
  int curtrace_offset=0;

  //  pxmcc_data.common.pwm_cycle = 2500;
  //  pxmcc_data.common.min_idle = 0x7fff;
  //  pxmcc_data.common.irc_base = (uint32_t)FPGA_IRC0;
  pxmcc_data.axis[0].inp_info=(uint32_t)FPGA_IRC0;//&pxmcc_data.axis[0].steps_pos;
  pxmcc_data.axis[0].out_info = 0;

  pxmcc = pxmcc_data.axis;
  int offset=0;
  do {
    offset+=4;
    pxmcc->ccflg  = 0;
    pxmcc->mode   = PXMCC_MODE_IDLE;
    pxmcc->ptirc  = 1000;     /* irc pulses per electrical "rotation" */
    pxmcc->ptreci = 4294967; /* (1LL<<32)*ptper/ptirc */
    pxmcc->pwm_dq = 0;
    pxmcc++;
  } while(pxmcc != pxmcc_data.axis + PXMCC_AXIS_COUNT);

  /* fill version to shared data to indicate that coprocessor is ready */
  asm volatile("": : : "memory");
  pxmcc_data.common.fwversion = PXMCC_FWVERSION;
  asm volatile("": : : "memory");

  /* main firmware loop starts there */
  while(1) {

    /* watchdog ensuring to zero outputs when higher layer is not active */
    pxmcc_data.common.watchdog_counter--;
    if(pxmcc_data.common.watchdog_reset==WATCHDOG_RESET) {
      pxmcc_data.common.watchdog_counter=WATCHDOG_TIMEOUT;
      pxmcc_data.common.watchdog_reset=0;
    } else if(pxmcc_data.common.watchdog_counter<0) {
      pxmcc = pxmcc_data.axis;
      do {
        pxmcc->pwm_dq = 0;
        pxmcc++; /* move to next axis */
      } while (pxmcc != pxmcc_data.axis + PXMCC_AXIS_COUNT);
    }

    pxmcc = pxmcc_data.axis;
    do {
      uint32_t irc;
      uint32_t ofs = pxmcc->ptofs;
      uint32_t per = pxmcc->ptirc;
      int32_t  pti;
      uint32_t pta;
      uint32_t pwmtx_info;
      volatile uint32_t *uptr;

      irc = *(uint32_t*)pxmcc->inp_info;

      /* Transform IRC into phase table index: 1. substract offset */
      pti = irc - ofs;
      /*
       * the '>=' is appropriate to keep pti in <0;per-1> range,
       * but sine and cosine computations can work over multiple
       * periods and value of per=0xffffffff allows to control/stop
       * updates if only '>' is used.
       */
      if ((uint32_t)pti > per) {
        /* 2. add/sub per(irc count in phase table) until pti is in phase table */
        if (pti < 0) {
          ofs -= per;
        } else {
          ofs += per;
        }
        pti = irc - ofs;
        pxmcc->ptofs = ofs;
      }
      pxmcc->ptindx = pti;

      pta = pti * pxmcc->ptreci; /* scale pti to 32-bit unit */

      /* the phase table value is computed, compute sine and cosine for Park */
      *FPGA_FNCAPPROX_SIN = pta;
      asm volatile("nop\n");
      asm volatile("nop\n");
      asm volatile("nop\n");
      pxmcc->ptsin = *FPGA_FNCAPPROX_SIN;
      asm volatile("nop\n");
      pxmcc->ptcos = *FPGA_FNCAPPROX_COS;

      /* if given axis processing is enabled (flag=0 is disable) */
      if (pxmcc->ccflg) {
        int32_t pwm_alp;
        int32_t pwm_bet;
        int32_t pwm_bet_div_2_k3;
        uint32_t pwm1;
        uint32_t pwm2;
        uint32_t pwm3;
        uint32_t pwm4;
        int32_t pwm_d;
        int32_t pwm_q;
       #if defined(COMPUTE_PHASE_SECTOR) || !defined(SUPPRESS_CONDITIONALS)
        uint32_t phs;
       #endif /*COMPUTE_PHASE_SECTOR*/

        /* decode requested D and Q PWM components from higher level controller */
        pwm_d = (volatile uint32_t)pxmcc->pwm_dq;
        pwm_q = (pwm_d << 16) >> 16;
        pwm_d >>= 16;

        /* Transform PWM DQ by inverse Park transformation to stator Alpha Beta */
        pwm_alp = pwm_d * pxmcc->ptcos - pwm_q * pxmcc->ptsin;
        pwm_bet = pwm_d * pxmcc->ptsin + pwm_q * pxmcc->ptcos;

        if (pxmcc->mode == PXMCC_MODE_BLDC) {
          /* BLDC mode PWM trasformation, inverse Clarke */
          pwm_bet_div_2_k3 = RECI16_SQRT3 * (pwm_bet >> 16);

         #ifndef SUPPRESS_CONDITIONALS
          if (pwm_bet > 0)
            if (pwm_alp > 0)
              /* pwm_bet > 2 * k3 * pwm_alp */
              if (pwm_bet_div_2_k3 > pwm_alp)
                phs = 1;
              else
                phs = 0;
            else
              /* -pwm_bet > 2 * k3 * pwm_alp */
              if (pwm_bet_div_2_k3 < -pwm_alp)
                phs = 2;
              else
                phs = 1;
          else
            if (pwm_alp > 0)
              /* pwm_bet > -2 * k3 * pwm_alp */
              if (pwm_bet_div_2_k3 > -pwm_alp)
                phs = 5;
              else
                phs = 4;
            else
              /* pwm_bet > 2 * k3 * u_alp */
              if (pwm_bet_div_2_k3 > pwm_alp)
                phs = 3;
              else
                phs = 4;

          if (phs <= 1) {
            /* pwm1 = pwm_alp + 1.0/(2.0*k3) * pwm_bet */
            pwm1 = (pwm_alp + pwm_bet_div_2_k3) >> 16;
            /* pwm2 = 1/k3 * pwm_bet */
            pwm2 = pwm_bet_div_2_k3 >> 15;
            pwm3 = 0;
          } else if (phs <= 3) {
            pwm1 = 0;
            /* pwm2 = 1.0/(2.0*k3) * pwm_bet - pwm_alp */
            pwm2 = (pwm_bet_div_2_k3 - pwm_alp) >> 16;
            /* pwm3 = -1.0/(2.0*k3) * pwm_bet - pwm_alp */
            pwm3 = (-pwm_bet_div_2_k3 - pwm_alp) >>16;
          } else {
            /* pwm1 = pwm_alp - 1.0/(2.0*k3) * pwm_bet */
            pwm1 = (pwm_alp - pwm_bet_div_2_k3) >>16;
            pwm2 = 0;
            /* pwm3 = -1/k3 * pwm_bet */
            pwm3 = -pwm_bet_div_2_k3 >> 15;
          }
         #else /*SUPPRESS_CONDITIONALS*/
          {
            int32_t alp_m_bet_d2k3;
            uint32_t state23_msk;
            uint32_t pwm23_shift;
            uint32_t bet_sgn = pwm_bet >> 31;
            uint32_t alp_sgn = pwm_alp >> 31;
           #ifdef COMPUTE_PHASE_SECTOR
            uint32_t bet_sgn_cpl = ~bet_sgn;
           #endif /*COMPUTE_PHASE_SECTOR*/

            alp_m_bet_d2k3 = (alp_sgn ^ pwm_alp) - (bet_sgn ^ (pwm_bet_div_2_k3 + bet_sgn));
            alp_m_bet_d2k3 = alp_m_bet_d2k3 >> 31;

            state23_msk = alp_sgn & ~alp_m_bet_d2k3;

            /*
             *   bet alp amb s23
             *    0   0   0   0 -> 0 (000)
             *    0   0  -1   0 -> 1 (001)
             *   -1   0  -1   0 -> 1 (001)
             *   -1   0   0  -1 -> 2 (010)
             *   -1  -1   0  -1 -> 3 (011)
             *   -1  -1  -1   0 -> 4 (100)
             *    0  -1  -1   0 -> 4 (100)
             *    0  -1   0   0 -> 5 (101)
             */

           #ifdef COMPUTE_PHASE_SECTOR
            phs = (bet_sgn & 5) + (bet_sgn_cpl ^ ((alp_m_bet_d2k3 + 2 * state23_msk) + bet_sgn_cpl));
           #endif /*COMPUTE_PHASE_SECTOR*/

            pwm1 = pwm_alp & state23_msk;
            pwm2 = pwm_bet_div_2_k3 - pwm1;
            pwm3 = -pwm_bet_div_2_k3 - pwm1;
            pwm2 &= (~bet_sgn | state23_msk);
            pwm3 &= (bet_sgn | state23_msk);
            pwm1 = pwm_alp + pwm2 + pwm3;
            pwm1 &= ~state23_msk;
            pwm1 >>= 16;
            pwm23_shift = 15 - state23_msk;
            pwm2 >>= pwm23_shift;
            pwm3 >>= pwm23_shift;
          }
         #endif /*SUPPRESS_CONDITIONALS*/

         #ifdef COMPUTE_PHASE_SECTOR
          pxmcc->ptphs = phs;
         #endif /*COMPUTE_PHASE_SECTOR*/

          /* PWM A, B, C is computed, proceed to apply it */
         #if 0
          *FPGA_LX_MASTER_TX_PWM0 = pwm2 | 0x4000;
          *FPGA_LX_MASTER_TX_PWM1 = pwm3 | 0x4000;
          *FPGA_LX_MASTER_TX_PWM2 = pwm1 | 0x4000;
         #else
          pwmtx_info = pxmcc->pwmtx_info;
          /* FPGA_LX_MASTER_TX_PWM0 is replaced by FPGA_PMSM_3PMDRV_PTR */
	  /* save the current PWM, max 14 bits, this corresponds to PMSM driver value */
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  0) & 0xff);
          pxmcc->pwm_prew[1] = *uptr & 0x3fff;
          *uptr = PWMLIM(pwm2) | FPGA_PWM_ENABLE_MASK;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  8) & 0xff);
          pxmcc->pwm_prew[2] = *uptr & 0x3fff;
          *uptr = PWMLIM(pwm3) | FPGA_PWM_ENABLE_MASK;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 16) & 0xff);
          pxmcc->pwm_prew[0] = *uptr & 0x3fff;
          *uptr = PWMLIM(pwm1) | FPGA_PWM_ENABLE_MASK;
         #endif
         /* END of PWM generation for BLCD/PMSM mode (PXMCC_MODE_BLDC) */
        } else {
          /* when not BLCD, use 4 PWM channels - stepper motor */
          uint32_t alp_sgn = pwm_alp >> 31;
          uint32_t bet_sgn = pwm_bet >> 31;
          pwm_alp >>= 16;
          pwm_bet >>= 16;
          pwm1 = -pwm_alp & alp_sgn;
          pwm2 = pwm_alp & ~alp_sgn;
          pwm3 = -pwm_bet & bet_sgn;
          pwm4 = pwm_bet & ~bet_sgn;

          pwmtx_info = pxmcc->pwmtx_info;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  0) & 0xff);
          pxmcc->pwm_prew[0] = *uptr & 0x3fff;
          *uptr = pwm1 | 0x4000;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  8) & 0xff);
          pxmcc->pwm_prew[1] = *uptr & 0x3fff;
          *uptr = pwm2 | 0x4000;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 16) & 0xff);
          pxmcc->pwm_prew[2] = *uptr & 0x3fff;
          *uptr = pwm3 | 0x4000;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 24) & 0xff);
          pxmcc->pwm_prew[3] = *uptr & 0x3fff;
          *uptr = pwm4 | 0x4000;
          if (pxmcc->mode == PXMCC_MODE_STEPPER) {
            if ((pxmcc->steps_sqn_next ^ last_rx_done_sqn) & 0x1f) {
              pxmcc->steps_pos += pxmcc->steps_inc;
            } else {
              pxmcc->steps_pos = pxmcc->steps_pos_next;
              pxmcc->steps_inc = pxmcc->steps_inc_next;
              pxmcc->steps_inc_next = 0;
            }
          }
        }
      } else {
        /* if axis is not active, then do not update PWM, just read current value */
        if (pxmcc->mode == PXMCC_MODE_BLDC) {
          pwmtx_info = pxmcc->pwmtx_info;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  0) & 0xff);
          pxmcc->pwm_prew[1] = *uptr & 0x3fff;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  8) & 0xff);
          pxmcc->pwm_prew[2] = *uptr & 0x3fff;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 16) & 0xff);
          pxmcc->pwm_prew[0] = *uptr & 0x3fff;
        } else {
          pwmtx_info = pxmcc->pwmtx_info;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  0) & 0xff);
          pxmcc->pwm_prew[0] = *uptr & 0x3fff;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  8) & 0xff);
          pxmcc->pwm_prew[1] = *uptr & 0x3fff;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 16) & 0xff);
          pxmcc->pwm_prew[2] = *uptr & 0x3fff;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 24) & 0xff);
          pxmcc->pwm_prew[3] = *uptr & 0x3fff;
        }
      }
      pxmcc++; /* move to next axis */
    } while(pxmcc != pxmcc_data.axis + PXMCC_AXIS_COUNT);
    /* thus is PWM for all axes updated */

    asm volatile("": : : "memory");

    /*
     * original firmware proceeds to synchronise using last_rx_done_sqn
     * and FPGA_LX_MASTER_RX_DDIV, also records the shortest time
     * to wait for sync
     */
    {
      uint32_t idlecnt = 0;
      uint32_t sqn;
      do {
        sqn = FPGA_STAT_GET_COUNT(*FPGA_STAT_REG);
        idlecnt++;
      } while ((sqn == last_rx_done_sqn) || (sqn > 0xFFF));
      pxmcc_data.common.act_idle = idlecnt;
      if ((idlecnt < pxmcc_data.common.min_idle) &&
         (last_rx_done_sqn != 0xffffffff)) {
        pxmcc_data.common.min_idle = idlecnt;
      }
      /* masking out the desync bit is not necessary because we require it to be 0 */
      last_rx_done_sqn = sqn;
      pxmcc_data.common.rx_done_sqn = last_rx_done_sqn;
      asm volatile("": : : "memory");
    }

    /* save ADC values (=current on engine coils) into pxmcc_curadc_data_t */
    volatile pxmcc_curadc_data_t *curadc = pxmcc_data.curadc;
    volatile uint32_t *siroladc = FPGA_PMSM_3PMDRV_ADC0;
    uint32_t val;

    for(int i = 0; i < PXMCC_CURADC_CHANNELS;i++ ) {
      val = *siroladc;

      curadc->cur_val = (uint16_t)(val - curadc->siroladc_last) - curadc->siroladc_offs;
      curadc->siroladc_last = val;

      curadc++;
      siroladc++;
    }

    /* finally it computes DQ currents for each axis */
    pxmcc = pxmcc_data.axis;
    do {
      /* the second loop over all axes, calculates DQ current from ADC */
      volatile int32_t *dptr = (void *)0x20;
      int32_t  cur_alp;
      int32_t  cur_bet;
      int32_t  cur_d;
      int32_t  cur_q;
     #ifdef CURRENT_FROM_LOWER
      uint32_t pwm1;
      uint32_t pwm2;
      uint32_t pwm3;
     #endif /*CURRENT_FROM_LOWER*/
      int32_t  cur1;
      int32_t  cur2;
      int32_t  cur3;
     #ifdef SUMINT
      volatile int32_t  *pcurmult;
      uint32_t curmult_idx;
      uint32_t pwm_reci;
     #endif /*SUMINT*/
      uint32_t out_info;
     #if defined(COMPUTE_PHASE_SECTOR) || !defined(SUPPRESS_CONDITIONALS)
      uint32_t phs;
     #ifdef COMPUTE_PHASE_SECTOR
      phs = pxmcc->ptphs;
     #endif /*COMPUTE_PHASE_SECTOR*/
     #endif /*COMPUTE_PHASE_SECTOR*/

      out_info = pxmcc->out_info;
      if (pxmcc->mode == PXMCC_MODE_BLDC) {

       #ifdef CURRENT_FROM_LOWER
        pwm1 = pxmcc->pwm_prew[0];
        pwm2 = pxmcc->pwm_prew[1];
        pwm3 = pxmcc->pwm_prew[2];//read previous pwm value

       #ifndef SUPPRESS_CONDITIONALS
        #ifndef COMPUTE_PHASE_SECTOR
        if (pwm1 > pwm2)
          if (pwm2 > pwm3)
            phs = 0;
          else if (pwm1 > pwm3)
            phs = 5;
          else
            phs = 4;
        else
          if (pwm2 < pwm3)
            phs = 3;
          else if (pwm1 < pwm3)
            phs = 2;
          else
            phs = 1;
        #endif /*COMPUTE_PHASE_SECTOR*/

       #ifdef SUMINT
        curmult_idx = (0x00201201 >> (4 * phs)) & 3;
        pwm_reci = pxmcc_data.common.pwm_cycle - pxmcc->pwm_prew[curmult_idx];
        pwm_reci = (pxmcc_data.common.pwm_cycle << 16) / pwm_reci;

        /*
         * Translate index from pwm1, pwm2, pwm3 order to
         * to order of current sources 0->2 1->0 2->1
         *
         * This solution modifies directly value in pxmcc_curadc_data_t
         * so it is destructive and has not to be applied twice,
         * but it is much better optimized
         */
        curmult_idx = (0x102 >> (curmult_idx * 4)) & 3;
        pcurmult = &pxmcc_data.curadc[out_info + curmult_idx].cur_val;
        *pcurmult = (int32_t)(pwm_reci * (*pcurmult)) >> 16;
       #endif /*SUMINT*/

        cur2 = pxmcc_data.curadc[out_info + 0].cur_val;
        cur3 = pxmcc_data.curadc[out_info + 1].cur_val;
        cur1 = pxmcc_data.curadc[out_info + 2].cur_val;

        if ((phs == 5) || (phs == 0))
          cur_alp = -(cur2 + cur3);
        else
          cur_alp = cur1;

        if ((phs == 5) || (phs == 0))
          cur_bet = cur2 - cur3;
        else if ((phs == 3) || (phs == 4))
          cur_bet = 2 * cur2 + cur1;
        else /* 1 2 */
          cur_bet = -(2 * cur3 + cur1);
       #else /*SUPPRESS_CONDITIONALS*/
        {
          /*
           *   u1>u2 u2>u3 u1>u3               cm
           *     0     1     1 ->  1 (1, 0, 2) 0
           *     0     1     0 ->  2 (1, 2, 0) 2
           *     0     0     0 ->  3 (2, 1, 0) 1
           *     1     0     0 ->  4 (2, 0, 1) 0
           *     1     0     1 ->  5 (0, 2, 1) 2
           *     1     1     1 ->  0 (0, 1, 2) 1
           */

          uint32_t u1gtu2 = (int32_t)(pwm2 - pwm1) >> 31;
          uint32_t u1gtu3 = (int32_t)(pwm3 - pwm1) >> 31;
          uint32_t u2gtu3 = (int32_t)(pwm3 - pwm2) >> 31;
          uint32_t state50_msk = u1gtu2 & u1gtu3;
         #ifdef SUMINT
          uint32_t pwm_reci_bits;
          uint32_t sz4idx = sizeof(*pxmcc->pwm_prew);
          uint32_t sz4cur = sizeof(*pxmcc_data.curadc);
          uint32_t curmult_idx2curadc;
         #endif /*SUMINT*/

         #ifdef SUMINT //this is not used when hall sensors measure actuall coil current
         #if 0
          curmult_idx = (((u1gtu3 ^ u1gtu2) | 1) ^ u2gtu3 ^ u1gtu2) & 3;
          pwm_reci = pxmcc_data.common.pwm_cycle - pxmcc->pwm_prew[curmult_idx];
         #else
          /* Variant where curmult_idx is directly computed as byte offset to uint32_t array */
          curmult_idx = (((u1gtu3 ^ u1gtu2) | (1 * sz4idx)) ^ u2gtu3 ^ u1gtu2) & (3 * sz4idx);
          pwm_reci = pxmcc_data.common.pwm_cycle - *offsbychar(pxmcc->pwm_prew, curmult_idx);
         #endif

         #if 0
          pwm_reci_bits = __builtin_clzl(pwm_reci);
         #else
          asm("clz %0,%1\n":"=r"(pwm_reci_bits):"r"(pwm_reci));
         #endif
          pwm_reci <<= pwm_reci_bits;
          *FPGA_FNCAPPROX_RECI = pwm_reci;
          asm volatile("nop\n");
          asm volatile("nop\n");
          asm volatile("nop\n");
          pwm_reci = *FPGA_FNCAPPROX_RECI;
          pwm_reci >>= 16;
          pwm_reci *= pxmcc_data.common.pwm_cycle;
          pwm_reci >>= 30 - pwm_reci_bits;
          /*
           * Translate index from pwm1, pwm2, pwm3 order to
           * to order of current sources 0->2 1->0 2->1
           *
           * This solution modifies directly value in pxmcc_curadc_data_t
           * so it is destructive and has not to be applied twice,
           * but it is much better optimized
           */
         #if 0
          curmult_idx = (0x102 >> (curmult_idx * 4)) & 3;
          pcurmult = &pxmcc_data.curadc[out_info + curmult_idx].cur_val;
         #else
          curmult_idx2curadc = ((2 * sz4cur) << (0 * sz4idx)) |
                               ((0 * sz4cur) << (1 * sz4idx)) |
                               ((1 * sz4cur) << (2 * sz4idx));
          curmult_idx = (curmult_idx2curadc >> curmult_idx) & (sz4cur | (sz4cur << 1));
          pcurmult = &pxmcc_data.curadc[out_info].cur_val;
          pcurmult = offsbychar(pcurmult, curmult_idx);
         #endif
          *pcurmult = (int32_t)(pwm_reci * (*pcurmult)) >> 16;
         #endif /*SUMINT*/
          cur2 = pxmcc_data.curadc[out_info + 0].cur_val;
          cur3 = pxmcc_data.curadc[out_info + 1].cur_val;
          cur1 = pxmcc_data.curadc[out_info + 2].cur_val;

          cur_alp = -(cur2 + cur3);                /* 5 0 */
          cur_alp &= state50_msk;
          cur_alp |= cur1 & ~state50_msk;          /* 1 2 3 4 */

          cur_bet = (-2 * cur3 - cur1) & u2gtu3;   /* 1 2 */
          cur_bet |= (2 * cur2 + cur1) & ~u2gtu3;  /* 3 4 */
          cur_bet &= ~state50_msk;
          cur_bet |= (cur2 - cur3) & state50_msk;  /* 5 0 */
        }
       #endif /*SUPPRESS_CONDITIONALS*/
        cur_bet *= RECI16_SQRT3;
        cur_bet >>= 16;
       #else /*CURRENT_FROM_LOWER*/
        cur2 = pxmcc_data.curadc[out_info + 0].cur_val;
        cur3 = pxmcc_data.curadc[out_info + 1].cur_val;
        cur1 = pxmcc_data.curadc[out_info + 2].cur_val;
        *(dptr++) = cur1; *(dptr++) = cur2; *(dptr++) = cur3; *(dptr++) = 0;

        cur_alp = (2 * cur1 - cur2 - cur3);
        cur_alp *= RECI16_3;
        cur_alp >>= 16;

        cur_bet = cur2 - cur3;
        cur_bet *= RECI16_SQRT3;
        cur_bet >>= 16;
       #endif /*CURRENT_FROM_LOWER*/
        /* end of calculation for BLCD motor */
      } else {
        /* current calculation for other motor types */
        int32_t bet_pwm = pxmcc->pwm_prew[2];
        uint32_t bet_sgn = (bet_pwm - 1) >> 31;
        int32_t alp_pwm = pxmcc->pwm_prew[0];
        uint32_t alp_sgn = (alp_pwm - 1) >> 31;
        cur_bet = (pxmcc_data.curadc[out_info + 3].cur_val & ~bet_sgn) -
                  (pxmcc_data.curadc[out_info + 2].cur_val & bet_sgn);
        cur_alp = (pxmcc_data.curadc[out_info + 1].cur_val & ~alp_sgn) -
                  (pxmcc_data.curadc[out_info + 0].cur_val & alp_sgn);
      }

      *(dptr++) = cur_alp; *(dptr++) = cur_bet;

      { /* apply current calibration matrix */
        int32_t tmp_alp, tmp_bet;
        tmp_alp = pxmcc->cur_cal_matrix[0][0] * cur_alp +
                  pxmcc->cur_cal_matrix[0][1] * cur_bet;
        tmp_bet = pxmcc->cur_cal_matrix[1][0] * cur_alp +
                  pxmcc->cur_cal_matrix[1][1] * cur_bet;
        cur_alp = tmp_alp >> 16;
        cur_bet = tmp_bet >> 16;
      }

      *(dptr++) = cur_alp; *(dptr++) = cur_bet;

      /* Park transform of Alpha Beta currents to DQ */
      cur_d =  cur_alp * pxmcc->ptcos + cur_bet * pxmcc->ptsin;
      cur_q = -cur_alp * pxmcc->ptsin + cur_bet * pxmcc->ptcos;

      *(dptr++) = pxmcc->ptcos; *(dptr++) = pxmcc->ptsin;
      *(dptr++) = cur_d; *(dptr++) = cur_q;

      /* store DQ currents into shared memory structure */
      pxmcc->cur_dq = (cur_d & 0xffff0000) | ((cur_q >> 16) & 0xffff);

      pxmcc->cur_d_cum = ((pxmcc->cur_d_cum + (cur_d >> 4)) & ~0x3f)|(last_rx_done_sqn & 0x1f);
      pxmcc->cur_q_cum = ((pxmcc->cur_q_cum + (cur_q >> 4)) & ~0x3f)|(last_rx_done_sqn & 0x1f);

      pxmcc++;
    } while(pxmcc != pxmcc_data.axis + PXMCC_AXIS_COUNT);

    #ifdef WRITE_TRACE
    if(pxmcc_data.common.trace_address==0)continue;
    if(curtrace_offset>=pxmcc_data.common.trace_size){
      pxmcc_data.common.trace_address=0;
      curtrace_offset=0;
      continue;
      }
    *IO32ADDR(pxmcc_data.common.trace_address+curtrace_offset+0)=pxmcc_data.curadc[0].cur_val;
    *IO32ADDR(pxmcc_data.common.trace_address+curtrace_offset+4)=pxmcc_data.curadc[1].cur_val;
    *IO32ADDR(pxmcc_data.common.trace_address+curtrace_offset+8)=pxmcc_data.curadc[2].cur_val;
    *IO32ADDR(pxmcc_data.common.trace_address+curtrace_offset+12)=0x12344321;
    curtrace_offset+=16;
    #endif
  } /* end of main loop */
}
