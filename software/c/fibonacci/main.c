int fibonacci(int n){
    switch(n){
        case 0:return 0;
        case 1:return 1;
        default:return fibonacci(n-1)+fibonacci(n-2);
        }
    }
int main(){//end of f(10)=0x37=55  at 0x1142c=70700
    int d=fibonacci(5);//result: f(15)=610=0x262 after 1.5e6ns  (git 3e6ns)  in t3=28
	int base=0x1000;
	int val=0xFFFFFFFF;
	asm volatile ( 
//		"sw %2, 8(zero);"
		"sw %0, 4(%1);"
		"sw %2, 0(%1);"
        "sw %2, 8(%1);"
		"sw %2, 8(%1);"
		"sw %2, 8(%1);"
		"sw %2, 8(%1);"
		"ebreak;"
			:
		    : "r" (val),"r" (base)	,"r" (d)	
		);//*/

/*    asm volatile ( //"addl %%ebx, %%eax;"
		"sw %0, 12(zero);"
        "sw %0, 4(%1);"
        "sw %2, 8(%1);"
        "ebreak;"
	//	"mv t5, %2;"
	//	"mv t6, %3;"
    //    : "=a" (add) would be output
        :
        : "r" (d), "r" (base), "r" (val) );//*/
    }
