#include <stdint.h>
volatile int var=0;
volatile int foo=0;
#define C_SW(addr,offset)		(*(volatile uint32_t*)(addr+offset))	
int main(){
    asm volatile(
        "addi tp,zero,4;"
        "addi t0,zero,5;"
        "addi t1,zero,6;"
        "addi t2,zero,7;"

        "addi s0,zero,8;"
        "addi s1,zero,9;"

        "addi a0,zero,10;"
        "addi a1,zero,11;"
        "addi a2,zero,12;"
        "addi a3,zero,13;"
        "addi a4,zero,14;"
        "addi a5,zero,15;"
        "addi a6,zero,16;"
        "addi a7,zero,17;"

        "addi s2,zero,18;"
        "addi s3,zero,19;"
        "addi s4,zero,20;"
        "addi s5,zero,21;"
        "addi s6,zero,22;"
        "addi s7,zero,23;"
        "addi s8,zero,24;"
        "addi s9,zero,25;"
        "addi s10,zero,26;"
        "addi s11,zero,27;"

        "addi t3,zero,28;"
        "addi t4,zero,29;"
        "addi t5,zero,30;"
        "addi t6,zero,31;"

        "sw   tp,0(zero);"//request interrupt

        "nop;"
        "nop;"
        "nop;"
        "nop;"
        "nop;"
        "nop;"//let none touch the registers while waiting for interupt
        ::);

    while(var==0){
        foo++;
        }

    asm volatile(
	"sw 		x3,0xC(a6);"
	"sw 		x4,0x10(a6);"
	"sw 		x5,0x14(a6);"
	"sw 		x6,0x18(a6);"
	"sw 		x7,0x1C(a6);"
	"sw 		x8,0x20(a6);"
	"sw 		x9,0x24(a6);"
	"sw 		x10,0x28(a6);"	
	"sw 		x11,0x2C(a6);"
	"sw 		x12,0x30(a6);"
	"sw 		x13,0x34(a6);"
	"sw 		x14,0x38(a6);"
	"sw 		x15,0x3C(a6);"
	"sw 		x16,0x40(a6);"
	"sw 		x17,0x44(a6);"
	"sw 		x18,0x48(a6);"
	"sw 		x19,0x4C(a6);"
	"sw 		x20,0x50(a6);"
	"sw 		x21,0x54(a6);"
	"sw 		x22,0x58(a6);"
	"sw 		x23,0x5C(a6);"
	"sw 		x24,0x60(a6);"
	"sw 		x25,0x64(a6);"
	"sw 		x26,0x68(a6);"
	"sw 		x27,0x6C(a6);"
	"sw 		x28,0x70(a6);"
	"sw 		x29,0x74(a6);"
	"sw 		x30,0x78(a6);"
	"sw 		x31,0x7C(a6);"
    ::);
    C_SW(0x90,0)=var;
	}	

void interrupt1(){
    var=1000; 
    }    