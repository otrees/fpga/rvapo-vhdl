#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
/**
This program converts binary machine code files (produced by buildc.sh) to format accepted by vhdl, simulated processor or rdwrmemf RAM loader.
*/

#define HELP "ARGV\n \
[0] reserved\n \
[1] opmode: -vhdl (build into code) -hexstring (load in sim) -rdwrmemf (load to ram), mandatory\n \
[2] input:  file to read, default a.ou\n \
[3] output: file to write\n"

#define MODE_VHDL "-vhdl"
#define MODE_HSTR "-hexstring"
#define MODE_RDWR "-rdwrmemf"
#define MODE_HELP "-help"
#define MODE_IC40 "-ice40"  

enum modes{VHDL,HEXSTRING,RDWRMEMF,NONE,ICE40};

void writevhdl(FILE*src,FILE*trg);
void writehstr(FILE*src,FILE*trg);
void writerdwr(FILE*src,FILE*trg);
void writeic40(FILE*src,FILE*trg);
/**
 * ARGV
 * [0] reserved
 * [1] opmode:  -vhdl, -hexstring or -rdwrmemf, mandatory
 * [2] input:   file to read
 * [3] output:  file to write
 **/
int main(int argc, char *argv[]){
    FILE*src;
    if(argc<2){
        printf("At least one argument is required.\n");
        printf(HELP);
        return 101;
        }
    enum modes mode=NONE;
    if(strcmp(argv[1],MODE_VHDL)==0)mode=VHDL;
    else if(strcmp(argv[1],MODE_HSTR)==0)mode=HEXSTRING;    
    else if(strcmp(argv[1],MODE_RDWR)==0)mode=RDWRMEMF; 
    else if(strcmp(argv[1],MODE_IC40)==0)mode=ICE40; 
    else if(strcmp(argv[1],MODE_HELP)==0){
    	printf(HELP);
        return 0;
    	}
    else{
    	printf("Work mode \"%s\" not recognised.\n",argv[1]);
    	return 101;
    	}  

    if(argc>2)src=fopen(argv[2],"rb");
    else src=fopen("./a.out","rb");
    if(src==NULL){
        printf("File to convert not found.\n");
        return 101;
        }
    FILE*trg;

    if(argc>3)trg=fopen(argv[3],"w");
    else if(mode==VHDL)trg=fopen("./out.vhdl","w");
    else if(mode==HEXSTRING)trg=fopen("./rvapo_mem_content.txt","w");
    else if(mode==RDWRMEMF)trg=fopen("./zynq_mem_content.txt","w");
    else if(mode==ICE40)trg=fopen("./ice40_mem_init.vhd","w");
    if(trg==NULL){
        printf("Could not open output file for writing.\n");
        return 101;
        }
        
    if(mode==VHDL)writevhdl(src,trg);
    else if(mode==HEXSTRING)writehstr(src,trg);
    else if(mode==RDWRMEMF)writerdwr(src,trg);
    else if(mode==ICE40)writeic40(src,trg);

    fclose(src);
    fclose(trg);
    return 0;
    }
uint32_t reverse(uint32_t x){
    x = (((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1));
    x = (((x & 0xcccccccc) >> 2) | ((x & 0x33333333) << 2));
    x = (((x & 0xf0f0f0f0) >> 4) | ((x & 0x0f0f0f0f) << 4));
    x = (((x & 0xff00ff00) >> 8) | ((x & 0x00ff00ff) << 8));
    return((x >> 16) | (x << 16));
	}

void writeic40(FILE*src,FILE*trg){
    int i=0;
    int block=0;
	uint16_t buf[16];
    uint32_t read;
    fprintf(trg,
"library IEEE;\n\
use IEEE.std_logic_1164.all;\n\
use IEEE.numeric_std.all;\n\
package ice40_memfill is\n");
    while(fread(&read,1,4,src)==4){
		if(i==0)fprintf(trg,"constant ICE40MEM1LO_%d:std_logic_vector(255 downto 0):=X\"",block);
		buf[i]=(uint16_t)((read>>16)&0xFFFF);//fprintf(trg,"%.4X",read&0xFFFF);
		i++;
		if(i==16){	
			for(i=15;i>=0;i--)fprintf(trg,"%.4X",buf[i]);
			i=0;
			fprintf(trg,"\";\n");
			block++;
			if(block==16){
				fprintf(trg,"\n\n");
				block=0;
				}
			}
		}
	if(i!=0){
		int oldi=i-1;		
		for(;i<16;i++)fprintf(trg,"0000");
		for(i=oldi;i>=0;i--)fprintf(trg,"%.4X",buf[i]);
		fprintf(trg,"\";\n");
		}
    for(block++;block<16;block++)fprintf(trg,
        "constant ICE40MEM1LO_%d:std_logic_vector(255 downto 0):=X\"0000000000000000000000000000000000000000000000000000000000000000\";\n",
        block);        
	i=0;
	block=0;
	fprintf(trg,"\n");
    fseek(src,0,SEEK_SET);
    while(fread(&read,1,4,src)==4){
		if(i==0)fprintf(trg,"constant ICE40MEM1HI_%d:std_logic_vector(255 downto 0):=X\"",block);
		buf[i]=(uint16_t)((read)&0xFFFF);
		i++;
		if(i==16){
			for(i=15;i>=0;i--)fprintf(trg,"%.4X",buf[i]);
			i=0;
			fprintf(trg,"\";\n");
			block++;
			if(block==16){
				fprintf(trg,"\n\n");
				block=0;
				}
			}
		}
	if(i!=0){
		int oldi=i-1;		
		for(;i<16;i++)fprintf(trg,"0000");
		for(i=oldi;i>=0;i--)fprintf(trg,"%.4X",buf[i]);
		fprintf(trg,"\";\n");
		}
    for(block++;block<16;block++)fprintf(trg,
        "constant ICE40MEM1HI_%d:std_logic_vector(255 downto 0):=X\"0000000000000000000000000000000000000000000000000000000000000000\";\n",
        block);    
    fprintf(trg,
"end package ice40_memfill;\n\
package body ice40_memfill is\n\
end;");    
    }


void writeic40_old(FILE*src,FILE*trg){
    int i=0;
    char block[32][256];
    for(int i=0;i<32;i++)for(int j=0;j<256;j++)block[i][j]='0';
    uint32_t read;
    while(fread(&read,1,4,src)==4){
        if(i>255){//dump
            i=0;
            for(int set=0;set<2;set++)for(int j=16*set;j<16*(set+1);j++){
                fprintf(trg,"INIT_%1X=>X\"",j-16*set);
                for(int k=0;k<64;k++){
                    char boilerplate[5];
                    memcpy(boilerplate,block[j]+k*4,4);
                    boilerplate[4]=0;
                    fprintf(trg,"%X",(unsigned int)strtol(boilerplate,NULL,2));    
                    }                
                fprintf(trg,"\",");
                }
			for(int i=0;i<32;i++)for(int j=0;j<256;j++)block[i][j]='0';
			}   
	//	printf("%d: %.8x converted into: ",i,read);
        for(int j=0;j<32;j++){
			block[31-j][i]='0'+(1&(read>>j));
		//	printf("%c='0'+(1 & %d)=%d\n",block[j][i],(read>>j),1&(read>>j));
        //    printf("%c",block[32-j][i]);
			}
    //    for(int j=0;j<32;j++)printf("%c",block[j][i]);    
    //    printf("\n");    
		i++;
        }
    for(int set=0;set<2;set++)for(int j=16*set;j<16*(set+1);j++){
            fprintf(trg,"%d=>X\"",j-16*set);
    //        for(int i=0;i<256;i++)printf("%c",block[j][i]);
   //         printf("\n");
            for(int k=0;k<64;k++){
                char boilerplate[5];
                memcpy(boilerplate,block[j]+k*4,4);
                //boilerplate[0]=block[j][k*4];
                //boilerplate[1]=block[j][(k*4)+1];
                //boilerplate[2]=block[j][(k*4)+2];
                //boilerplate[3]=block[j][(k*4)+3];
                boilerplate[4]=0;
          //      printf("boilerplate %s=>%ld\n",boilerplate,strtol(boilerplate,NULL,2));
                fprintf(trg,"%X",(unsigned int)strtol(boilerplate,NULL,2));
                }
            fprintf(trg,"\",\n");
            }
    }
void writevhdl(FILE*src,FILE*trg){
    fprintf(trg,"library IEEE;\n"
        "use IEEE.std_logic_1164.all;\n"
        "use WORK.rvapo_pkg.all;\n"
        "package rvapo_cm_target_init is\n"
        "subtype Byte_t is std_logic_vector(7 downto 0);\n"
        "type memory_array_t is array (0 to 65535 + 4) of Byte_t;\n"
        "constant MEMORY_INIT: memory_array_t := (\n"
 //       "x\"00\",x\"00\",x\"01\",x\"00\",--at 0x0 00010000 which is stack start = top of mem\n"//is now automagically in compiled program
 //       "x\"08\",x\"00\",x\"00\",x\"00\",--at 0x4 00000008 which is pc start\n"
        );
    char read[4];
    while(fread(read,1,4,src)==4)fprintf(trg,"x\"%02x\",x\"%02x\",x\"%02x\",x\"%02x\",\n",(unsigned char)read[0],(unsigned char)read[1],(unsigned char)read[2],(unsigned char)read[3]);            
    fprintf(trg,"		others => x\"00\"\n"
        ");\n"
        "end package rvapo_cm_target_init;\n");
    }
void writehstr(FILE*src,FILE*trg){
    char read[4];
    while(fread(read,1,4,src)==4)fprintf(trg,"%02d\n%02d\n%02d\n%02d\n",(unsigned char)read[0],(unsigned char)read[1],(unsigned char)read[2],(unsigned char)read[3]);            
    }   
void writerdwr(FILE*src,FILE*trg){
    char read[4];
    while(fread(read,1,4,src)==4)fprintf(trg,"0x%02x%02x%02x%02x;//\n",(unsigned char)read[3],(unsigned char)read[2],(unsigned char)read[1],(unsigned char)read[0]);            
    }      
