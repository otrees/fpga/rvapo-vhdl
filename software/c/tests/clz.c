int main(){
    unsigned int num=0xF0F0F0F0;
    int addr=0x10;
    for(int i=0;i<33;i++){
        asm volatile ( 
        "clz s1,%1;"
		"sw s1, 0(%0);" 
			:
		    : "r" (addr),"r" (num)	//*/
		);   
        addr+=4;
        num>>=1;    
        }
	}	