#include <stdint.h>
#define C_LW(addr,offset)		*((volatile uint32_t*)(addr+offset))
#define C_SW(addr,offset)		(*(volatile uint32_t*)(addr+offset))	
#define AUTOAXIANSWER			0x2BCDDCBA
int main(){
	int val1=0x12345678;
	int val2=0xABBACDDC;
	int axi_base=C_LW(C_LW(4,0),-4); //0x10000;
	C_SW(axi_base,8)=AUTOAXIANSWER;
	asm volatile ( 
		//setup
		"li t0, 0x7;"
		"li t1, 0x5;"
		"li t2, 0x3;"
		"sw t0, 0xC(zero);"
		"sw t1, 4(zero);"
		"sw t2, 8(zero);"

		"nop;"
		"nop;"
		"nop;"
		"nop;"
		//skip ALU
		"li s0, 0x3;"
		"nop;"
		"j skip0;"
		"li s0, 0x9;"
	"skip0:"
		"nop;"
		"nop;"
		"sw s0, 0x10(zero);"
		"nop;"
		"nop;"
		//jump after lw
		"lw s0, 0x4(zero);"
		"j skip1;"
		"li s0, 0x9;"
	"skip1:"
		"nop;"
		"nop;"
		"sw s0, 0x14(zero);"
		"nop;"
		"nop;"
		//jump after long lw
		"lw s0, 0x8(%1);"
		"j skip2;"
		"li s0, 0x9;"
	"skip2:"	
		"nop;"
		"nop;"
		"sw s0, 0x18(zero);"
		"nop;"
		"nop;"
		//skip long ALU
		"li s0, 0x25;"
		"j skip3;"	
		"mul s0,t1,t2;"
	"skip3:"		
		"nop;"
		"nop;"
		"sw s0, 0x1C(zero);"
		"nop;"
		"nop;"
		//skip conditional on mem	
		"lw s0, 0x4(zero);"
		"j skip4;"
		"li s0, 0x9;"
	"skip4:"
		"nop;"
		"nop;"
		"sw s0, 0x20(zero);"
		"nop;"
		"nop;"	

		"ebreak;"
			:
		    : "r" (val1),"r" (axi_base)	//*/
		);
	}	
/*	Expected memory dump:
0000   : 00 00 	10 00
0004   : 00 00  00 05
0008   : 00 00  00 03
000C   : 00 00  00 07
0010   : 00 00  00 03
0014   : 00 00  00 05
0018   : 2B CD  DC BA
001C   : 00 00  00 25
0020   : 00 00  00 05
*/  

