#define IO32ADDR(_val) ((volatile int *)(_val))
#define FPGA_FNCAPPROX_BASE     0x00002000
#define FPGA_FNCAPPROX          IO32ADDR(FPGA_FNCAPPROX_BASE)
#define FPGA_FNCAPPROX_RECI     IO32ADDR(FPGA_FNCAPPROX_BASE+0x04)
#define FPGA_FNCAPPROX_SIN      IO32ADDR(FPGA_FNCAPPROX_BASE+0x08)
#define FPGA_FNCAPPROX_COS      IO32ADDR(FPGA_FNCAPPROX_BASE+0x0c)

int main(){
    int addr=0x10;
    for(int i=10000000;i<1000000000;i+=40000000){
        *FPGA_FNCAPPROX_SIN = i;
        asm volatile("nop\n");
        asm volatile("nop\n");
        asm volatile("nop\n");
        int val1 = *FPGA_FNCAPPROX_SIN;
        asm volatile("nop\n");
        int val2 = *FPGA_FNCAPPROX_COS;

        *FPGA_FNCAPPROX_RECI = i;
        asm volatile("nop\n");
        asm volatile("nop\n");
        asm volatile("nop\n");
        int val3 = *FPGA_FNCAPPROX_RECI;

        asm volatile ( 
		    "sw %1, 0(%0);" 
            "sw %2, 4(%0);" 
            "sw %3, 8(%0);" 
			:
		    : "r" (addr),"r" (val1),"r" (val2),"r" (val3)	
		);   
        addr+=0x10;
        }
	}	