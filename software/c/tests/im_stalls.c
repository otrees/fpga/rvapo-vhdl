#include <stdint.h>
#define C_LW(addr,offset)		*((volatile uint32_t*)(addr+offset))
#define C_SW(addr,offset)		(*(volatile uint32_t*)(addr+offset))	
#define AUTOAXIANSWER			0x2BCDDCBA
int main(){
	int val1=0x12345678;
	int val2=0xABBACDDC;
	int axi_base=C_LW(C_LW(4,0),-4); 
	C_SW(axi_base,8)=AUTOAXIANSWER;

	asm volatile ( 
		//setup
		"li t0, 0x7;"
		"li t1, 0x5;"
		"li t2, 0x3;"
		"sw t0, 0xC(zero);"
		"sw t1, 4(zero);"
		"sw t2, 8(zero);"
		"li t3,	0x1;"

		"nop;"
		"nop;"
		"nop;"
		"nop;"
		//lone long instr read
		"sw t3, 0(zero);"
		"nop;"
		"nop;"
		"nop;"
		"addi s0,zero, 0x90;"
		"nop;"
		"nop;"
		"sw s0, 0x10(zero);"
		"nop;"
		"nop;"
		//long instr read with full pipeline
		"sw t2, 0(zero);"//does long instr read with delay
		"nop;"
		"nop;"
		"addi s0,zero, 0x10;"				
		"addi s0,s0, 0x10;"
		"addi s0,s0, 0x10;"
		"addi s0,s0, 0x10;"
		"addi s0,s0, 0x10;"//here is the long instr read
		"addi s0,s0, 0x10;"//make sure that next pc is not skipped
		"nop;"
		"nop;"
		"sw s0, 0x14(zero);"
		"nop;"
		"nop;"
		//long instr read with long exe
		"sw t3, 0(zero);"
		"mul s0,t1,t2;"//the exe_stall here means that the im_stall will be applied right immediately, while the sw that caused it is still in M
		"nop;"
		"addi s0,s0, 0x10;"//and this is the instr that will take long to read
		"nop;"
		"sw s0, 0x18(zero);"
		"nop;"
		"nop;"
		//long instr read with long LW
		"sw t3, 0(zero);"
		"lw s0, 8(%1);"
		"nop;"		
		"nop;"
		"sub s0,s0,t0;"
		"nop;"
		"nop;"
		"sw s0, 0x1C(zero);"
		"nop;"
		"nop;"
		//long instr read with long LW and long exe dependent on each other (then the exe will be stalled)
		"sw t3, 0(zero);"
		"lw s0, 8(%1);"
		"mul s0,s0,t2;"
		"nop;"
		"sub s0,s0,t0;"
		"nop;"
		"nop;"
		"sw s0, 0x20(zero);"
		"nop;"
		"nop;"
		//long instr read with long LW and long exe independent on each other (then all three will hit at the same moment)
		"sw t3, 0(zero);"
		"lw s0, 8(%1);"
		"mul s1,t1,t0;"
		"nop;"
		"sub s0,s0,t0;"
		"nop;"
		"nop;"
		"sw s0, 0x24(zero);"
		"sw s1, 0x28(zero);"
		"nop;"
		"nop;"
		//long instr read with jump
		"sw t3, 0(zero);"
		"j skip0;"//the jump must enter M right on the heels of it
		"nop;"
		"nop;"		
		"addi s0,zero,0x40;"//this is the long instr read
	"skip0:"
		"addi s0,zero,0x10;"
		"sw s0, 0x2C(zero);"
		"nop;"
		"nop;"
		//long instr read with jump and long exe
		"addi s0,zero,0;"
		"sw t3, 0(zero);"
		"j skip1;"
		"mul s0,t0,t1;"
		"nop;"
		"sub s0,s0,t0;"
	"skip1:"	
		"addi s0,s0,0x15;"
		"nop;"
		"sw s0, 0x30(zero);"
		"nop;"
		"nop;"

		//stall+istall
		"sw t2, 0(zero);"//does long instr read with delay
		"nop;"
		"nop;"
		"nop;"
		"nop;"
		"lw s1, 0x14(zero);"//and here is the lw that is dropped if stall does not have lower priority
		"addi s1,s1, 0x10;"
		"nop;"//here is the long instr read
		"sw s1, 0x34(zero);"
		"nop;"	

		"ebreak;"
			:
		    : "r" (val1),"r" (axi_base)	//*/
		);
	}	
/*	Expected memory dump:
0000   : 00 00 	10 00
0004   : 00 00  00 05
0008   : 00 00  00 03
000C   : 00 00  00 07
0010   : 00 00  00 90
0014   : 00 00  00 60
0018   : 00 00  00 1F
001C   : 2B CD  DC B3
0020   : 83 69  96 27
0024   : 2B CD  DC B3
0028   : 00 00  00 23
002C   : 00 00  00 10
0030   : 00 00  00 15
0034   : 00 00  00 70
*/  

