
#include <stdint.h>
#define C_LW(addr,offset)		*((volatile uint32_t*)(addr+offset))
#define C_SW(addr,offset)		(*(volatile uint32_t*)(addr+offset))	
#define AUTOAXIANSWER			0x2BCDDCBA
int main(){
	int val1=0x12345678;
	int val2=0xABBACDDC;
	int axi_base=C_LW(C_LW(4,0),-4); //0x10000;
	C_SW(axi_base,8)=AUTOAXIANSWER;

	asm volatile ( 
		//setup
		"li t0, 0x7;"
		"li t1, 0x5;"
		"li t2, 0x3;"
		"sw t0, 0xC(zero);"
		"sw t1, 4(zero);"
		"sw t2, 8(zero);"

		"nop;"
		"nop;"
		"nop;"
		"nop;"
		//ALU depends on previous LW
		"lw s0, 8(zero);"
		"addi s0,s0, 0x90;"
		"nop;"
		"nop;"
		"sw s0, 0x10(zero);"
		"nop;"
		"nop;"
		//ALU depends on previous long ALU
		"mul s0,t1,t2;"
		"addi s0,s0, 0x10;"
		"nop;"
		"nop;"
		"sw s0, 0x14(zero);"
		"nop;"
		"nop;"
		//long ALU depends on previous LW
		"lw s0, 0xC(zero);"
		"mul s0,s0,t2;"
		"nop;"
		"nop;"
		"sw s0, 0x18(zero);"
		"nop;"
		"nop;"
		//ALU depends on previous long LW
		"lw s0, 8(%1);"
		"sub s0,s0,t0;"
		"nop;"
		"nop;"
		"sw s0, 0x1C(zero);"
		"nop;"
		"nop;"
		//long ALU depends on previous long LW
		"lw s0, 8(%1);"
		"mul s0,s0,t2;"
		"nop;"
		"nop;"
		"sw s0, 0x20(zero);"
		"nop;"
		"nop;"



		"ebreak;"
			:
		    : "r" (val1),"r" (axi_base)	//*/
		);
	}	
/*	Expected memory dump:
0000   : 00 00 	10 00
0004   : 00 00  00 05
0008   : 00 00  00 03
000C   : 00 00  00 07
0010   : 00 00  00 93
0014   : 00 00  00 1F
0018   : 00 00  00 15
001C   : 2B CD  DC B3
0020   : 83 69  96 2E
*/  

