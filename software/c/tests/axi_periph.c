#include <stdint.h>

#define PARLCD_REG_BASE_PHYS  ((unsigned char*)0x50000000)  //((unsigned char*)0x1000) //0x43c00000 change for fpga
#define PARLCD_REG_SIZE       0x00004000

#define PARLCD_REG_CMD_o                0x0008
#define PARLCD_REG_DATA_o               0x000C

#define SPILED_REG_BASE_PHYS       	0x50200000// 0x00200000 //wrong
#define SPILED_REG_SIZE             0x00004000

#define SPILED_REG_LED_LINE_o           0x004
#define SPILED_REG_LED_RGB1_o           0x010
#define SPILED_REG_LED_RGB2_o           0x014
#define SPILED_REG_LED_KBDWR_DIRECT_o   0x018       //what is this?

#define SPILED_REG_LED_KBDENC_i         0x020 
#define SPILED_REG_LED_ENC2_i         0x024 

#define AXI_BRAM_BASE           0x50100000//0x00100000

#define AXI_ARM_BASE           0x3604c008//0x00100000

#define DATA                    0xF0F0F0F0

void testleds(){
    int base=SPILED_REG_BASE_PHYS;
	int val=DATA;
	asm volatile ( 
		"sw %0, 4(%1);"
		"sw %0, 10(%1);"
        "sw %0, 14(%1);"
        "sw %0, 18(%1);"
			:
		    : "r" (val),"r" (base)	//*/
		);

    base=AXI_BRAM_BASE;
	asm volatile ( 
		"sw %0, 4(%1);"
		"sw %0, 10(%1);"
        "sw %0, 14(%1);"
        "sw %0, 18(%1);"
			:
		    : "r" (val),"r" (base)	//*/
		);   

	base=AXI_ARM_BASE;
	asm volatile ( 
		"sw %0, 4(%1);"
		"sw %0, 10(%1);"
        "sw %0, 14(%1);"
        "sw %0, 18(%1);"
			:
		    : "r" (val),"r" (base)	//*/
		);    

    volatile uint32_t* ledenc=(volatile uint32_t*)SPILED_REG_BASE_PHYS;
    volatile uint32_t* aximem=(volatile uint32_t*)SPILED_REG_BASE_PHYS;

    *(aximem+0x10)=DATA;

    uint32_t data=*(ledenc+SPILED_REG_LED_KBDENC_i);
    (*aximem)=data;

    data=*(ledenc+SPILED_REG_LED_ENC2_i);
    *(aximem+0x4)=data;

    }

void parlcd_write_cmd(unsigned char *parlcd_mem_base, uint16_t cmd){
  	*(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_CMD_o) = cmd;
  	}

void parlcd_write_data(unsigned char *parlcd_mem_base, uint16_t data){
  	*(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_DATA_o) = data;
	}

void parlcd_delay(int msec){
	for(int i=0;i<(msec<<18);i++);
	//for(int i=0;i<(msec);i++);
}

void selectArea(int top_rightx,int top_righty,int low_lefty,int low_leftx){
    parlcd_write_cmd(PARLCD_REG_BASE_PHYS,0x2a);
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(0));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(top_rightx));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(0));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(low_leftx));
    parlcd_write_cmd(PARLCD_REG_BASE_PHYS,0x2b);
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(0));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(top_righty));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(0));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(low_lefty));
    }

void parlcd_hx8357_init(unsigned char *parlcd_mem_base){
  // toggle RST low to reset
/*
    digitalWrite(_rst, HIGH);
    parlcd_delay(50);
    digitalWrite(_rst, LOW);
    parlcd_delay(10);
    digitalWrite(_rst, HIGH);
    parlcd_delay(10);
*/
    parlcd_write_cmd(parlcd_mem_base, 0x1);
    parlcd_delay(30);

// Configure HX8357-B display. ILI9481 also works, though both blinks a lot
    parlcd_write_cmd(parlcd_mem_base, 0x11);
    parlcd_delay(20);
    parlcd_write_cmd(parlcd_mem_base, 0xD0);
    parlcd_write_data(parlcd_mem_base, 0x07);
    parlcd_write_data(parlcd_mem_base, 0x42);
    parlcd_write_data(parlcd_mem_base, 0x18);

    parlcd_write_cmd(parlcd_mem_base, 0xD1);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x07);
    parlcd_write_data(parlcd_mem_base, 0x10);

    parlcd_write_cmd(parlcd_mem_base, 0xD2);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0x02);

    parlcd_write_cmd(parlcd_mem_base, 0xC0);
    parlcd_write_data(parlcd_mem_base, 0x10);
    parlcd_write_data(parlcd_mem_base, 0x3B);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x02);
    parlcd_write_data(parlcd_mem_base, 0x11);

    parlcd_write_cmd(parlcd_mem_base, 0xC5);
    parlcd_write_data(parlcd_mem_base, 0x08);

    parlcd_write_cmd(parlcd_mem_base, 0xC8);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x32);
    parlcd_write_data(parlcd_mem_base, 0x36);
    parlcd_write_data(parlcd_mem_base, 0x45);
    parlcd_write_data(parlcd_mem_base, 0x06);
    parlcd_write_data(parlcd_mem_base, 0x16);
    parlcd_write_data(parlcd_mem_base, 0x37);
    parlcd_write_data(parlcd_mem_base, 0x75);
    parlcd_write_data(parlcd_mem_base, 0x77);
    parlcd_write_data(parlcd_mem_base, 0x54);
    parlcd_write_data(parlcd_mem_base, 0x0C);
    parlcd_write_data(parlcd_mem_base, 0x00);

    parlcd_write_cmd(parlcd_mem_base, 0x36);
    parlcd_write_data(parlcd_mem_base, 0x0a);

    parlcd_write_cmd(parlcd_mem_base, 0x3A);
    parlcd_write_data(parlcd_mem_base, 0x55);

    parlcd_write_cmd(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0x3F);

    parlcd_write_cmd(parlcd_mem_base, 0x2B);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0xDF);

    parlcd_delay(120);
    parlcd_write_cmd(parlcd_mem_base, 0x29);

    parlcd_delay(25);

    selectArea(3,3,3,3);
    parlcd_write_cmd(PARLCD_REG_BASE_PHYS,0x2c);
    parlcd_write_data(PARLCD_REG_BASE_PHYS,0x5489);
}


int main(){
    testleds();

	parlcd_hx8357_init(PARLCD_REG_BASE_PHYS);
	selectArea(0,0,10,10);
    parlcd_write_cmd(PARLCD_REG_BASE_PHYS,0x2c);
    for(int i=0;i<100;i++)parlcd_write_data(PARLCD_REG_BASE_PHYS,0x45F0);
    asm volatile ( 
		"ebreak;"
			:
		    : 	//*/
		);
	}	
