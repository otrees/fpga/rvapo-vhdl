int main(){
	int base=0x2000;
	int val=0x45;
	asm volatile ( 
		"sw %0, 4(%1);"
		"sw %0, 16(%1);"
		"nop;"
			:
		    : "r" (val),"r" (base)	//*/
		);
	}	
//		theory					act	
//reg1	=0x4	is led line		0x4
//reg4	=0x10	is rgb1			0x18
//reg5	=0x14
//reg6  =0x18   
