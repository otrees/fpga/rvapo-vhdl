#include <stdint.h>

#define PARLCD_REG_BASE_PHYS  ((unsigned char*)0x1000) //0x43c00000 change for fpga
#define PARLCD_REG_SIZE       0x00004000

#define PARLCD_REG_CMD_o                0x0008
#define PARLCD_REG_DATA_o               0x000C

/*
instr 0x290 moves from a1/param 0x0001 to a5/0x15 
instr 0x294 stores from a5/0x15 0x0001 to 0xFAA
instr 0x298 loads from 0xFAC 0x1000 to be saved in a5/0x15
instr 0x29c adds 8 to 0x1000 in a5 to get 0x1008 - the addr
instr 0x2a0 loads 0x0001 to a4/0x14
instr 0x2a4 is axi 

but at horizontal mark 0x2A0 unalign skips the 0x298, therefore in a5 remains the 0x0001, therefore dm_addr ends up 0x9 and axi is ignored. 
0F64   : 01 40  05 13
0F68   : B5 DF  F0 EF
0F6C   : 02 10  05 93
0F70   : FE C4  25 03
0F74   : AE 1F  F0 EF
0F78   : 01 40  05 13
0F7C   : B4 9F  F0 EF
0F80   : 02 90  05 93
0F84   : FE C4  25 03
0F88   : AC DF  F0 EF
0F8C   : 07 80  05 13
0F90   : B3 5F  F0 EF
0F94   : 00 00  00 13
0F98   : 01 C1  20 83
0F9C   : 00 00  00 78
0FA0   : 02 01  01 13
0FA4   : 00 00  80 67
0FA8   : 00 05  10 00
0FAC   : 00 00  10 00
0FB0   : 00 00  00 00
0FB4   : 00 00  00 00
0FB8   : 00 00  00 00
0FBC   : 00 00  0F E0
0FC0   : 00 00  00 05
0FC4   : 00 00  00 05
0FC8   : F0 F0  00 00
0FCC   : 00 00  10 00
0FD0   : 00 00  00 00
0FD4   : 00 00  00 00
0FD8   : 00 00  10 00
0FDC   : 00 00  10 00
0FE0   : 00 00  00 00
0FE4   : 00 00  00 00
0FE8   : 00 00  00 00
0FEC   : 00 00  00 19
0FF0   : 00 00  00 00
0FF4   : 00 00  00 00
0FF8   : 00 00  00 00
0FFC   : 00 00  00 00
1000   : 00 00  00 00
*/
void parlcd_write_cmd(unsigned char *parlcd_mem_base, uint16_t cmd){
  	*(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_CMD_o) = cmd;
  	}

void parlcd_write_data(unsigned char *parlcd_mem_base, uint16_t data){
  	*(volatile uint16_t*)(parlcd_mem_base + PARLCD_REG_DATA_o) = data;
	}

void parlcd_delay(int msec){
	for(int i=0;i<(msec<<18);i++);
	//for(int i=0;i<(msec);i++);
}

void selectArea(int top_rightx,int top_righty,int low_lefty,int low_leftx){
    parlcd_write_cmd(PARLCD_REG_BASE_PHYS,0x2a);
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(0));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(top_rightx));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(0));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(low_leftx));
    parlcd_write_cmd(PARLCD_REG_BASE_PHYS,0x2b);
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(0));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(top_righty));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(0));
    parlcd_write_data(PARLCD_REG_BASE_PHYS,(uint16_t)(low_lefty));
    }

void parlcd_hx8357_init(unsigned char *parlcd_mem_base){
  // toggle RST low to reset
/*
    digitalWrite(_rst, HIGH);
    parlcd_delay(50);
    digitalWrite(_rst, LOW);
    parlcd_delay(10);
    digitalWrite(_rst, HIGH);
    parlcd_delay(10);
*/
    parlcd_write_cmd(parlcd_mem_base, 0x1);
    parlcd_delay(30);

// Configure HX8357-B display. ILI9481 also works, though both blinks a lot
    parlcd_write_cmd(parlcd_mem_base, 0x11);
    parlcd_delay(20);
    parlcd_write_cmd(parlcd_mem_base, 0xD0);
    parlcd_write_data(parlcd_mem_base, 0x07);
    parlcd_write_data(parlcd_mem_base, 0x42);
    parlcd_write_data(parlcd_mem_base, 0x18);

    parlcd_write_cmd(parlcd_mem_base, 0xD1);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x07);
    parlcd_write_data(parlcd_mem_base, 0x10);

    parlcd_write_cmd(parlcd_mem_base, 0xD2);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0x02);

    parlcd_write_cmd(parlcd_mem_base, 0xC0);
    parlcd_write_data(parlcd_mem_base, 0x10);
    parlcd_write_data(parlcd_mem_base, 0x3B);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x02);
    parlcd_write_data(parlcd_mem_base, 0x11);

    parlcd_write_cmd(parlcd_mem_base, 0xC5);
    parlcd_write_data(parlcd_mem_base, 0x08);

    parlcd_write_cmd(parlcd_mem_base, 0xC8);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x32);
    parlcd_write_data(parlcd_mem_base, 0x36);
    parlcd_write_data(parlcd_mem_base, 0x45);
    parlcd_write_data(parlcd_mem_base, 0x06);
    parlcd_write_data(parlcd_mem_base, 0x16);
    parlcd_write_data(parlcd_mem_base, 0x37);
    parlcd_write_data(parlcd_mem_base, 0x75);
    parlcd_write_data(parlcd_mem_base, 0x77);
    parlcd_write_data(parlcd_mem_base, 0x54);
    parlcd_write_data(parlcd_mem_base, 0x0C);
    parlcd_write_data(parlcd_mem_base, 0x00);

    parlcd_write_cmd(parlcd_mem_base, 0x36);
    parlcd_write_data(parlcd_mem_base, 0x0a);

    parlcd_write_cmd(parlcd_mem_base, 0x3A);
    parlcd_write_data(parlcd_mem_base, 0x55);

    parlcd_write_cmd(parlcd_mem_base, 0x2A);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0x3F);

    parlcd_write_cmd(parlcd_mem_base, 0x2B);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x00);
    parlcd_write_data(parlcd_mem_base, 0x01);
    parlcd_write_data(parlcd_mem_base, 0xDF);

    parlcd_delay(120);
    parlcd_write_cmd(parlcd_mem_base, 0x29);

    parlcd_delay(25);

    selectArea(3,3,3,3);
    parlcd_write_cmd(PARLCD_REG_BASE_PHYS,0x2c);
    parlcd_write_data(PARLCD_REG_BASE_PHYS,0x5489);
}


int main(){
	parlcd_hx8357_init(PARLCD_REG_BASE_PHYS);
	selectArea(0,0,10,10);
    parlcd_write_cmd(PARLCD_REG_BASE_PHYS,0x2c);
    for(int i=0;i<100;i++)parlcd_write_data(PARLCD_REG_BASE_PHYS,0x45F0);
    asm volatile ( 
		"ebreak;"
			:
		    : 	//*/
		);
	}	
