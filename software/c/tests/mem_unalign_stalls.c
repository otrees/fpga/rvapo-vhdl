int main(){
	int val1=0x12345678;
	int val2=0xABBACDDC;
	asm volatile ( 
		"li t3,	0x1;"
		"nop;"
		"nop;"

		"sw t3, 0(zero);"
		"sw %0, 5(zero);"
		"sw t3, 0(zero);"
		"sw %1, 11(zero);"
		"sw t3, 0(zero);"
		"sh %1, 16(zero);"
		"sw t3, 0(zero);"
		"sh %1, 21(zero);"	//0x15
		"sw t3, 0(zero);"
		"sh %1, 27(zero);"	//0x1B
		"sw t3, 0(zero);"
		"sb %1, 32(zero);"	//0x20
		"sw t3, 0(zero);"
		"sb %1, 37(zero);"
		"sw t3, 0(zero);"
		"sb %1, 42(zero);"
		"sw t3, 0(zero);"
		"sb %1, 47(zero);"

		"sw t3, 0(zero);"
		"lw s2, 5(zero);"
		"nop;"
		"sw t3, 0(zero);"
		"sw s2, 52(zero);"	//0x34
		"nop;"

		"sw t3, 0(zero);"
		"lhu s2, 16(zero);"
		"nop;"
		"sw t3, 0(zero);"
		"sh s2, 60(zero);"
		"nop;"

		"sw t3, 0(zero);"
		"lhu s2, 21(zero);"
		"nop;"
		"sw t3, 0(zero);"
		"sh s2, 64(zero);"	//0x40
		"nop;"

		"sw t3, 0(zero);"
		"lhu s2, 27(zero);"
		"nop;"
		"sw t3, 0(zero);"
		"sh s2, 68(zero);"	//0x44
		"nop;"

		"sw t3, 0(zero);"
		"lbu s2, 32(zero);"
		"nop;"
		"sw t3, 0(zero);"
		"sb s2, 72(zero);"
		"nop;"

		"sw t3, 0(zero);"
		"lbu s2, 37(zero);"
		"nop;"
		"sw t3, 0(zero);"
		"sb s2, 76(zero);"
		"nop;"

		"sw t3, 0(zero);"
		"lbu s2, 42(zero);"
		"nop;"
		"sw t3, 0(zero);"
		"sb s2, 80(zero);"
		"nop;"

		"sw t3, 0(zero);"
		"lbu s2, 47(zero);"
		"nop;"
		"sw t3, 0(zero);"
		"sb s2, 84(zero);"
		"nop;"

		"lw s3,0x12(zero);"//mstall+stall (stall must have lower priority otherwise the second lw is dropped)
		"lw s2,0x1C(zero);"
		"addi s2,s2,0x100;"
		"sw s2, 0x60(zero);"

		"nop;"
		"nop;"
		"nop;"

		"ebreak;"
			:
		    : "r" (val1),"r" (val2)	//*/
		);
	}	
/*	Expected memory dump:
0000   : 00 00  10 00
0004   : 34 56  78 00
0008   : DC 00  00 12
000C   : 00 AB  BA CD
0010   : 00 00  CD DC
0014   : 00 CD  DC 00
0018   : DC 00  00 00
001C   : 00 00  00 CD
0020   : 00 00  00 DC
0024   : 00 00  DC 00
0028   : 00 DC  00 00
002C   : DC 00  00 00
0030   : 00 00  00 00
0034   : 12 34  56 78
0038   : 00 00  00 00
003C   : 00 00  CD DC
0040   : 00 00  CD DC
0044   : 00 00  CD DC
0048   : 00 00  00 DC
004C   : 00 00  00 DC
0050   : 00 00  00 DC
0054   : 00 00  00 DC
0060   : 00 00  01 CD	
*/  

