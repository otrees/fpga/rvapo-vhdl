# RVapo

VHDL educational configurable single-cycle/5-stage pipeline RISC-V processor implementation.

Project homepage [https://gitlab.fel.cvut.cz/otrees/fpga/rvapo-vhdl](https://gitlab.fel.cvut.cz/otrees/fpga/rvapo-vhdl).

The project has been started by Eduard Lavus as homework in frame of the [CTU FEE](https://fel.cvut.cz) [Advanced Computer Architectures](https://cw.fel.cvut.cz/wiki/courses/b4m35pap/start) course. The concept with structured signals passed through stage registers has been proposed by lecturer Pavel Pisa. The project has been substantially expanded to be usable as co processor on AMD/Xylinx Zynq platform by Damir Gruncl. The code base and HDL originating at [PiKRON](https://www.pikron.com/) company has been used as base to prepare the PMSM motor control application as real proof of concept for use in sound applications.

## Structure
Following are core components of the project. Each has their own README which expands on the basic information provided here.
* `/integration/vivado` Files required to synthesize a bitstream for the Zynq-7000 series FPGA using Vivado 2018.2.
* `/integration/ice40` Files required to synthesize a bitstream for the ICE-40 series FPGA using Yosys.
* `/rvapo` HDL files of the processor.
* `/scripts` Scripts to make all the source code do something.
* `/software` Software to run on the processor.

## Platform
The core can be ran in GHDL simulation or on FPGA (specifically, [MZ_APO board](https://cw.fel.cvut.cz/wiki/courses/b35apo/documentation/mz_apo/start) with Zynq-7010 chip or the ICE-V board with Lattice iCE40 chip.

## Workloads
If you want to
* Simulate the processor: 	go to `/rvapo`
* Run regression tests: 	go to `/scripts`
* Run on Zynq: 				go to `/scripts` 
* Design Zynq system		go to `/integration/vivado`
* Work on ICE-V: 			go to `/integration/ice40`
* Write some software:		go to `/software/c`
* Control motor:			go to `/software/c/firmware_fresh`
* Read documentation: 		go to `/rvapo`

## Required software tools
Here is listed software required to make use of the HDL components. 

#### riscv-gcc
`riscv64-unknown-elf-gcc` is required to compile software. Linux binaries can be found here: https://github.com/riscv-collab/riscv-gnu-toolchain. 
(Older) Mac and Windows binaries are here: https://github.com/sifive/freedom-tools/releases.

#### GHDL
Required for simulations. gcc backend is assumed, because it will produce simulation executables that can be then ran anytime. With mcode backend, `f` and `r` modes of `build.sh` will not work correctly.

#### Yosys
Used for ICE-V board, can be obtained here: https://github.com/YosysHQ/oss-cad-suite-build

#### Vivado 2018.2
Used for Zynq-7000. Can be downloaded in the Xilinx archive. 

## Rust
The original Rust toolchain remains, though it only generates .vhd files for `-t` mode. (Also, on last test it did not terminate. To be investigated.)

Run `cargo xtask build <bin>` from `/software/rust` directory to build the selected binary. This requires a nightly rust toolchain with the native and `riscv32i-unknown-none-elf` targets installed. This builds the selected binary crate in `software/` workspace and post-processes the elf file to extract the relevant sections, which it then dumps into a vhdl template.

It is also possible to see the assembly of the binary by:
* Install cargo-binutils: `cargo install cargo-binutils`
* Install llvm-tools-preview: `rustup component add llvm-tools-preview`
* Run `cargo xtask dump <bin> -d`

When writing new software, make sure to use the same structure as the existing one - to correctly set the reset vector and to halt at the end of execution (otherwise the testbench will never terminate). See `/software/rust/common` crate for implementation and `/software/rust/fibonacci` crate for minimal example.

## Resources

https://comparch.edu.cvut.cz/

https://github.com/cvut/qtrvsim/blob/master/src/machine/instruction.cpp

https://jakubdupak.com/

https://dev.jakubdupak.com/qtrvsim/

https://en.wikipedia.org/wiki/RISC-V#Design
